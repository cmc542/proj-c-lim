#! /bin/csh 

set o_path = /data2/cmc542/scratch/2019/Pheno  
#-set x_case = f.e12.FCLM45SP.f19_19.earlyphen.001z 
#-set x_case = f.e12.FCLM45SP.f19_19.latephen.001z 
set x_case = f.e12.FCLM45SP.f19_19.control.001z 

set in_path = /glade/work/cmc542/post/$x_case/to_PDSI/

#-scp -r cmc542@cheyenne.ucar.edu:/glade/scratch/cmc542/post/f.e12.FAMIPC5CN.f19_19.limtoga.001x/to_PDSI/   /data2/cmc542/scratch/MEME/post/f.e12.FAMIPC5CN.f19_19.limtoga.001x/

mkdir -p $o_path/post/$x_case/   
scp -r cmc542@cheyenne.ucar.edu:/$in_path  $o_path/post/$x_case/

