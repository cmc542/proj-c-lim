function [l,t] = mkLegend(ax,Edots)
% [l,t] = mkLegend(ax,Edots)

set(gcf,'currentax',ax);
EdotNames = num2str(Edots);

%Scale Dots
Elim = max(Edots);
FalseLats = zeros(size(Edots));
FalseLons = Edots;
[EDotSize,LimRatio] = ScaleDots(Edots,Elim);


hold on
for i=1:length(Edots)
    hold on
    if ~isnan(LimRatio(i))
        if Edots(i) > 0;
        %    m_plot(x(i), y(i),'marker','o','color', 'k','MarkerFaceColor','k','MarkerSize',EDotSize(i));
             l(i,:) = plot(FalseLons(i), FalseLats(i),'marker','o','color', 'k','MarkerSize',EDotSize(i),'LineWidth',1.5);

        else
%            m_plot(x(i), y(i),'marker','V','color', 'k','MarkerFaceColor',[.95 .95 .95],'MarkerSize', EDotSize(i),'LineWidth',1.5);
             l(i,:) = plot(FalseLons(i),FalseLats(i),'marker','V','color', 'k','MarkerSize', EDotSize(i),'LineWidth',1.5);
        end
         

    end
end
set(gca,'xtick',[],'ytick',[],'box','off','grid','none','xlim',1.1*[FalseLons([1 end])]);
t = text(FalseLons,FalseLats+.25, EdotNames,'rotation',45);