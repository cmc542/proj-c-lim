function TopoMtx = lon_lat_interp(lon,lat);
%TopoMtx = lon_lat_interp(lon,lat);

load topo
TopoLons = [[.5:179.5]'; [-179.5:1:-.5]']; 
TopoLats = [-89.5:89.5]';

lats = lat(1);
latn = lat(end);
lonw = lon(1);
lone = lon(end);

LatInd = find(TopoLats >= lats & TopoLats <=latn);
LonInd = find(TopoLons >= lonw & TopoLons <=lone);

TempTopoMtx = topo(LatInd,LonInd);

TempMtx1 = interp1(TopoLats(LatInd),TempTopoMtx,lat);
TopoMtx = interp1(TopoLons(LonInd),TempMtx1',lon)'; 