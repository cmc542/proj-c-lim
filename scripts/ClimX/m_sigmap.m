function m_sigmap(lon,lat,bindata,varargin);
hold on
for i =1:length(lon);
    for j=1:length(lat);
        if bindata(i,j)==1
            m_plot(lon(i),lat(j),'k.','markersize',3);
        end
    end
end