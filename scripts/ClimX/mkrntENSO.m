function [Xnr] = mkrntENSO(NINO34,PC,nr);
%Usage:
%[Xnr] = mkrntENSO(NINO34,P,nr);
%
%1)Generates nr relizations of a red-noise time series with variance of x.
%   a) estimate acf from x using Allen and Smith AR(1) model estimation
%       -code for ar1 estimation written by Eric Breigenberger (help: ar1)
%   b) computes red noise series with N= lenght(x)*10;
%   c) uses only last Returns Matr (Xnr) of red-noise series.
%   d) Returns only last "length(x)" values (to allow for stabilization)

if size(PC,2)>2
    error('P must include column with time')
end
if any(isnan(PC))
    error('PC must not include NaNs')
end


[Overlap, Ei, Pi] = intersect(floor(NINO34(:,1)),floor(PC(:,1)));
E = mtx_std(NINO34(Ei,2));
P = mtx_std(PC(Pi,2));
n = length(PC);

%estimate alpha parameter
% Regress P agains E:
[m,Bint,Resid] = regress(P,[E ones(size(E))]);
alpha = abs(m(1));

%Estimate beta parameter
% Regress residual (above) against PC:
m2 = regress(P(2:end), [Resid(1:end-1) ones(size(Resid(1:end-1)))]);
beta = abs(m2(1));

y(1) = E(1);
longx =[repmat(E,10,1)];
for j = 1:nr
    for i=2:length(longx);
        y(i) = y(i-1)*alpha+beta*longx(i)+randn;
    end
    Xnr(:,j) = y(end-n+1:end);
end

Xnr = mtx_std(Xnr);
% [Pxx,f] = mtmspec(Xnr(:,1),1600,2);
% Pxx(:,2) = mtmspec(xq,1600,2);
% Pxx(:,3) = mtmspec(Pq,1600,2);
% set(gca,'yscale','linear');
% plot(f,Pxx)
% legend('rn','nino','PC')