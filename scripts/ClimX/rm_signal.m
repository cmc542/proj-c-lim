function [Ypred,Yresid]=rm_signal(X,Y)
%[Ypred,Yresid]=rm_signal(X,Y)

q=intersect(find(~isnan(Y)),find(~isnan(X)));
Yq=Y(q);
Xq=X(q);

b=regress(Yq,[Xq ones(size(Xq))]);
Yqpred=Xq*b(1)+b(2);
Yqresid=Yq-Yqpred;

Yresid=nan(size(Y));
Ypred=nan(size(Y));

Ypred(q)=Yqpred;
Yresid(q)=Yqresid;
