function issig=ttest3D(targ,ref)
%issig=ttest3D(targ,ref)

[nx ny nt]=size(targ);
issig=nan(nx,ny);
for i =1:nx
    for j=1:ny
        x=squeeze(targ(i,j,:));
        y=squeeze(ref(i,j,:));
        if ~all(isnan(x)) && ~all(isnan(y))            
            issig(i,j)=ttest2(x,y);
        end
    end
end