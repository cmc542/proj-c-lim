function l= m_sclplot(ax,lat,lon,dvec,varargin)
% l = m_sclplot(ax,lat,lon,dvec,varargin);

if isempty(varargin)
    maxabs=max(abs(dvec));
    minmax=[-maxabs maxabs];
else
    minmax=varargin{1};
end
    

set(gcf,'currentax',ax);

%Scale Dots
[dvecSize,LimRatio] = ScaleDots(dvec,minmax);


hold on
for i=1:length(dvec)
    hold on
    if ~isnan(LimRatio(i))
        if dvec(i) > 0;
        %    m_plot(x(i), y(i),'marker','o','color', 'k','MarkerFaceColor','k','MarkerSize',dvecSize(i));
             l(i,:) = m_plot(lon(i), lat(i),'marker','o','color', 'k','MarkerSize',dvecSize(i),'LineWidth',1.5);

        else
%            m_plot(x(i), y(i),'marker','V','color', 'k','MarkerFaceColor',[.95 .95 .95],'MarkerSize', dvecSize(i),'LineWidth',1.5);
             l(i,:) = m_plot(lon(i),lat(i),'marker','V','color', 'k','MarkerSize', dvecSize(i),'LineWidth',1.5);
        end
         

    end
end
