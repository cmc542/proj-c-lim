function [SeasMean,SeasTotals,MeanYears] = seas_ave(X,time,months);
%[SeasMean,SeasTotals,MeanYears] = seas_ave(X,time,months);

[nt,ns] = size(X);
Ind12mo = [0:12:nt]';

if months == 'DJF';
    MonInd = [[12:12:nt-12]' [13:12:nt]' [14:12:nt]'];
    SeasMean = [X(MonInd(:,1),:)+X(MonInd(:,2),:)+X(MonInd(:,3),:)];
    MeanYears = [time(MonInd(:,2))]'; %Second Year is used as identifier
elseif months == 'JJA'
    MonInd = [[6:12:nt]' [7:12:nt]' [8:12:nt]'];
    SeasMean = [X(MonInd(:,1),:)+X(MonInd(:,2),:)+X(MonInd(:,3),:)]; 
    MeanYears = floor(time(MonInd(:,2)))';
    
end

SeasTotals = SeasMean;        
SeasMean = SeasMean./3;

    
        