

a=netcdf.open('~/projects/trpacpxxv1/scripts/b40.1850.track1.1deg.006.cam2.h0.TREFHT.000101-130012.annual.nc',0);
TREFHT=netcdf.getVar(a,0);
varname='TREFHT';

lat=netcdf.getVar(a,1);
latname='lat';

lon=netcdf.getVar(a,2);
lonname='lon';

time=1:1300;
timename='time';

ofilename='treftest.nc';
eval(['!rm ' ofilename])


nccreate(ofilename,varname,'Dimensions',{'lon',length(lon),'lat',length(lat),'time',length(time)},'Format','classic')
nccreate(ofilename,'time','dimensions',{'time',length(time)})
nccreate(ofilename,'lon','dimensions',{'lon',length(lon)})
nccreate(ofilename,'lat','dimensions',{'lat',length(lat)})

% write out variables
ncwrite(ofilename,varname,eval(varname))
ncwrite(ofilename,latname,eval(latname))
ncwrite(ofilename,lonname,eval(lonname))
ncwrite(ofilename,timename,eval(timename))


% write out std lon/lat attributes
% lat
ncwriteatt(ofilename,'lat','units','degrees_north')
ncwriteatt(ofilename,'lat','long_name','lattitude')
                
%lon
ncwriteatt(ofilename,'lon','units','degrees_east')
ncwriteatt(ofilename,'lon','long_name','longitude')

% write out variable attributes (units, etc...):
ncwriteatt(ofilename,varname,'long_name','...')


% write out file attirbutes
ncwriteatt(ofilename,'/','notes','blah, blah, blah, ...')
