function tser=GridAveLatLon(lon,lat,data,lonedges,latedges)
% tser=GridAveLatLon(lon,lat,data,lonedges,latedges)

[nx,ny,nt]=size(data);

lonq=find(lon>=lonedges(1) & lon<=lonedges(2));
latq=find(lat>=latedges(1) & lat<=latedges(2));

nlon=length(lon);
nlonq=length(lon(lonq));
nlat=length(lat);
nt=size(data,3);

if ny==length(lon)
    data=permute(data,[2 1 3]);
end

tser=latWgtAve3D(lon(lonq),lat(latq),data(lonq,latq,:));
