function [ndroughts,qdroughts,idroughts,nconseq]=find_mdroughts(x,thresh,dur);
% [ndroughts,qdroughts,idroughts,nconseq]=find_mdroughts(x,thresh,dur);
nconseq=zeros(size(x));
is_dry=false(size(x));

nconseq(1)=0;
for i =1:length(x)
    if x(i)<thresh
        is_dry(i)=1;            
        if i>1
            nconseq(i)=nconseq(i-1)+1;
        else
            nconseq(i)=0;
        end
    end
end

idroughts=(nconseq>=dur);
qdroughts=find(idroughts);
ndroughts=sum(idroughts);