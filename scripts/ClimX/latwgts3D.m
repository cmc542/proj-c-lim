function [wgts3D,wgts2D] = latwgts3D(lat,lon,time);
% [wgts3D,wgts2D] = latwgts3D(lat,lon,time);

lat=lat(:);
lon=lon(:);
time=time(:);
nlon=length(lon);
nlat=length(lat);
ntime=length(time);
wgts=cosd(lat);
ndenom=sum(wgts)*nlon;
wgts2D=repmat(wgts',[nlon 1])./ndenom;
wgts3D=repmat(wgts2D,[1 1 ntime]);