function [bin_median, BinTime] =  median_x2(Xtime,X,bins);
%[bin_mean, bin_sum, BinTime] =  bin_x2(Xtime,X,bins);


binstep = bins(2)-bins(1);

for i = 1:length(bins)-1
    q = find(Xtime >= bins(i) & Xtime<bins(i+1));
    
    bin_mean(i,:) = [nanmedian(X(q,:),1)];
    BinTime(i,1)= nanmean(Xtime(q));
end

i=i+1;
bin_median(i,:) = nan(1,size(X,2));