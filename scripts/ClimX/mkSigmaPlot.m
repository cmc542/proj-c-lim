function [h,l] =  mkSigmaPlot(Data,Width,Height,Overlap);
%Usage:
%h =  mkSigmaPlot(Data,Width,Height,Overlap);
%
%Input:
%Data.......Expects matrix whose first column is the independent x variable
%           (i.e Time, Frequency, etc...) and whose following rows are all 
%           data.
%Width......Width of individual plots
%Height.....Total Height of all the stacked plots for scaling. Note: the heigh 
%           that will appear will be (1-Overlap)*Height.
%Overlap....fraction of overlap between each plot
%
%Output:
%h..........vector of handles for each plot

TempH = axes;
ColorMtx = get(TempH,'ColorOrder');
axis(TempH,'off')

[nt,nplots] = size(Data(:,2:end));
YaxisPos = repmat({'left';'right'},nplots,1);
axHeight = Height/nplots;
LB = .95-axHeight:-(axHeight*(1-Overlap)):-1;
xbounds = [Data(1,1) Data(end,1)];
j = 1;
for i = 1:nplots
    h(i) = axes('Position',[.1 LB(i) Width-.05 axHeight]);
    if j > length(ColorMtx)
        j = 1;
    end
    
    l(i) = plot(Data(:,1), Data(:,i+1),'Color',ColorMtx(j,:),'LineWidth',1.5);
    set(gca,'Yaxislocation',YaxisPos{i},'xcolor',[1 1 1],'color','none','box','off','xlim',xbounds);
    j = j+1;
end

set(gcf,'Color',[1 1 1])
set(h(1),'xcolor',[0 0 0],'xaxislocation','top')
set(h(1:nplots-1),'xticklabel','')
set(h(nplots),'xcolor',[0 0 0])