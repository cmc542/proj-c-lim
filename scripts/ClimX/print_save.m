function print_save(figname,res)
% prnt_sv(figname,res)
%
% figname - string with the desired name of the figure
% res - desired resolution (also a string)

set(gcf,'paperpositionmode','auto')
print([figname '.png'],'-dpng','-painters',['-r' res]);
saveas(gca,[figname '.fig'],'fig');
