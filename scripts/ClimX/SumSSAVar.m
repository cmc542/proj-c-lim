clear
close all

%Data
SSAFilename = '~/SSAFiltRes.mat';
load(SSAFilename);
load(filename);
Data = 
GetCoords
q = PrecipData.GoodData;
AllX{1} = PrecipData.Series.opt(:,q);
AllX{2} = PrecipData.Series.wint(:,q);
AllX{3} = PrecipData.Series.spr(:,q);
AllX{4} = PrecipData.Series.summ(:,q);
AllX{5} = PrecipData.Series.fall(:,q);

%Filtering Parameters
DecBw = [10 20];
MultiDecBw = [20 50];
TrendBw = 50;
m = 15;
RCcutoff =6;

%Matrices to be populated with SSA filtering results:
%   Col1-Trend
%   Col2-Decadal
%   Col3-Multidecadal
OptVar =  nan(3,nlon*nlat);
WintVar = nan(3,nlon*nlat);
SprVar =  nan(3,nlon*nlat);
SummVar =  nan(3,nlon*nlat);
FallVar =  nan(3,nlon*nlat);


%For each season:
for k = 1:length(AllX)
    X = AllX{k};
    
    %Do SSA on each gridpoint
    for j = 1:size(X,2)
        P = SSAStruct{k}.P(1:RCcutoff,j); %Periods that will be considered
        eig = SSAStruct{k}.eig(:,j);
        normeig = eig./sum(eig);

        %Index the "trendy" RCs (Periods >51)
        TrendInd = find(P >=TrendBw);
        TrendVar(k,j) = sum(normeig(TrendInd));
        
        %Compute fraction variance of decadal RCs w/ respect to non-trend RC's
        DecInd = find(P >= DecBw(1) & P < DecBw(2));
        DecVar(k,j) = sum(normeig(DecInd));
        
        %Do the same for Multidecadal RCs
        MultiDecInd = find(P >= MultiDecBw(1) & P < MultiDecBw(2));
        MultiDecVar(k,j) = sum(normeig(MultiDecInd));
    end
end    
    
    
OptVar(:,q) = [DecVar(1,:);
               MultiDecVar(1,:)
               TrendVar(1,:)];

WintVar(:,q) = [DecVar(2,:);
               MultiDecVar(2,:);
               TrendVar(2,:)];
           
SprVar(:,q) = [DecVar(3,:);
               MultiDecVar(3,:);
               TrendVar(3,:)];
               
           
SummVar(:,q) = [DecVar(4,:);
               MultiDecVar(4,:);
               TrendVar(4,:)];
           
FallVar(:,q) = [DecVar(5,:);
               MultiDecVar(5,:);
               TrendVar(5,:)];
           
AllVar = [OptVar; WintVar; SprVar; SummVar; FallVar];