function [BetaVals,xPxx,xf,PxxPred]=CalcBeta(Xmtx,nf,fdomain)
% [BetaVals,xPxx,xf,PxxPred]=CalcBeta(Xmtx,nf,fdomain)

[xPxx,xf]=mtmspec(Xmtx,nf,0);

f_valid=xf> fdomain(1) & xf<=fdomain(2);

logPxx=log(xPxx(f_valid,:));
logf=log(xf(f_valid));
PxxPred=nan(size(xPxx));

for i =1:size(logPxx,2);
	b=regress(logPxx(:,i),[logf ones(size(logf))]);
        BetaVals(i)=b(1);
     
    PxxPred(~f_valid,i)=nan;
    PxxPred(f_valid,i)=exp(log(xf(f_valid))*b(1)+(b(2)));

end


