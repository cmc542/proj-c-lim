function [m,t,dt]=calc_linear_trend(datavec)
%[m,t,dt]=calc_linear_trend(datavec);

y=datavec(:);
q=~isnan(datavec);
x=(1:length(datavec))';
onevec=ones(size(x));
b=regress(y(q),[onevec(q) x(q)]);
m=b(2);
t=nan(size(y));
t(q)=onevec(q)*b(1)+x(q)*b(2);
dt=y-t;