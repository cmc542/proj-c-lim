function time=YYYY2yrfract(YYYYMMstr)

YYYYMMstr=num2str(YYYYMMstr);
time=str2num(YYYYMMstr(:,1:4))+(str2num(YYYYMMstr(:,5:6))-.5)/12;