function [RegCoeffs,Resid,C,P] = FieldReg(x,Field)
%Usage:
%[RegCoeffs,Resid] = FieldReg(x,Field)
%
%Least squares regression of x onto the matrix "Field." Computes
%coefficients for each column of Field using x as the predictor variable.
%The first row of RegCoeffs will be the slope, the second will be the
%intercept. This algorithm follows the inverse model:
%G*m=d;
%
%Ginv = inv([G'*G])*G';
%m = Ginv*d;

%Target field & NaN adjustments:
d = rm_mtx_mean(Field);
GoodData = find(sum(~isnan(d)) >0);
d = d(:,GoodData);
d(isnan(d))=0;
P = nan(1,size(Field,2));
C = P;
[C(:,GoodData),P(:,GoodData)] = nancorr(x,d);

% %Predictor variables "G"
G= [x ones(size(x))];
Ginv = inv([G'*G])*G';
m = Ginv*d;

%Regression coefficients:
RegCoeffs = nan(size(m,1),size(Field,2));
RegCoeffs(:,GoodData) = m;
Resid = nan(size(Field));
Resid = Field-(G*RegCoeffs);