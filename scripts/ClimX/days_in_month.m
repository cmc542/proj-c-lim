function ndays=days_in_month(mo,yr)
% ndays=days_in_month(mo,yr)
ndaysnormal=[31 28 31 30 31 30 31 31 30 31 30 31];
ndaysleap=[31 29 31 30 31 30 31 31 30 31 30 31];

if leapyear(yr)
    ndays=ndaysleap(mo);
else
    ndays=ndaysnormal(mo);
end