function strout=properNoun(strin);
%strout=properNoun(strin);
strout=[upper(strin(1)) strin(2:end)];
