function [ndroughts,qdroughts,idroughts,xsmoo]=find_droughts(x,thresh,tsmoo)
%[ndroughts,qdroughts,idroughts,xsmoo]=find_droughts(x,thresh,tsmoo)
% Input
xsmoo=smoothx(x,tsmoo,'mean');

idroughts=xsmoo<thresh;
qdroughts=find(idroughts);

for i =2:length(qdroughts)
    
    if(qdroughts(i)-qdroughts(i-1)<tsmoo)
        idroughts(qdroughts(i-1))=false;
    end
end

%qdroughts=find(idroughts);
ndroughts=sum(idroughts);