function [pvar,bwstd]=calcpvar_par(x,nf,bw)
%[pvar,bwstd]=calcpvar_par(x,nf,bw)

dims=size(x);
if dims(2)==1
    [Pxx,f]=mtmspec(x,nf,0);
    a=Pxx;
    for i =1:length(f)-1
        b=f(i+1)-f(i);
        h1=min(a(i),a(i+1));
        h2=max(a(i),a(i+1))-h1;
        A(i)=b*h1+.5*b*h2;
    end
    fq=f>=bw(1) & f<bw(2);
    pvar=sum(A(fq))./sum(A);
    bwstd=pvar*nanstd(x);
   %bwstd=sqrt(sum(A(fq)));
    
elseif length(dims)==2
    for j =1:dims(2)
        [Pxx,f]=mtmspec(x(:,j),nf,0);
        a=Pxx;
        for i =1:length(f)-1
            b=f(i+1)-f(i);
            h1=min(a(i),a(i+1));
            h2=max(a(i),a(i+1))-h1;
            A(i)=b*h1+.5*b*h2;

        end
        fq=f>=bw(1) & f<bw(2);
        pvar(j)=sum(A(fq))./sum(A);
        bwstd=pvar*nanstd(x(:,j));
        %bwstd(j)=sqrt(sum(A(fq)));
    end
elseif length(dims)==3
%-  pvar=nan(dims(1),dims(2));
%-  bwstd=nan(dims(1),dims(2));
    pvar=nan(dims(1)*dims(2),1);
    bwstd=nan(dims(1)*dims(2),1);

    [nx ny nt] = size(x); 
     x2 = reshape(x,nx*ny,nt); 
%    for it=1:nt 
%        x3(:,:,it) = reshape(x2(:,it),nx,ny); 
%    end 
%- for k=1:dims(1)
%-     for j=1:dims(2)
%    for ii = 1:nx*ny 
     parfor ii = 1:nx*ny 
%          xtmp=squeeze(x(k,j,:));
           xtmp=squeeze(x2(ii,:));
           if sum(~isnan(xtmp))>0
               [Pxx,f]=mtmspec(xtmp(:),nf,0);
%               a=Pxx;
%               for i =1:length(f)-1
%                   b=f(i+1)-f(i);
%                   h1=min(a(i),a(i+1));
%                   h2=max(a(i),a(i+1))-h1;
%                   A(i)=b*h1+.5*b*h2;
%               end
%               the above have to be in a function to work with parfor 
                A = Acoef(f,Pxx); 
                fq=f>=bw(1) & f<bw(2);
                pvar(ii)=sum(A(fq))./sum(A);
%               bwstd=pvar*nanstd(x(ii,:));

                bwstd(ii)=pvar(ii)*nanstd(x2(ii,:));
%               bwstd=pvar*nanstd(x2(ii,:));
%	        whos bwstd

%               pvar(k,j)=sum(A(fq))./sum(A);
%               bwstd=pvar*nanstd(x(k,j,:));
%-0             %bwstd(k,j)=sqrt(sum(A(fq)));
           else
%              pvar(k,j)=nan;
               pvar(ii)=nan;
           end           
%      end
%  end
     end 
     pvar2 = reshape(pvar,nx,ny); 
     bwstd2 = reshape(bwstd,nx,ny); 
     clear pvar bwstd 
     pvar = pvar2;
     bwstd = bwstd2;
end
