function [unique_modids,modid]=modlist2uniqids(modlist)
% [uniquemodid,modid]=modlist2uniqids(modlist)
% CMIP5 specific

for i =1:length(modlist)
    xline=char(modlist(i).name);
    qu=find(xline=='_');
    modid{i}=xline(1:qu(3)-1);
end
unique_modids=unique(modid);
