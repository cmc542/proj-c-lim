function [Pxx,f,PxxClv95] = mtmspecnorm(x,fin,plotopt,varargin)
%Usage:
%[Pxx,f] = mtmspec(x,fin,plotopt)

if length(varargin)>0
    method=varargin{1};
else
    method='adapt';
end

vx=nanvar(x);
[nt,ns] = size(x);
win=fin.*(2*pi);

for i = 1:ns
    xanom=x(:,i)-nanmean(x(:,i));
    xanom(isnan(xanom))=0;
    
    [Pxx(:,i),PxxClv95,w] = pmtm(xanom,2,win,2*pi);

    %now normalize (this correction is needed b/c of how matlab
    %treats the mtm spectrum (e.g., mean(Pxx) does not equal
    %var(x), but it should).
    Pxx(:,i)=(Pxx(:,i)./nanmean(Pxx(:,i))).*vx(i);

    f = w./(2*pi);
end

if plotopt ==1
    hold on
    plot(f,Pxx)
%     plot(f,PxxClv95,'y')
    set(gca,'XLim',[0 .5])
else end


