function [eofdata] = eof_lim(X,neig,NormOpt,RotOpt)
%[eofdata] = eof_lim(X,neig,NormOpt,RotOpt);
%
% Performs EOF analysis on the correlation or covariance matix of a n x m
% matrix of time series whose rows are observations in time and whose
% columns are locations in space. 
%
% Input:
% X...........Time series matrix (m x n)
% neig........Number of eigenvalues/vectors to compute
% NormOpt.....Normalization option: 1 for no normalization 
%             (covariance matrix), 2 for normalization (correlation matrix)
% RotOpt......Varimax rotation option (1 for rotation)
%
% Output:
% eofdata.E............eigenvectors (m x neig)
% eofdata.A............principal component time series (n x m)
% eofdata.XAcorr.......PC scores correlated with original data (m x neig)
% eofdata.CorrSig......significance of correlations (m x neig). P-values
%                      are determined using estimated degrees of freedom 
%                      adjusted for autocorrelation                       
% eofdata.X_anom.......original time series matrix, X, with column-wise 
%                      mean removed (n x m)  
% eofdata.eig..........eigenvalues (m x 1)
% eofdata.Method.......string indicationg whether the "correlation" 
%                      or "covariance" matrix was used
% eofdata.C............Correlation/Covariance Matrix
% eofdata.Rotation.....string indicating if (varimax) rotation was used or not
%
% UW functions called:
% rm_mtx_mean.m
% mtx_std.m
% AdjDOF95.m
% 
% Toby Ault
% Written: July, 2008
% Re-written: 9/5/2009
% modified: 11/23/2009 (Documentation and AdjDOF95 function added)

% First, center each column by removing mean
X_anom = rm_mtx_mean(X);

%Normalization of X_anom
if NormOpt ==1
    Cttl = 'Covariance Matrix';
    X2 = X_anom;
    C = nancov(X2);
elseif NormOpt ==2
    Cttl = 'Correlation Matrix';
    X2 = mtx_std(X_anom);
    C = nancorr(X2);
    
elseif NormOpt ==3;
    Cttl = {'Covariance Matrix of Normalized Data'; '(diag(C) not necessarily = ones)'};
    X2 = mtx_std(X_anom);
    C = nancov(X2);
else
    error('Invalid Normalization Option')
end

%Correlation/Covariance matrix computation:

%SVD
[E,V] = svd(C);

%Eigenvalues:
L = diag(V);

%Amplitudes:
XnoNaN = X2;
XnoNaN((isnan(X2))) = 0;
A = XnoNaN*E;

%Rotation
if RotOpt ==1;
    A = rotatefactors(A(:,1:neig));
    Rotation = 'On';

else
    Rotation = 'Off';
end

% Correlation between A and each record
for i = 1:neig
    [XAcorr(:,i),CorrSig(:,i)] = AdjDOF95(A(:,i),X);
end

eofdata.E = E;
eofdata.A = A;
eofdata.XAcorr = XAcorr;
eofdata.CorrSig = CorrSig;
eofdata.X_anom = X_anom;
eofdata.eig = L;
eofdata.Method = Cttl;
eofdata.C = C;
eofdata.Rotation = Rotation;

function [C, Sig,Praw,NullVal,AR1x,AR1y] = AdjDOF95(x,Y);
%  [C, Sig,Praw,NullVal,AR1x,AR1y] = AdjDOF95(x,Ymtx);
%
%Returns the correlation coefficient between x (a single column vector) and
%Ymtx (a vector or Matrix) along with a 1 or 0 indicating weather the
%correlation coefficient is signfificant
q= find(sum(~isnan(Y)) ~=0);
Ymtx = Y(:,q);

%correlation between x and Ymtx:
C = nan(1,size(Y,2));
Praw = nan(size(C));
[C(q),Praw(q)] = corr(x,Ymtx,'rows','pairwise');

%number of observations (unadjusted DOF, but accounting for NaNs)
Nx = sum(~isnan(x));
NYmtx = sum(~isnan(Ymtx));

%Autocorrelation coefficient for x
AR1x = corr(x(1:end-1),x(2:end),'rows','pairwise');
AR1y = nan(1,size(Y,2));
for i = 1:length(q)
    AR1y(q(i)) = corr(Ymtx(1:end-1,i),Ymtx(2:end,i),'rows','pairwise');
end

%Adjusted DOF for x - Ymtx correlation:
Nprime = nan(1,size(Y,2));
Nprime(q) = NYmtx.*[1-[AR1x .* AR1y(q)]]./[1+[AR1x .* AR1y(q)]];

%95% confidence values for accepting the Null hypothesis:
NullVal = nan(2,size(Y,2));
NullVal(:,q) = [-1.96./sqrt([Nprime(q)-2]); 1.96./sqrt([Nprime(q)-2])];


%Locations where corelation is less than/greater than Null value:
Sig = nan(size(AR1y));
Sig = [C <= NullVal(1,:) | C>= NullVal(2,:)];

