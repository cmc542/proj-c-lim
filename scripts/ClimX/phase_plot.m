function [p1,a1]=phase_plot(x,y,r,ph)

ang1=0:360;
ang2=0:ph/2;
ang3=ph/2:ph;
xp=r*cosd(ang1);
yp=r*sind(ang1);

p1=plot(x+xp,y+yp);
hold on


if ph>0 & ph<=90;
    %first quadrant
    xph


