function trend3D=calc_linear_trend3D(X3D);
%trend3D=calc_linear_trend3D(X3D);

[nx,ny,nt]=size(X3D);
trend3D=nan(nx,ny);
for i =1:nx;
    for j=1:ny;
        x=squeeze(X3D(i,j,:));
        nnan=sum(isnan(x));
        
        if nnan<.1*nt
            b=regress(x,[ones(size(x)) (1:length(x))']);
                 trend3D(i,j)=b(2);
        end
   
    end
end