function [betamap,meanbeta,meanPxx,f]=PxxSlope(Data3D,nf,frange)
%betamap=PxxSlop(Data3D,nf,frange)
%
%Data3D (X,Y,T)
%nf (1x1)
%frange (1x2)

dim=size(Data3D);
resh=reshape(Data3D,dim(1)*dim(2),dim(3))';

q=sum(~isnan(resh))>0;
nq=sum(q);

[Pxxq,f]=mtmspec(resh(:,q),nf,0);
logf=log(f);
regindx=(1./f >= frange(1) & 1./f <=frange(2));
betaflat=nan(1,dim(1)*dim(2));
betaq=nan(1,nq);
meanPxx=mean(Pxxq,2);
b=regress(log(meanPxx(regindx)),[ones(sum(regindx),1) logf(regindx)]);
meanbeta=b(2);

for i=1:nq
    logPxx=log(Pxxq(:,i));
    b=regress(logPxx(regindx),[ones(sum(regindx),1) logf(regindx)]);
    betaq(i)=b(2);
    
end
betaflat(q)=betaq;
betamap=reshape(betaflat,dim(1),dim(2));