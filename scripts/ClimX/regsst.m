function [RegCoeffs, Sig,Resid] =  regsst(YrPC,TimeScale)
%Usage:
%corrsst(YrPC,TimeScale)
Plotype = 2;

load MyColorMap3
%load ~/Matlab/HadISST/HadISST/excelfiles/HadISST.mat
load pacsst_dt;

data.time = round(data.time);
cla

%Time Index for SST
SSTi1 = find(data.time >= TimeScale(1) & data.time <= TimeScale(end));
% sst = data.tsm(:,SSTTimeIndex)';

%Time index for PC
YrPCi1 = find(YrPC(:,1)>= TimeScale(1) & YrPC(:,1) <= TimeScale(1)) ;
% PC = YrPC(PCTimeIndex,2);

%Overlap:
[O,SSTi,YrPCi] = intersect(data.time,YrPC(:,1));
PC = YrPC(YrPCi,2);
sst = data.tsm(:,(SSTi))';

%Correlate PC w/ SST, compute significance levels:
% C = corr(PC,sst,'rows','pairwise');
[C, Sig,Praw,NullVal,AR1x,AR1y] = AdjDOF95(PC,sst);
clv95 = [min(NullVal(1,:)) max(NullVal(2,:))];
[RegCoeffs,Resid] = FieldReg(PC,sst);

%Make matrices to  map
C2 = RegCoeffs(1,:);
C2(~Sig) = 0;

m1 = reshape(C2',data.nlat,data.nlon)';
m2 = reshape(C',data.nlat,data.nlon)';
m2(isnan(m2)) = 0;
lonw = data.lonlim(1);
lone = data.lonlim(2);
lats = data.latlim(1);
latn = data.latlim(2);

lats = -40;
latn = 60;

cmap = colormap;
%%

%% Grey Coastlines, interpolated data, Mollweide projection
cla
[LATmtx,LONmtx] = meshgrid(data.lat,data.lon);
hold on
pcolorm(LATmtx, LONmtx, m1);
contourm(LATmtx+2.5, LONmtx+2.5, m2,[clv95' clv95'],'k-','linewidth',1);


shading flat
mlabel on;
plabel on;
load coast;
axis off
gridm off
framem on
hg =geoshow('landareas.shp', 'FaceColor', [.7 .7 .7],'edgecolor','k');
colormap(MyColormap)
caxis([-.8 .8])
% c = colorbar;
% set(c,'pos', [0.881    0.305    0.0333    0.4])
%%
colormap(MyColormap);

% m_contour(data.lon,data.lat,m1',[lb lb],'k','LineWidth',3);
% m_contour(data.lon,data.lat,m1',-[lb lb],'k','LineWidth',3);

