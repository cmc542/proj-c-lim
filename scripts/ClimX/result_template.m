% product.metadata.sourcefile=fname;
% product.metadata.creation_date=date;                     
% product.metadata.creation_script=mfilename('fullpath');  
% product.metadata.created_by=getenv('USER');              
%                                                          
% product.nino34.time=time;                                     
% product.nino34.time_units='Years from initialization';                               
% product.nino34.time_annual=time(6:12:end);
% product.nino34.time_djf=time(13:12:end);
%                                                          
% product.nino34.monthly=trefht;          
% product.nino34.quantity = 'SST';
% product.nino34.units='degK';
% product.nino34.ylabel=[product.nino34.quantity '(' product.nino34.units ')'];        
% product.nino34.annual=bin_x2(product.nino34.time,product.nino34.monthly,product.nino34.time(1:12:end));
% product.nino34.djf=(product.nino34.monthly(13:12:end)+product.nino34.monthly(14:12:end)+product.nino34.monthly(14:12:end))./3;
%                                                          
% product.spectrum.fmo=fmo;
% product.spectrum.fyr=fyr;                                 
% product.spectrum.monthly=mlpmtm(product.nino34.monthly,nw,nfmo);                                  
% product.spectrum.annual=mlpmtm(product.nino34.annual,nw,nf);
% product.spectrum.djf=mlpmtm(product.nino34.djf,nw,nf);

!head -22 ~/Matlab/ClimX/result_template.m | sed 's/\%/   /g'

