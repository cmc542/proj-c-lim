function [F]=butterfilt(x,choice,N,per,missing)
%http://portal.iri.columbia.edu/Production/Climate/Pred/WWC/matlab/filtrage_nan.m
%V
% Y = butterfilt(x,choice,N,per,missing)
%      (Y = butterfilt(x,filtertype,nord,freq_cutoff,missingval)
%
% This function filters the columns of an input matrix with a recursive
% Butterworth filter.
%
% Input
% X : matrix of real numbers (with time in rows and space in columns). The
% columns of 'x' are independetly filtered.
%
% 'N' : odd scalar giving the d of the filter 
%
% 'choice' : character string to design the filter ('low', 'high', 
% 'bandpass' or 'stop' for low-pass, high-pass, band-pass (frequency between
% limits defined in 'order' are kept), and band-pass (frequency outside
% limits defined in 'order' are kept) filters). if choice = 'bandpass' or
% 'stop', order should be a two-member vector.
%
% 'per' : scalar or vector giving the frequency to be filtered in periods 
% of unit time (if data are daily data and order = 30, the cut-off is fixed
% at 1/30 cycle-per-day.
% 
% missing: missing values (ie nan)
%
% Output
% Y : filtered matrix
%
% created in May 1996 and modified in May 2004
% Vincent Moron
% moron@cerege.fr

if missing ~= NaN; 
   x(find(x == missing))=NaN*ones(size(find(x == missing)));
end

[n,nc]=size(x);
ns=round(n/2);
sim1=flipud(x(2:ns+1,:));
sim2=flipud(x(n-ns:n-1,:));
x=[sim1;x;sim2];

[b,a]=butter(N,[2./per],choice);

for i=1:nc,
    y=x(:,i);
    pos=find(isnan(x(:,i)));
    if ~isempty(pos);
        nm=find(~isnan(x(:,i)));
        for j=1:length(pos);
            d=pos(j)-nm;
            d=find(abs(d)==min(abs(d)));
            if length(d) == 2
                y(pos(j))=sum(x(nm(d)))./2;
            else
                y(pos(j))=x(nm(d));
            end
        end    
        [b2,a2]=butter(N,[2./round(n/3)],'low');
        bf2=filtfilt(b2,a2,y);
        y=x(:,i);
        y(pos)=bf2(pos);
    end
    bf(:,i)=filtfilt(b,a,y);
    bf(pos,i)=NaN*ones(length(pos),1);
end;

F=bf(ns+1:n+ns,:);