function pr=convert_pr_units(pr,time,startmo,fromUnits,toUnits,varargin)
%pr=convert_pr_units(pr,time,startmo,fromUnits,toUnits,varargin);

if nargin==6
    time_calendar=varargin{1}; 
else
    time_calendar='gregorian';   
end

if strcmp(fromUnits,'kg m-2 s-1') && strcmp(toUnits,'mm/mo')
    % first convert from mass flux to mm/day    
    pr=(pr./1000)*86400000.;% convert from precip flux (kg/m2 s) to precip (mm/day) 
    fromUnits='mm/day';
end

if (strcmp(fromUnits,'mm/mo') || strcmp(fromUnits,'mm'))&& strcmp(toUnits,'mm/mo')    
    disp('units already in mm/mo');
 elseif strcmp(fromUnits,'mm/day') && strcmp(toUnits,'mm/mo')    
    k=startmo;
    for i =1:length(time)
        if (strcmp(time_calendar,'noleap') | strcmp(time_calendar,'365_day') | strcmp(time_calendar,'360_day'))
            ndays=30;
        else
            ndays=days_in_month(k,floor(time(i)));
        end
        pr(:,:,i)=pr(:,:,i)*ndays;        
        k=k+1;  
        if k>12
            k=1;
        end
    end    
else
    error('Unrecognized units')
end
    
    