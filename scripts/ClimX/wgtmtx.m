function [latwgts,wgts3D] = wgtmtx(lat,nlon,nt)
%[latwgts,wgts3D] = wgtmtx(lat,nlon,nt);

if size(lat,1) < size(lat,2)
    lat = lat';
end
nlat = length(lat);    
wgts = repmat(cosd(lat),nt,nlon);
latwgts = reshape(reshape(repmat(cosd(lat),nt,nlon),nlat,nlon,nt),nlon*nlat,nt)';
wgts3D=permute(reshape(repmat(cosd(lat),nt,nlon),nlat,nlon,nt),[2 1 3]);
