function ts=rsclxplaw(x,bapprox);
%  ts=rsclxplaw(x,bapprox);
% x - a time series to be rescaled
% bapprox - approximate value of beta.

new=1;
if new==1
    %new:
    % ts params
    mn=mean(x);
    sg=std(x);
    x=(x-mn)/sg;

    % fft params
    nreps=2;
    N=length(x)*nreps;
    k=(0:N-1)';

    kshift=0;
    sclfac=((k+kshift)/N).^(-bapprox/sqrt(2)); %pelletier 2007, eqn 9.1
    if isinf(sclfac(1)) %can't have inf, so 0/N gets rescaled by same coeff as 1/n
        sclfac(1)=sclfac(2);
    end

    xk=fft(repmat(x(:),nreps,1)); %fft: x(k)
    xk=xk./mean(abs(xk));

    xpk=xk.*sclfac; % scaled fft: x_p(k)

    ts=real(ifft(xpk)); % ifft
    ts=ts(1:N/nreps);

    %restore original mean and variance
    ts=(ts-mean(ts));
    ts=ts./std(ts);
    ts=ts*sg+mn;
elseif new==2
        %new:
    % ts params
    mn=mean(x);
    sg=std(x);
    x=(x-mn)/sg;

    % fft params
    nreps=2;
    N=length(x)*nreps;
    k=(0:N-1)';

    kshift=0;
    sclfac=((k+kshift)/N).^(-bapprox); %pelletier 2007, eqn 9.1 need to resolve
    if isinf(sclfac(1)) %can't have inf, so 0/N gets rescaled by same coeff as 1/n
        sclfac(1)=sclfac(2);
    end

    xk=fft(repmat(x(:),nreps,1)); %fft: x(k)
    xk=xk./mean(abs(xk));

    xpk=xk.*sclfac; % scaled fft: x_p(k)

    ts=real(ifft(xpk)); % ifft
    ts=ts(1:N/nreps);

    %restore original mean and variance
    ts=(ts-mean(ts));
    ts=ts./std(ts);
    ts=ts*sg+mn;
else
    %old 
    % ts params
    mn=mean(x);
    sg=std(x);
    x=(x-mn)/sg;

    % fft params
    nf=length(x)*2;
    f=[1/2:1/2:nf/2]'/(nf);

    ts=real(ifft((1./f.^(bapprox)).*fft([x; x]))); 
    ts=ts(1:length(x));% avoids edge effects from fft
    ts=(ts-mean(ts));
    ts=ts./std(ts);

    ts=ts*sg+mn;
end
    
%Option1: turn off 0/N
%option2: use something very small
% kshift=.001;
% k=(kshift:N-1+kshift)';

%option 3: use 1/N for 0/N
