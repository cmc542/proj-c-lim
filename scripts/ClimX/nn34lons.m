function lonedges=nn34lons

disp('170W to 120W')
lonedges=lon180_360(-[170 120]);

disp(num2str(lonedges))