function [eofdata] = limitedeof(X,neig,NormOpt,RotOpt,varargin)
%[eofdata] = limitedeof(X,neig,NormOpt,RotOpt,Nrot);
%
%Inputs
%X........................Time observations (N-rows x M-columns)
%                        (columns should be time, rows should be space)
%neig.....................Number of Eigenvectors to compute
%NormOpt..................Data normalization option:
%                           1-no normalization (Covariance Matrix is used
%                             for SVD)
%                           2-Normalize data (SVD is performed on
%                             correlation matrix)
%RotOpt...................Rotation (varimax) option (1-yes, 2-no)
%
%
%Outputs:
% eofdata.E...............Eigenvector matrix (M,neig);
% eofdata.A...............PC time series matrix (N,neig);
% eofdata.XAcorr..........Correlation between orginal data (X) and each PC
% eofdata.CorrSig.........Index (boolean) of points where the correlation
%                         is significant
% eofdata.X_anom..........Anomaly matrix of X (columnwise mean removed)
% eofdata.eig.............Eigenvalues
% eofdata.Method..........Method used (Correlation or Covariance)
% eofdata.C...............Correlation or covariance matrix (depends on
%                         method)
% eofdata.Rotation........States whether varimax rotation was applied or
%                         not

%1-No normalization (covariance matrix)
%2-Normalize first (correlation matrix)
if nargin==5
    Nrot=varargin{1};
else
    Nrot=neig;
end
X_anom = rm_mtx_mean(X);

%Normalization of X_anom
if NormOpt ==1
    Cttl = 'Covariance Matrix';
    X2 = X_anom;
    C = nancov(X2,'pairwise');
elseif NormOpt ==2
    Cttl = 'Correlation Matrix';
    X2 = mtx_std(X_anom);
    C = nancorr(X2);
    
elseif NormOpt ==3;
    Cttl = {'Covariance Matrix of Normalized Data'; '(diag(C) not necessarily = ones)'};
    X2 = mtx_std(X_anom);
    C = nancov(X2,'pairwise');
else
    error('Invalid Normalization Option')
end

%Correlation/Covariance matrix computation:

%SVD
[E,V] = eigs(C,neig);

%Eigenvalues:
L = diag(V);

%Amplitudes:
XnoNaN = X2;
XnoNaN((isnan(X2))) = 0;
A = XnoNaN*E;

%Rotation
if RotOpt ==1;
    [E,rotmax] = rotatefactors(E(:,1:Nrot),'Maxit',1000);
    A =A*rotmax;
    Rotation = 'On';

else
    Rotation = 'Off';
end

% Correlation between A and each record
for i = 1:neig
    [XAcorr(:,i),CorrSig(:,i)] = AdjDOF95(A(:,i),X);
    Reg1 = FieldReg(mtx_std(A(:,i)),XnoNaN);
    R(:,i) = Reg1(1,:)';
end

eofdata.E = E;
eofdata.A = A;
eofdata.Astd = zscore(A);
eofdata.XAcorr = XAcorr;
eofdata.R = R;
eofdata.CorrSig = CorrSig;
eofdata.X_anom = X_anom;
eofdata.eig = L;
eofdata.Method = Cttl;
eofdata.C = C;
eofdata.Rotation = Rotation;

function [C, Sig,Praw,NullVal,AR1x,AR1y] = AdjDOF95(x,Y);
%  [C, Sig,Praw,NullVal,AR1x,AR1y] = AdjDOF95(x,Ymtx);
%
%Returns the correlation coefficient between x (a single column vector) and
%Ymtx (a vector or Matrix) along with a 1 or 0 indicating weather the
%correlation coefficient is signfificant
q= find(sum(~isnan(Y)) ~=0);
Ymtx = Y(:,q);

%correlation between x and Ymtx:
C = nan(1,size(Y,2));
Praw = nan(size(C));
[C(q),Praw(q)] = corr(x,Ymtx,'rows','pairwise');

%number of observations (unadjusted DOF, but accounting for NaNs)
Nx = sum(~isnan(x));
NYmtx = sum(~isnan(Ymtx));

%Autocorrelation coefficient for x
AR1x = corr(x(1:end-1),x(2:end),'rows','pairwise');
AR1y = nan(1,size(Y,2));
for i = 1:length(q)
    AR1y(q(i)) = corr(Ymtx(1:end-1,i),Ymtx(2:end,i),'rows','pairwise');
end

%Adjusted DOF for x - Ymtx correlation:
Nprime = nan(1,size(Y,2));
Nprime(q) = NYmtx.*[1-[AR1x .* AR1y(q)]]./[1+[AR1x .* AR1y(q)]];

%95% confidence values for accepting the Null hypothesis:
NullVal = nan(2,size(Y,2));
NullVal(:,q) = [-1.96./sqrt([Nprime(q)-2]); 1.96./sqrt([Nprime(q)-2])];


%Locations where corelation is less than/greater than Null value:
Sig = nan(size(AR1y));
Sig = [C <= NullVal(1,:) | C>= NullVal(2,:)];

function [C,P] = nancorr(X,varargin)
%Usage: Same as corr, but for matrices with missing values:
%[C,P] = nancorr(X);
%Returns the correlation matrix for X
%
%[C,P] = nancorr(X,Y);
%Returns the correlation matrix betwen the columns of X and the columns of
%Y.



if ~isempty(varargin)
    Y = varargin{1};
    [C,P] = corr(X,Y,'rows','pairwise');
else
    [C,P] = corr(X,'rows','pairwise');
end


function [NormX,ColStd,MtxSigma]  = mtx_std(X);
%[NormX,ColStd,MtxSigma]  = mtx_std(X);

X = rm_mtx_mean(X);
[nt,ns] = size(X);
ColStd = nanstd(X);
MtxSigma = repmat(ColStd,nt,1);
NormX = X./MtxSigma;