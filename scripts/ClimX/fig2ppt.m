function fig2ppt(width_height,filename,varargin)
% fig2ppt([width height],filename,varargin)

if length(varargin)==0
	figh=gcf;
	res='300';

elseif length(varargin)==1
	figh=varargin{1};
	res='300';

elseif length(varargin)==2
	figh=varargin{1};
	res=num2str(varargin{2});

end

w=width_height(1)*11;
h=width_height(2)*8;
figure(figh)

set(gcf,'color','w')
set(figh,'paperposition',[1 1 w h])
PrevFig(figh)
set(gcf, 'InvertHardCopy', 'off');
print('-dpng',['-r' res],[filename '.png'])
saveas(gca,[filename '.fig'],'fig');
