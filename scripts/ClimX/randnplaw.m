function ts=randnplaw(nt,bapprox);
%  ts=randnplaw(nt,betacoeff);
% nt - number of points in time desired
% bapprox - approximate value of beta.

nf=nt;
f=[1/2:1/2:nf/2]'/(nf);

ts=real(ifft((1./f.^(bapprox/sqrt(2))).*fft(randn(nf,1)))); 
ts=ts./std(ts);