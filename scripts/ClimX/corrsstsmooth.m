function [C, Sig,NullVal,AR1x,AR1y] =  corrsstsmooth(YrPC,TimeScale)
%Usage:
%corrsst(YrPC,TimeScale)
Plotype = 2;

load MyColorMap3
%load ~/Matlab/HadISST/HadISST/excelfiles/HadISST.mat
load NNresidsst;

data.time = round(data.time);
cla

%Time Index for SST
SSTi1 = find(data.time >= TimeScale(1) & data.time <= TimeScale(end));
% sst = data.tsm(:,SSTTimeIndex)';

%Time index for PC
YrPCi1 = find(YrPC(:,1)>= TimeScale(1) & YrPC(:,1) <= TimeScale(1)) ;
% PC = YrPC(PCTimeIndex,2);

%Overlap:
[O,SSTi,YrPCi] = intersect(data.time,YrPC(:,1));
PC = YrPC(YrPCi,2);
sst = data.tsm(:,(SSTi))';

%Correlate PC w/ SST, compute significance levels:
% C = corr(PC,sst,'rows','pairwise');
smoothsst = smoothx(sst,7,'gaus');
[C, Sig,NullVal,AR1x,AR1y] = AdjDOF95(PC,smoothsst);

%Make matrices to  map
C2 = C(1,:);
% C2(~Sig) = 0;

m1 = reshape(C2',data.nlat,data.nlon)';
m2 = reshape(C2',data.nlat,data.nlon)';
lonw = data.lonlim(1);
lone = data.lonlim(2);
lats = data.latlim(1);
latn = data.latlim(2);

lats = -40;
latn = 60;

cmap = colormap;
%%

%% Grey Coastlines, interpolated data, Mollweide projection
cla
[LATmtx,LONmtx] = meshgrid(data.lat,data.lon);
hold on
pcolorm(LATmtx, LONmtx, m1);
% contourm(LATmtx, LONmtx, m2,[1 1],'k--','linewidth',1);

shading flat
mlabel on;
plabel on;
load coast;
axis off
gridm off
framem on
hg =geoshow('landareas.shp', 'FaceColor', [.7 .7 .7],'edgecolor','k');
colormap(MyColormap)
caxis([-.8 .8])
% c = colorbar;
% set(c,'pos', [0.881    0.305    0.0333    0.4])
%%
colormap(MyColormap);

% m_contour(data.lon,data.lat,m1',[lb lb],'k','LineWidth',3);
% m_contour(data.lon,data.lat,m1',-[lb lb],'k','LineWidth',3);

