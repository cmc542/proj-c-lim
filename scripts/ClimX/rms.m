function rmsd=rms(x1,x2);
% calcualte root mean squared difference, handles NaNs:
%
%			           /  sum[  { [C-mean(C)] - [Cr-mean(Cr)] }.^2  ]  \
%			RMSD = sqrt|  -------------------------------------------  |
%			           \                      N                        /
%
%
% x1 is the reference vector (N x 1)
% x2 is the vector of samples to be compared with x1 (N x m)*
%  *x2 may have muliple columns, but must have same # of rows as x1

mn1=nanmean(x1);
mn2=nanmean(x2);
[N,m]=size(x2);
if m==1
   rmsd=sqrt(sum((((x1-mn1) - (x2-mn2)).^2)/N));
end