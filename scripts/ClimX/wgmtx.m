function latwgts = wgtmtx(lat,nlon,nt)

if size(lat,1) > size(lat,2)
    lat = lat';
end
    
wgts = repmat(cos(lat),1,nlon);