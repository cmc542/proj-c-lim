function ColorInd = ScaleColors(X,varargin)
 % ColorInd = ScaleColors(X,varargin)
if ~isempty(varargin);
    if length(varargin{1})==1
        max_X = varargin{1};
        min_X=nanmin(X);
    elseif length(varargin{1})==2
        max_X = varargin{1}(2);
        min_X = varargin{1}(1);
    end    
else
    max_X = nanmax(X);
    min_X=nanmin(X);
end

if nargin==3
    ncolors=length(varargin{2});
else
    ncolors=64;
end

    

ColorInd = round(ncolors*(X-min_X)./(max_X-min_X));
ColorInd(ColorInd <1) = 1;
ColorInd(ColorInd >ncolors) = ncolors;
