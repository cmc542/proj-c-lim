function [f,P,prob] = lombscrgl(t,h,ofac,hifac)
%   [f,P,prob] = lombscrgl(t,h,ofac,hifac)
% Calls "lomb" but allows for matrices in "h" and also adjusts
% variance rather than normalizing it as in lomb.

[nt,ns]=size(h);
for i=1:ns
    v=var(h(:,i));
    [f,P(:,i),prob(:,i)]=lomb(t,h(:,i),ofac,hifac);
    P(:,i)=P(:,i)*v;
end