function filename=filename2fileinfo(varname,realm,modid,expid,runid,yrStartStr,yrEndStr)
% filename=filename2fileinfo(varname,realm,modid,expid,runid,yrStartStr,yrEndStr)
%
% Requires inputs as strings.


%pr_Amon_CCSM4_historical_r1i2p1_185001-200512.nc
% [1]  [2]   [3]        [4]    [5]
filename=[varname '_' realm '_' modid '_' expid '_' runid '_' yrStartStr '-' yrEndStr];