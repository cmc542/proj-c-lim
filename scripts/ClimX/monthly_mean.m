function mo_means=monthly_means(data);
for i =1:12
    mo_means(:,:,i)=nanmean(data(:,:,i:12:end),3);
end