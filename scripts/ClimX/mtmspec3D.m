function [Pxx3D,f]=mtmspec3D(X3D,nf);
% [Pxx3D,f]=mtmspec3D(X3D,nf);
% Time must be last dimension

[resh,q,nx,ny,nt]=reshape3Ddata(X3D);
[Pxxq,f]=mtmspec(resh(:,q),nf,0);
nfu=size(Pxxq,1);

pxxreshq=nan(nfu,nx*ny);
pxxreshq(:,q)=Pxxq;
Pxx3D=reshape(pxxreshq',nx,ny,nfu);