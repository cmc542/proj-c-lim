function data_i=interpMap(lon,lat,data,newlon,newlat);
%data_i=interpMap(lon,lat,data,newlon,newlat);

[LON,LAT]=meshgrid(lon,lat);
[newLON,newLAT]=meshgrid(newlon,newlat);
[nlat,nlon,nt]=size(data);
nlatNew=length(newlat);
nlonNew=length(newlon);
data_i=nan(nlonNew,nlatNew,nt);

for i =1:nt
%    disp(i) 
    %F=TriScatteredInterp(reshape(LON,[],1),reshape(LAT,[],1),reshape(squeeze(data(:,:,i)'),[],1));
    %data_i(:,:,i)=F(newLON,newLAT)';
    data_i(:,:,i)=interp2(LON,LAT,data(:,:,i)',newLON,newLAT)';
end
    