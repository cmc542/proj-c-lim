function [pvar,bwstd]=calcpvar(x,nf,bw)
%[pvar,bwstd]=calcpvar(x,nf,bw)

dims=size(x);
if dims(2)==1
    [Pxx,f]=mtmspec(x,nf,0);
    a=Pxx;
    for i =1:length(f)-1
        b=f(i+1)-f(i);
        h1=min(a(i),a(i+1));
        h2=max(a(i),a(i+1))-h1;
        A(i)=b*h1+.5*b*h2;

    end
    fq=f>=bw(1) & f<bw(2);
    pvar=sum(A(fq))./sum(A);
    bwstd=pvar*nanstd(x);
    %bwstd=sqrt(sum(A(fq)));
    
elseif length(dims)==2
    for j =1:dims(2)
        [Pxx,f]=mtmspec(x(:,j),nf,0);
        a=Pxx;
        for i =1:length(f)-1
            b=f(i+1)-f(i);
            h1=min(a(i),a(i+1));
            h2=max(a(i),a(i+1))-h1;
            A(i)=b*h1+.5*b*h2;

        end
        fq=f>=bw(1) & f<bw(2);
        pvar(j)=sum(A(fq))./sum(A);
        bwstd=pvar*nanstd(x(:,j));
        %bwstd(j)=sqrt(sum(A(fq)));
    end
elseif length(dims)==3
    pvar=nan(dims(1),dims(2));
    bwstd=nan(dims(1),dims(2));
   for k=1:dims(1)
       for j=1:dims(2)
           xtmp=squeeze(x(k,j,:));
           if sum(~isnan(xtmp))>0
               [Pxx,f]=mtmspec(xtmp,nf,0);
                a=Pxx;
                for i =1:length(f)-1
                    b=f(i+1)-f(i);
                    h1=min(a(i),a(i+1));
                    h2=max(a(i),a(i+1))-h1;
                    A(i)=b*h1+.5*b*h2;

                end
                fq=f>=bw(1) & f<bw(2);
                pvar(k,j)=sum(A(fq))./sum(A);
                bwstd=pvar*nanstd(x(k,j,:));
                %bwstd(k,j)=sqrt(sum(A(fq)));
           else
               pvar(k,j)=nan;
           end           
       end
   end
end

    
