function units=ncgetunits(filename,varname)
%units=ncgetunits(filename,varname)

ncid=netcdf.open(filename,0);
varID=netcdf.inqVarID(ncid,varname);
units=netcdf.getAtt(ncid,varID,'units');
netcdf.close(ncid);