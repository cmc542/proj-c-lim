% Adds a d18O label to the y-axis of the current axis.

%%
ylbl = ylabel(['{\fontsize{8}\delta^{18}O} {\fontsize{10}%}{\fontsize{6}o}']);
%%
