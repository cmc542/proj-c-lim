function AgeModel;
%Written by T.R. Ault 4/11/07
%
%Linear interopolation routine to convert observations in depth to
%observations in time using a user specified file with time-depth
%conversion points. From the command line in Matlab, simply time "AgeModel"
%and follow the instructions on subsequent menus. The user will be asked to
%specify filenames for two files: (1) the file where the data is stored and
%(2) the depth-time conversion file. User will be asked to specify the
%desired sample resolution (in time). Output will be stored to a
%user-specified text file. 
%
%This function relies on interp1 for most of the work. Observations in
%depth outside of the range specified by the depth-time file are truncated.
%
%Make sure first column of "Data" file contains sample depths (increasing).
%Make sure second column of Depth-Time conversion file is Time (decreasing, 
%preferably monotonically).

help AgeModel
%Load file with data as a function of sample Depth
DataFile = uigetfile(['*.txt'],'Please select ".txt" data file ([DEPTH VALUES]):');
Data = load(DataFile);

%Load Depth-Time Conversion file
DepthTimeFile = uigetfile(['*.txt'],'Please select ".txt" depth-time conversion file ([DEPTH TIME]):');
DepthTime = load(DepthTimeFile);

%Interpolate to find the correct time values at each sample depth:
RawTime = interp1(DepthTime(:,1),DepthTime(:,2),Data(:,1));
q = find(~isnan(RawTime));

ResChoice = menu('Select Desired Resolution', 'Monthly','Bi-Monthly','Seasonal','Annual','Other');

if ResChoice ==1
    Res = 1./12;
elseif ResChoice ==2
    Res = 1./6;
elseif ResChoice ==3
    Res = 1./4;
elseif ResChoice ==4
    Res = 1;
else
    Res = 1./cell2mat(inputdlg('Enter # of desired observations per year:'));
end

NewTime = [RawTime(q(1)):-Res:RawTime(q(end))]';
InterpData =[NewTime interp1(RawTime(q),Data(q,2:end),NewTime)];
figure
subplot(211)
    plot(DepthTime(:,1),DepthTime(:,2),'kd',Data(:,1),RawTime,'r');
    xlabel('Depth')
    ylabel('Time')
    title('Matlab Age Model','Fontsize',16)
    legend('Tie points','Age Model')
subplot(212)
    plot(InterpData(:,1),InterpData(:,2:end))
    xlabel('Time')
    ylabel('Data')
    title('Interpolation Results','Fontsize',16)
    
PrintOpt = menu({'Interpolation successful!';'Would you like a .jpg image of the results?'},'Yes','No');
if PrintOpt ==1
    print -djpeg -r300 AgeModelOutput.jpg
end
close

NewDataFile = uiputfile(['*.txt'],'Please enter output file name:');
save(NewDataFile,'InterpData','-ascii');