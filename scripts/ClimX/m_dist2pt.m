function dvec= m_dist2pt(xdot,xcol);

dvec=nan(size(xcol,1),1);
for i =1:size(xcol,1)
    dvec(i,1)=m_lldist([xdot(1) xcol(i,1)],[xdot(2) xcol(i,2)]);
end