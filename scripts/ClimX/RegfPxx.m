function [Beta,Pxx,f,fbinned,PxxBinned,PxxPred,Rsq,Fstat,pval,Err]= RegfPxx(x,nf,fmin);
% [Beta,Pxx,f,fbinned,PxxBinned,PxxPred,Rsq,Fstat,pval,Err]=RegfPxx(x,nf,fmin);

[Pxx,f]=mtmspec(x,nf,0);
fminindx=find(f>=fmin,1,'first');
logf=log(f);
logfstep=logf(fminindx+1)-logf(fminindx);%this will be smallest valid step
logfend=logf(end);
logfstart=logf(fminindx);

logfbinedges=(logfstart:logfstep:logfend+logfstep)';

for i = 1:length(logfbinedges)-1
    lb=logfbinedges(i);
    ub=logfbinedges(i+1);
    
    q = find(logf > lb & logf<=ub);
    
    PxxBinned(i,:) = [exp(nanmean(log(Pxx(q,:)),1))];
    logfbins(i,1)= (ub+lb)/2; %midpoint
end

LogPxxBinned=log(PxxBinned);
yint=ones(size(logfbins));
[b,bint,R,Rint,STATS]=regress(LogPxxBinned,[yint logfbins]);
Beta=b(2);
PxxPred=exp(b(1)+logfbins*b(2));
Rsq=STATS(1);
Fstat=STATS(2);
pval=STATS(3);
Err=STATS(4);

fbinned=exp(logfbins);