function SpecSummary3D=CalcBetaSpecMTM3D(X3D,f,fbinedges,fmin,fcut);
% SpecSummary3D=CalcBetaSpecMTM3D(X3D,f,fbinedges,fmin,fcut);
[nx,ny,nt]=size(X3D);

RandSpec=CalcBetaSpecMTM(randn(nt,1),f,fbinedges,fmin,fcut);	


for i=1:nx
    for j=1:ny;
        x=squeeze(X3D(i,j,:));        
        if sum(~isnan(x))>0
            SpecSummary=CalcBetaSpecMTM(x,f,fbinedges,fmin,fcut);
            SpecSummary3D.Pxx(i,j,:)=SpecSummary.Pxx;
            SpecSummary3D.PxxBinned(i,j,:)=SpecSummary.PxxBinned;
            SpecSummary3D.fq=SpecSummary.fq;
            SpecSummary3D.fql=SpecSummary.fql;
            SpecSummary3D.fqh=SpecSummary.fqh;
            SpecSummary3D.Beta(i,j)=SpecSummary.Beta;
            SpecSummary3D.Betalf(i,j)=SpecSummary.Betalf;
            SpecSummary3D.Betahf(i,j)=SpecSummary.Betahf;
            SpecSummary3D.PxxPred(i,j,:)=SpecSummary.PxxPred;
            SpecSummary3D.PxxPredlf(i,j,:)=SpecSummary.PxxPredlf;
            SpecSummary3D.PxxPredhf(i,j,:)=SpecSummary.PxxPredhf;
            SpecSummary3D.allfrqs{i,j}=SpecSummary.allfrqs;
            SpecSummary3D.lf{i,j}=SpecSummary.lf;
            SpecSummary3D.hf{i,j}=SpecSummary.hf;
        else

            SpecSummary3D.Pxx(i,j,:)=RandSpec.Pxx*nan;
            SpecSummary3D.PxxBinned(i,j,:)=RandSpec.PxxBinned*nan;
            SpecSummary3D.fq=RandSpec.fq*nan;
            SpecSummary3D.fql=RandSpec.fql*nan;
            SpecSummary3D.fqh=RandSpec.fqh*nan;
            SpecSummary3D.Beta(i,j)=RandSpec.Beta*nan;
            SpecSummary3D.Betalf(i,j)=RandSpec.Betalf*nan;
            SpecSummary3D.Betahf(i,j)=RandSpec.Betahf*nan;
            SpecSummary3D.PxxPred(i,j,:)=RandSpec.PxxPred*nan;
            SpecSummary3D.PxxPredlf(i,j,:)=RandSpec.PxxPredlf*nan;
            SpecSummary3D.PxxPredhf(i,j,:)=RandSpec.PxxPredhf.*nan;
            SpecSummary3D.allfrqs{i,j}=RandSpec.allfrqs.STATS*nan;
            SpecSummary3D.lf{i,j}=RandSpec.lf.STATS.*nan;
            SpecSummary3D.hf{i,j}=RandSpec.hf.STATS.*nan;
        end    
    end
end