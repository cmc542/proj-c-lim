function [nmbrout]=trunc8(nmbr,nsig);

d=10^nsig;
nmbrout=round(nmbr*d)./d;

