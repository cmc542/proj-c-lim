function [Gmtx,fout,Per,Lower95,Upper95]=specbt1mtx(X,f,M);
%  [Gmtx,fout,Per,Lower95,Upper95]=specbt1mtx(X,f,M);
%sets up loop to call specbt1, uses only inputs of X (a matrix), f
%(a vector of freuencies), and M (a scalar).


%some parameters
ns=size(X,2);
prevwind='nothing';
sername='nothing';
kopt=2;
tlab='nothing';

for i =1:ns;
    z=X(:,i);
    z=z-nanmean(z);
    z(isnan(z))=0;

    
    G=specbt1(z,M,f,prevwind,sername,kopt,tlab);
    Gmtx(:,i)=G(:,3);
    Lower95(:,i)=G(:,4);
    Upper95(:,i)=G(:,5);
end

fout=G(:,1);
Per=G(:,2);

