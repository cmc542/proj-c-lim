function prnt_sv(figname,res)
% prnt_sv(figname,res)
%
% figname - string with the desired name of the figure
% res - desired resolution (also a string)

set(gcf,'paperpositionmode','auto')
print([figname '.jpg'],'-djpeg',['-r' res]);
saveas(gca,[figname '.fig'],'fig');
