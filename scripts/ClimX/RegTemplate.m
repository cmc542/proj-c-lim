%Script to do the following (4/30/2008):
%1) Load NCEP pressure pressure data
%2) Regress/correlate PC1 (from SI eof analysis - see loadSI.m);
%3) Map correlation/regression coefficients out in space
%4) Do EOF analsis on pressure pressure data for a specified month.

%First, load Soil Moisture data from NCEP:
clear
close all

PC1data = load('X9PC1data.dat');
PC1data(:,2) =-PC1data(:,2);
Surf = loaddap('http://iridl.ldeo.columbia.edu/expert/SOURCES/.NOAA/.NCEP-NCAR/.CDAS-1/.MONTHLY/.Diagnostic/.surface/.temp/X/%28125W%29%28100W%29RANGEEDGES/Y/%2820N%29%2860N%29RANGEEDGES/yearly-anomalies/T/%28Jan%201949%29%28Dec%202007%29RANGE/dods');
load zcolormap;

%Some Generally useful variables:
Surf.temp.temp(Surf.temp.temp >10000) = nan;
nlon = length(Surf.X);
nlat = length(Surf.Y);
nt = length(Surf.T);
Time = 1960 + Surf.T/12;
load ~/Matlab/PDSI/PDSIColors

%% August Only
figure(1)
clf
m_proj('mercator', 'latlim',[30 60],'lonlim',[-126 -100]);
%Define the month over which regression/correlation analysis
MonInd = 8;
months =Time(MonInd:12:end);
Years = floor(months);
[ovr,Ind1,Ind2] = intersect(PC1data(:,1),Years);
nm = length(months);
 
pressure_1mo = rm_mtx_mean(reshape(Surf.temp.temp(:,:,MonInd:12:end),nlon*nlat,nm)');
[SurfHeat_reg,Resid,C,P] = FieldReg(PC1data(Ind1,2),pressure_1mo(Ind2,:));
Reg_PC1toSurfHeat = reshape(SurfHeat_reg(1,:),nlat,nlon,1);
P3D = reshape(P,nlat,nlon,1);
C2D = reshape(C,nlat,nlon,1);
m_elev('contour',[0:500:4000],'linecolor','k')
hold on
m_contourf(Surf.X,Surf.Y,C2D,'linestyle','none');
hold on
m_contour(Surf.X,Surf.Y,P3D,[.01 .01],'linecolor','k','LineWidth',2)
m_grid('linestyle','none','tickdir','out','linewidth',1,'xticklabel','','yticklabel','','box','on');
m_coast('color','k')
colormap(flipud(zcolormap))
caxis([-1 1])
colorbar


%% Figure 2 - all months
figure(2)
clf
Col1Left = 0.14;
ColWidth = 0.15;
Row1Bottom = 0.68;
RowHeight = 0.315;
nrows = 3;
ncols = 4;

lone = -100;
lonw = -125;
latn = 55;
lats = 25;

ColLeft = [Col1Left:ColWidth:ColWidth*ncols];
RowBottom = [Row1Bottom:-RowHeight:(Row1Bottom+RowHeight-RowHeight*nrows)]';
Months = 'JFMAMJJASOND';
k = 0;
MonInd = 0;
for i = 1:nrows;
    for j = 1:ncols;       
        k = k+1;
        
        h(k) = axes('position',[ColLeft(j) RowBottom(i) ColWidth RowHeight]);
        
        %% Analysis
        MonInd = MonInd+1;
        months =Time(MonInd:12:end);
        Years = floor(months);
        [ovr,Ind1,Ind2] = intersect(PC1data(:,1),Years);
        nm = length(months);

        pressure_1mo = rm_mtx_mean(reshape(Surf.temp.temp(:,:,MonInd:12:end),nlon*nlat,nm)');
        [SurfHeat_reg,Resid,C,P] = FieldReg(PC1data(Ind1,2),pressure_1mo(Ind2,:));
        Reg_PC1toSurfHeat = reshape(SurfHeat_reg(1,:),nlat,nlon,1);
        P3D = reshape(P,nlat,nlon,1);
        C2D = reshape(C,nlat,nlon,1);
        m_contourf(Surf.X,Surf.Y,C2D,'linestyle','none');
        hold on
        m_contour(Surf.X,Surf.Y,P3D,[.01 .01],'k','LineWidth',1)        
        %%%%%%%%%

        m_coast('Color',[0 0 0],'LineWidth',.5); 
        m_grid('linestyle','none','tickdir','out','linewidth',1,'xticklabel','','yticklabel','','box','on');
        m_text(-110, 50,Months(k),'FontSize',12,'FontWeight','bold');        
        caxis([-1 1])
   
    end
end
c = colorbar;
set(c,'pos',[0.7663    0.1976    0.0587    0.6417])
colormap(flipud(zcolormap))

print -djpeg -r300 'spr-pc1-temp-WNA.jpg'