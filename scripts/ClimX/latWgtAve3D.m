function wgtdAve=latWgtAve3D(lon,lat,data)
%wgtdAve=latWgtAve3D(lon,lat,data)

[nx,ny,nt]=size(data);
if length(lon) ~=nx;
    error('length(lon) ~=nx')
end

if length(lat) ~=ny;
    error('length(lon) ~=ny')
end

[~,wgts3D] = wgtmtx(lat,nx,1);
gridmask=ones(size(data(:,:,1)));
gridmask(isnan(data(:,:,1)))=nan;

wgtSum=nansum(nansum(wgts3D.*gridmask));

wgtdAve=nan(nt,1);
for i =1:nt
    wgtdAve(i,1)=nansum(nansum(gridmask.*wgts3D.*data(:,:,i)))./wgtSum;
end

