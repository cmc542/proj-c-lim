function [Xanom3D, Seas3D] = rm_ann_cyc(X3D,nseas);
% [Xanom3D, Seas3D] = rm_ann_cyc(X3D,nseas);

dim=size(X3D);
X=reshape(X3D,dim(1)*dim(2),dim(3))';


nt = size(X,1);
Xanom = nan(size(X));
for i = 1:nseas
    Mu = nanmean(X(i:nseas:end,:));
    Seas(i,:) = Mu;
    Xanom(i:nseas:end,:) = X(i:nseas:end,:)-repmat(Mu,nt/nseas,1);
end

Xanom3D=reshape(Xanom',dim(1),dim(2),dim(3));
Seas3D=reshape(Seas',dim(1),dim(2),nseas);