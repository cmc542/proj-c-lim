function gridmask=landmask(lon,lat)
%gridmask=landmask(lon,lat)

if any(lon>180)
    lon=lon360_180(lon);
end

[lonmtx,latmtx]=meshgrid(lon,lat);
lonedges=[min(lon) max(lon)];
latedges=[min(lat) max(lat)];
lonstr=[num2str(lonedges(1)) '/' num2str(lonedges(2))];
latstr=[num2str(latedges(1)) '/' num2str(latedges(2))];
%url=['http://iridl.ldeo.columbia.edu/expert/SOURCES/.UEA/.CRU/.TS2p1/.monthly/.elev/X/' lonstr '/RANGE/Y/' latstr '/RANGE/dup/div/dods'];
%loaddap(url)
elev.elev=ncgetvar('CRU_TS2.1_elev.nc','elev')';

elev.X=ncgetvar('CRU_TS2.1_elev.nc','X');
elev.Y=ncgetvar('CRU_TS2.1_elev.nc','Y');
mask1=elev.elev;
[Xmtx,Ymtx]=meshgrid(elev.X,elev.Y);
gridmask=interp2(Xmtx,Ymtx,mask1,lonmtx,latmtx);

gridmask(gridmask<0)=nan;
gridmask=gridmask./gridmask;
