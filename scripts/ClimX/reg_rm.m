function [Yresid, Ystats]=reg_rm(x,Y,varargin)
% [Yresid, Ystats]=reg_rm(x,Y,varargin)
% Y may be 1D, 2D or 3D

ndims=size(Y);

dimx=size(x);
if dimx(2)> dimx(1)
    x=x';
end
onevec=ones(size(x));

if length(ndims)==2 && ndims(2)==1;
    [b,Ystats{1}.bint,Ystats{1}.r,Ystats{1}.rint,Ystats{1}.stats]=regress(Y,[x onevec]);
    ypred=b(1)*x + b(2);
    Yresid=Y-ypred;
elseif length(ndims)==2 && ndims(2)>1;
    Yresid = nan(size(Y));
    for i =1:ndims(2)
        [b(i,:),Ystats{i}.bint,Ystats{i}.r,Ystats{i}.rint,Ystats{i}.stats]=regress(Y(:,i),[x onevec]);
        ypred=b(i,1)*x + b(i,2);
        Yresid(:,i)=Y(:,i)-ypred;
    end
elseif  length(ndims)==3 && ndims(3)==length(x)
    Yresid = nan(size(Y));
    for i =1:ndims(1);
        for j=1:ndims(2)
            y=squeeze(Y(i,j,:));
            if ~all(isnan(y))
                [b(i,j,:),Ystats{i,j}.bint,Ystats{i,j}.r,Ystats{i,j}.rint,Ystats{i,j}.stats]=regress(y,[x onevec]);
                ypred=b(i,j,1)*x + b(i,j,2);
                Yresid(i,j,:)=y-ypred;
            end
        end
    end
end
    
    