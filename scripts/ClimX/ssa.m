function [ssadata] = ssa(x,m)

%Written 12/04 by Toby Ault
%Last Modified: 3/28/06
%Assisted by Mike Evans
%SSA performs singular spectrum analysis on specified time series, x with a
%window m. Technique described by Ghil et al. 
%
%NOTE: This method can only handle missing values at the ENDS of the time series
%INTERNAL MISSING VALUES WILL PRODUCE ERRORS!!!! Use nanssa.m instead.
%
%Inputs:
%SSA(TIME SERIES, WINDOW SIZE M)
%
%Outputs:
%       ssadata.eig.........eigenvalues
%       ssadata.E...........eigenvectors
%       ssadata.RC..........Reconstructed Components
%       ssadata.anom........anomaly
%       ssadata.frqs........frequencies (approximate) of reconstructed components
%       ssadata.p...........phase (approximate) of RCs
%       ssadata.quads.......Index of pairs in quadrature

if size(x,2) > size(x,1)
    x = x';
else end

x_anom = x - nanmean(x); %Remove mean
q = find(~isnan(x_anom));
x_anom = x_anom(q);
xnan = nan(size(x));
RCnan = nan(length(x),m);

nt = length(x_anom);
N = nt-m+1;
nf = nt*4;
uf = ceil(nf/2);
f = [0:nf-1]/nf;

for t = 1:m
    X(:,t)= x_anom(t:t+N-1);
end

%Broomhead and King method (much faster):
C = (X'*X)./(N);
%C = cov(X);

%Toeplitz method:
% for i = 1:m
%     for j = 1:m
%         Denom = [nt-abs(i-j)];
%         for t = 1:nt-abs(i-j)
%             temp(t) = [x_anom(t)*x_anom(t+abs(i-j))]./Denom;
%         end
%         C(i,j) = sum(temp)./Denom;
%     end
% end

[E, V, E2] = svd(C);
eig = diag(V);

Z = X;
Z(find(isnan(Z)))=0;
A = Z*E;

RC=zeros(length(A)+m-1,m)*NaN;
for i=1:m
  RC(:,i)=conv(A(:,i),E(:,i));
  %RC=conv2(A,E);
end

for i=1:nt
  if i<=m-1,
    RC(i,:)=RC(i,:)./i;
  elseif i>=m && i<=nt-m+1,
    RC(i,:)=RC(i,:)./m;
  elseif i>=nt-m+2 && i<=nt,
    RC(i,:)=RC(i,:)./(nt-i+1);
  end
end

dftRC = fft(RC,nf);
[val,pos] = max(abs(dftRC(1:uf,:)));
frqs = f(pos);
frqs(find(frqs <= 1/nf))=1./length(x);

newpos =[(1:length(pos))' pos'];
posind = sub2ind(size(dftRC),newpos(:,2),newpos(:,1));
p = angle(dftRC(posind));
l = abs(dftRC(posind));
cplx = dftRC(posind);
%quads = quadtest(eig,frqs);

xnan(q) = x_anom;
x_anom = xnan;
RCnan(q,:) = RC;
RC = RCnan;

ssadata.eig = eig;
ssadata.normeig = eig/sum(eig);
ssadata.sv = sqrt(eig);
ssadata.E = E;
ssadata.RC = RC;
ssadata.x_anom = x_anom;
ssadata.frqs = frqs;
%ssadata.quads = quads;
ssadata.p = p;
ssadata.l = l;
ssadata.cplx = cplx;
ssadata.X = X;
ssadata.C = C;

% function [ind] = quadtest(eigvals,frqs);
% %Check for quadrature pairs.
% %
% %"ind" is of the form: [p1 p2;
% %                        .
% %                        .
% %                        .
% %                       pn pn+1] 
% %
% %where pn and pn+1 are the two RC's in quadtrature.
% 
% eigvals = eigvals/sum(eigvals);
% 
% diff1 = eigvals(1:end-1)-eigvals(2:end);
% diff2 = abs([1./(frqs(1:(end-1)))]-[1./(frqs(2:end))]);
%     
% ind1 = find(diff1 < 0.02);
% ind2 = find(round(diff2) <= 1);
% 
% ind = intersect(ind1,ind2);
% ind = [ind; ind+1]';
% return
