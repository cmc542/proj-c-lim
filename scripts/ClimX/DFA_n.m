function [D,a1,F_n]=DFA_n(DATA,n,order)
%[D,a1]=DFA_main(DATA)
% DATA should be a time series of length(DATA) greater than 2000,and of column vector.
% A is the alpha in the paper
% D is the dimension of the time series
% n can be changed to your interest

% DFA script (see below) modified from Matlab file exhange
% 
% The referrenced paper is attached here.
% DATA1.mat contains the sample data of Weierstrass 
%function of length 5000 between 0 to 1 of dimension 1.75
% 
% After setting the current directory and loading the 
% sampled data in matlab,in command window,input:
%  [D,Alpha]=DFA_main(DATA1)
%  
% which gives the dimension and alpha 
% (Estimated scaling exponent) of the time series DATA1.
% 
% Made by:Guan Wenye
% contact:guanwenye@tju.edu.cn
% 

N1=length(n);
F_n=zeros(N1,1);
for i=1:N1
 F_n(i)=DFA(DATA,n(i),order);
end

A=polyfit(log(n(1:end)),log(F_n(1:end)),1);
a1=A(1);
D=3-A(1);
return

function [output1,fitcoef,Yn]=DFA(DATA,win_length,order)
% output1=DFA(DATA,win_length,order)

N=length(DATA);   
n=floor(N/win_length);
N1=n*win_length;
y=zeros(N1,1);
Yn=zeros(N1,1);

fitcoef=zeros(n,order+1);
mean1=mean(DATA(1:N1));
for i=1:N1
    y(i)=sum(DATA(1:i)-mean1);
end
y=y';
for j=1:n
    fitcoef(j,:)=polyfit(1:win_length,y(((j-1)*win_length+1):j*win_length),order);
end

for j=1:n
    Yn(((j-1)*win_length+1):j*win_length)=polyval(fitcoef(j,:),1:win_length);
end

sum1=sum((y'-Yn).^2)/N1;
sum1=sqrt(sum1);
output1=sum1;
          