function yr_YYYYMM =yrfract2YYYYMM(yr)
%yr_YYYYMM =yrfract2YYYYMM(yr)
yr_YYYYMM=[];

sigfig=100000;
yrdec=round(sigfig*yr);
yrint=floor(yr)*100;
for i =1:length(yrdec);
   if ([yr(i) - floor(yr(i))]) < 1/12;
       moint=1;
   elseif ([yr(i) - floor(yr(i))]) >= 1/12 & ([yr(i) - floor(yr(i))]) < 2/12;
       moint=2;
   elseif ([yr(i) - floor(yr(i))]) >= 2/12 & ([yr(i) - floor(yr(i))]) < 3/12;
       moint=3;
   elseif ([yr(i) - floor(yr(i))]) >= 3/12 & ([yr(i) - floor(yr(i))]) < 4/12;
       moint=4;
   elseif ([yr(i) - floor(yr(i))]) >= 4/12 & ([yr(i) - floor(yr(i))]) < 5/12;
       moint=5;
   elseif ([yr(i) - floor(yr(i))]) >= 5/12 & ([yr(i) - floor(yr(i))]) < 6/12;
       moint=6;
   elseif ([yr(i) - floor(yr(i))]) >= 6/12 & ([yr(i) - floor(yr(i))]) < 7/12;
       moint=7;
   elseif ([yr(i) - floor(yr(i))]) >= 7/12 & ([yr(i) - floor(yr(i))]) < 8/12;
       moint=8;
   elseif ([yr(i) - floor(yr(i))]) >= 8/12 & ([yr(i) - floor(yr(i))]) < 9/12;
       moint=9;
   elseif ([yr(i) - floor(yr(i))]) >= 9/12 & ([yr(i) - floor(yr(i))]) < 10/12;
       moint=10;
   elseif ([yr(i) - floor(yr(i))]) >= 10/12 & ([yr(i) - floor(yr(i))]) < 11/12;
       moint=11;
   elseif ([yr(i) - floor(yr(i))]) >= 11/12 & ([yr(i) - floor(yr(i))]) < 12/12;
       moint=12;       
   end
   
  yr_YYYYMM(i,1)=(yrint(i)+moint);
end