function time= timefromfilename(filename)
%time= timefromfilename(filename)

[varname,realm,modid,expid,runid,yrStart,moStart,yrEnd,moEnd]=filename2fileinfo(filename);
time=(yrStart+(moStart-.5)./12):1/12:(yrEnd+(moEnd-.5)./12);
