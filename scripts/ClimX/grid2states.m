function [axm,cbarax,states,state_color,state_ax]=grid2states(griddeddata,lon,lat,cmap,colorax)
%[axm,states,state_color,state_ax]=grid2states(griddeddata,lon,lat,cmap,colorax)

% data must be lon x lat
if size(griddeddata,1)==length(lat);
    griddeddata=griddeddata';
end

axm=usamap('conus');
states = shaperead('usastatehi', 'UseGeoCoords', true);
hold on

for i =1:length(states);    
    lonedges=states(i).BoundingBox(:,1);
    latedges=states(i).BoundingBox(:,2);
    
    lonq=find(lon>lonedges(1) & lon<=lonedges(2));
    latq=find(lat>latedges(1) & lat<=latedges(2));
    state_grid_index=false(length(lon),length(lat));
    
    for k =1:length(lonq);
        for j =1:length(latq);
            state_grid_index(lonq(k),latq(j))=inpolygon(lon(lonq(k)),lat(latq(j)),states(i).Lon,states(i).Lat);
            
        end
    end
    state_ave=nanmean(griddeddata(state_grid_index));
    state_stddev=nanstd(griddeddata(state_grid_index));
    
    %riskstr=[num2str(num2str(round(state_ave-state_stddev))) '-' num2str(num2str(round(state_ave+state_stddev))) '%'];
    riskstr=[num2str(num2str(round(state_ave)))  '%'];
    if state_ave <5 | isnan(state_ave)
        riskstr='<5%';
    end
    
    disp([states(i).Name ': ' riskstr ])
    colorindx=round(state_ave/colorax(2)*length(cmap));
    colorindx(colorindx<1)=1;
    colorindx(colorindx>length(cmap))=64;
    
    if ~strcmp(states(i).Name,'District of Columbia') & ~isnan(colorindx)
        state_color(i,:)=cmap(colorindx,:);
        
        a=geoshow(states(i),'FaceColor',state_color(i,:),'edgecolor','k');
        if ~isempty(a)
            state_ax(i)=a;
        else
            state_ax(i)=nan;
        end

    end
end
colormap(cmap)
cbarax=colorbar('horiz');
caxis(colorax);

%%
setm(axm,'plinevisible','off')
setm(axm,'mlinevisible','off')
setm(axm,'grid','off')
setm(axm,'frame','off')
setm(axm,'meridianlabel','off')
setm(axm,'parallellabel','off')
