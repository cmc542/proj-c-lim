
delete_ax=axes('pos',[0 0 1 1]);
axis([0 100 0 100]);
set(gca,'xtick',[0:10:100],'ytick',0:10:100);
set(gca,'color','none')
grid on

for i =1:10;
    text((i-1)*10+7,97,num2str(i/10));
     text(1,(i-1)*10+8,num2str(i/10));
end