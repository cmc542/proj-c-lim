function day_of_year=doy;

day_of_year=floor(datenum(now)-datenum(datestr(now,'YYYY'),'YYYY'));