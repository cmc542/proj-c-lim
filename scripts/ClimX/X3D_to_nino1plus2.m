function nino1=X3D_to_nino1plus2(sst,lon,lat)
% nino1=X3D_to_nino1(sst,lon,lat)
% nino1:  0-10S, 90W-80W
if any(lon<0)
    lon=lon180_360(lon);
end
nino1lons=[270   280];
nino1lats=[-10 0];
qnn34lon= lon>nino1lons(1) & lon<=nino1lons(2);
qnn34lat= lat>nino1lats(1) & lat<=nino1lats(2);

sst=permute(sst,[find(size(sst)==length(lon)) find(size(sst)==length(lat)) 3]);
nino1=squeeze(mean(mean(sst(qnn34lon,qnn34lat,:))));
