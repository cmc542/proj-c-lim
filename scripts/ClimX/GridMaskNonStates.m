function GridMask=GridMaskNonStates(griddeddata,lon,lat);
%GridMask=GridMaskNonStates(griddeddata,lon,lat);

% data must be lon x lat
if size(griddeddata,1)==length(lat);
    griddeddata=griddeddata';
end

states = shaperead('usastatelo', 'UseGeoCoords', true);
GridMask=false(length(lon),length(lat));

for i =1:length(states);    
    lonedges=states(i).BoundingBox(:,1);
    latedges=states(i).BoundingBox(:,2);
    
    lonq=find(lon>lonedges(1) & lon<=lonedges(2));
    latq=find(lat>latedges(1) & lat<=latedges(2));
    for k =1:length(lonq);
        for j =1:length(latq);
            [IN,ON]=inpolygon(lon(lonq(k)),lat(latq(j)),states(i).Lon,states(i).Lat);
            if IN | ON
                
                GridMask(lonq(k),latq(j))=true;
            end
            
        end
    end
end

GridMask=GridMask*1.0;
GridMask(GridMask==0)=nan;