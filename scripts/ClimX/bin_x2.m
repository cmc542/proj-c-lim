function [bin_mean, bin_sum,BinTime] =  bin_x2(Xtime,X,bins);
%[bin_mean, bin_sum, BinTime] =  bin_x2(Xtime,X,bins);


binstep = bins(2)-bins(1);

for i = 1:length(bins)-1
    q = find(Xtime >= bins(i) & Xtime<bins(i+1));
    
    bin_mean(i,:) = [nanmean(X(q,:),1)];
    bin_sum(i,:)  = [nansum(X(q,:),1)];
    BinTime(i,1)= nanmean(Xtime(q));
end

i=i+1;
bin_mean(i,:) = nan(1,size(X,2));