function C2D = cov3D(X3D)
%Last dimensions should be time
[nx,ny,nz] = size(X3D);
C2D = nan(nx*ny,nx*ny);

X2D = reshape(X3D,nx*ny,nz)';
p = sum(~isnan(X2D));
q = ~~p;
X2Dq=X2D(:,q);
C2Dq=nancov(X2Dq);

C2Dq1=nan(sum(q),ny*nx);
C2Dq1(:,q)=C2Dq;
C2D(q,:)=C2Dq1;
