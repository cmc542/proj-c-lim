function prod=mulvec(X);
% prod=mulvec(X);
for j=1:size(X,2);
    xi=X(1,j);
    for i=2:size(X,1);
        xi=xi*X(i,j);
    end
    prod(1,j)=xi;
end

