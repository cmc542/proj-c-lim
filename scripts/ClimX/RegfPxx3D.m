function [Beta2D,Pxx3D,f,fbinned,PxxBinned3D,PxxPred3D,Rsq2D,Fstat2D,pval2D,Err2D]=RegfPxx3D(X3D,nf,fmin)
% [Beta2D,Pxx3D,f,fbinned,PxxBinned3D,PxxPred3D,Rsq2D,Fstat2D,pval2D,Err2D]=RegfPxx3D(X3D,nf,fmin)
%
% input:
% X3D - a 3D array of data: must be X-Y-TIME (X/Y could be lat or lon).
% nf - number of fourier frequencies to use in calculating the power
%      spectum (usually 2*length)
% fmin - low-frequency cutoff (usually 1/(N/2))
%
% output:
%
% Beta2D - map of power laws (NOT multiplied by -1)
% Pxx3D - 3D array of spectral densities
% fbinned - binned frequency bands used for calculating power laws
% PxxBinned - spectral densities
% PxxPred3D - Linear prediction of spectral slope
% Rsq2D - map of R^2 (goodness of fit) for frq to Pxx
% Fstat2D - map of F-statistic
% pval2D - map of p-values
% Err2D - map of error estimates.

[nx,ny,nt]=size(X3D);
[Beta,Pxx,f,fbinned,PxxBinned,PxxPred,Rsq,Fstat,pval,Err]=RegfPxx(randn(nt,1),nf,fmin);

Pxx3D=nan(nx,ny,length(f));
PxxBinned3D=nan(nx,ny,length(fbinned));
Beta2D=nan(nx,ny);
PxxPred3D=nan(nx,ny,length(fbinned));
Rsq2D=nan(nx,ny);
Fstat2D=nan(nx,ny);
pval2D=nan(nx,ny);
Err2D=nan(nx,ny);

for i=1:nx
    for j=1:ny
        x=squeeze(X3D(i,j,:));
        nnan=sum(isnan(x));
            if nnan < .1*nt                
                [Beta2D(i,j),Pxx3D(i,j,:),f,fbinned,PxxBinned3D(i,j,:),PxxPred3D(i,j,:),Rsq2D(i,j),Fstat2D(i,j),pval2D(i,j),Err2D(i,j)]=RegfPxx(x,nf,fmin);    
            end
    end
end