function  SpecSummary=PxxfReg(f,Pxx,fN,fbins,fmin,fcut);
% PxxfReg(f,Pxx,fN,fbins,fmin,fcut);


% Log transform pxx, f:
logPxx=log(Pxx);
logf=log(f);

%% now set up regression I: entire x-domain (fmin to fN)
fq=find(fbins>=fmin & fbins<fN);
logfbinsq=log(fbins(fq));

% bin logPxx
logPxxBinned=bin_x2(f,logPxx,fbins(fq));

%Finally, do regression:
yint=ones(size(fq));
[allfrqs.b,allfrqs.bint,allfrqs.R,allfrqs.Rint,allfrqs.STATS]=regress(logPxxBinned,[yint logfbinsq]);

%% now set up regression II: low-frequency domain (fmin to fcut)
fql=find(fbins>=fmin & fbins<fcut);
logfbinsql=log(fbins(fql));

% bin logPxx
logPxxBinnedql=bin_x2(f,logPxx,fbins(fql));


%Finally, do regression:
yint=ones(size(fql));
[lf.b,lf.bint,lf.R,lf.Rint,lf.STATS]=regress(logPxxBinnedql,[yint logfbinsql]);



%% now set up regression III: high-frequency domain (fcut to fN)
fqh=find(fbins>=fcut & fbins<fN);
logfbinsqh=log(fbins(fqh));


% bin logPxx
logPxxBinnedql=bin_x2(f,logPxx,fbins(fqh));

%Finally, do regression:
yint=ones(size(fqh));
[hf.b,hf.bint,hf.R,hf.Rint,hf.STATS]=regress(logPxxBinnedql,[yint logfbinsqh]);



% Generate output structure:
PxxBinned=exp(logPxxBinned);
SpecSummary.Pxx=Pxx;
SpecSummary.PxxBinned=PxxBinned;
SpecSummary.fq = fq;
SpecSummary.fql = fql;
SpecSummary.fqh = fqh;
SpecSummary.Beta=-allfrqs.b(2);
SpecSummary.Betalf=-lf.b(2);
SpecSummary.Betahf=-hf.b(2);

SpecSummary.PxxPred=exp(allfrqs.b(1)+logfbinsq*allfrqs.b(2));
SpecSummary.PxxPredlf=exp(lf.b(1)+logfbinsql*lf.b(2));
SpecSummary.PxxPredhf=exp(hf.b(1)+logfbinsqh*hf.b(2));

SpecSummary.allfrqs=allfrqs;
SpecSummary.lf=lf;
SpecSummary.hf=hf;
