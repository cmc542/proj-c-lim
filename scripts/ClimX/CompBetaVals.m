function [BetaMap,xPxx3D,xf,PxxPred3D]=CompBetaVals(X3D,nf,fdomain)
% [BetaMap,xPxx3D,xf,PxxPred3D]=CompBetaVals(X3D,nf,fdomain)

dim=size(X3D);
Xresh=reshape(X3D,dim(1)*dim(2),dim(3))';
q=sum(~isnan(Xresh))>0;
Xmtx=Xresh(:,q);

[xPxx,xf]=mtmspec(Xmtx,nf,0);

f_valid=xf> fdomain(1) & xf<=fdomain(2);

logPxx=log(xPxx(f_valid,:));
logf=log(xf(f_valid));
PxxPred=nan(size(xPxx));

for i =1:size(logPxx,2);
	b=regress(logPxx(:,i),[logf ones(size(logf))]);
        BetaVals(i)=b(1);
     
    PxxPred(~f_valid,i)=nan;
    PxxPred(f_valid,i)=exp(log(xf(f_valid))*b(1)+(b(2)));

end

%% Outputs
Betaq=nan(dim(1)*dim(2),1);
Betaq(q)=BetaVals;
BetaMap=reshape(Betaq,dim(1),dim(2));

xPxxq=nan(dim(1)*dim(2),length(xf))';
xPxxq(:,q)=xPxx;
xPxx3D=reshape(xPxxq',dim(1),dim(2),length(xf));

PxxPredq=nan(dim(1)*dim(2),length(xf))';
PxxPredq(:,q)=PxxPred;
PxxPred3D=reshape(PxxPredq',dim(1),dim(2),length(xf));
