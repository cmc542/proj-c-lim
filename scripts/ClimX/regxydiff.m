function [pval,xstat,ystat,yrstat]=regxydiff(t,x,y)
%Usage:
% [pval,xstat,ystat,yrstat]= regxydiff(t,x,y)
%
% Tests if x and y have siginificantly different linear trends by first
% calculating the slope of x, then subtracting it from y, and calculating
% the p-value of the residual.
%
% pval = 1 slopes NOT similar
% pval = 0 slopes ARE similar


[xstat.b,xstat.bint,xstat.r,xstat.rint,xstat.stat]=regress(x,[t ones(size(t))]);
[ystat.b,ystat.bint,ystat.r,ystat.rint,ystat.stat]=regress(x,[t ones(size(t))]);
xpred=t*xstat.b(1)+xstat.b(2);
yprime=y-xpred;
[yrstat.b,yrstat.bint,yrstat.r,yrstat.rint,yrstat.stat]=regress(yprime,[t ones(size(t))]);

pval=1-yrstat.stat(:,3);