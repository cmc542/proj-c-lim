function [EDotSize,LimRatio] = ScaleDots(MarkerData,MinMax)
% n [EDotSize,LimRatio] = ScaleDots(MarkerData,MinMax)
%Usage: ScaleDots(MarkerData,MinMax);


%Min/Max:
minE = abs(MinMax);
maxE = -minE;
%Define boundaries for plotting
SizeCoeff = 6;
MinSize = 3;
MaxSize = SizeCoeff + MinSize;
MaxLim = max(abs([minE maxE])); %maximum absolute value in minE maxE limits
LimRatio = abs(MarkerData)/MaxLim;       %relative portion of each value in MarkerData with respect to MaxLim
% LimRatio = nan(size(MarkerData));
% LimRatio(find(MarkerData <0)) = abs((MarkerData(find(MarkerData<0)))/(minE)); 
% LimRatio(find(MarkerData >= 0)) = MarkerData(find(MarkerData >=0))./maxE;

EDotSize = LimRatio*SizeCoeff+MinSize; %Size of individual markers
EDotSize(find(EDotSize ==0))= MinSize; %Make ith marker VERY small ('MarkerSize',1) if MarkerData(i) happens to be zero.
EDotSize(find(LimRatio > 1)) = MaxSize; %Make with a size greater than maxE equal to MaxSize;