function SpecSummary=CalcBetaSpecMTM(x,f,fbins,fmin,fcut); 
% SpecSummary=CalcBetaSpecMTM(x,nf,fbins,fmin,fcut);
%Calculates power spectrum and regressions over some portion of that spectrum (from fmin to fcut)


%
%%%%%%%%%%%%   TEMP %%%%%%%%%%%%%%%%%%%
% $$$ clear
% $$$ close all
% $$$  
% $$$ load ~/Matlab/Jam/sstgeorg/TS4scott.txt
% $$$ load fparams
% $$$ % $$$ 
% $$$ 
% $$$ 
% $$$ x=TS4scott(:,2);
% $$$ f=fyr;
% $$$ M=round(length(x)/20);
% $$$  
% $$$ fmin=1/1000;
% $$$ fcut=1/10;
% $$$ fbins=fbinedges;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% First calculate spectrum:
Pxx=mtmspecnorm(x,f,0);
fN=1/2;

% Now pass to PxxfReg.m
SpecSummary=PxxfReg(f,Pxx,fN,fbins,fmin,fcut);
