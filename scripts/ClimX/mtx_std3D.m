function [NormX,ColStd,MtxSigma]  = mtx_std3D(X);
%[NormX,ColStd,MtxSigma]  = mtx_std(X);

X = rm_mtx_mean3D(X);
dim = size(X);
ColStd = nanstd(X,[],3);
MtxSigma = repmat(ColStd,[1 1 dim(3)]);
NormX = X./MtxSigma;
