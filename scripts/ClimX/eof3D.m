function eofdata3D=eof3D(X3D,neig,NormOpt,RotOpt);
%eofdata3D=eof3D(X3D,neig,NormOpt,RotOpt);
%  neig.....................Number of Eigenvectors to compute
%  NormOpt..................Data normalization option:
%                             1-no normalization (Covariance Matrix is used
%                               for SVD)
%                             2-Normalize data (SVD is performed on
%                               correlation matrix)
%  RotOpt...................Rotation (varimax) option (1-yes, 2-no)
%  
%  
%  Outputs:
%   eofdata.E...............Eigenvector matrix (M,neig);
%   eofdata.A...............PC time series matrix (N,neig);
%   eofdata.XAcorr..........Correlation between orginal data (X) and each PC
%   eofdata.CorrSig.........Index (boolean) of points where the correlation
%                           is significant
%   eofdata.X_anom..........Anomaly matrix of X (columnwise mean removed)
%   eofdata.eig.............Eigenvalues
%   eofdata.Method..........Method used (Correlation or Covariance)
%   eofdata.C...............Correlation or covariance matrix (depends on
%                           method)
%   eofdata.Rotation........States whether varimax rotation was applied or
%                           not

[ny,nx,nt]=size(X3D);
X2D=reshape(X3D,nx*ny,nt)';
q=find(sum(~isnan(X2D))>0);
X2Dq=X2D(:,q);

eofdata=limitedeof(X2Dq,neig,NormOpt,RotOpt);
Eq=eofdata.E;
E=nan(nx*ny,neig);
E(q,:)=Eq;
E3D=reshape(E,ny,nx,neig);

XAcorrq=eofdata.XAcorr;
XAcorr=nan(nx*ny,neig);
XAcorr(q,:)=XAcorrq;
XAcorr3D=reshape(XAcorr,ny,nx,neig);

CorrSigq=eofdata.CorrSig;
CorrSig=nan(nx*ny,neig);
CorrSig(q,:)=CorrSigq;
CorrSig3D=reshape(CorrSig,ny,nx,neig);

X_anomq=eofdata.X_anom;
X_anom=nan(nt,nx*ny);
X_anom(:,q)=X_anomq;
X_anom3D=reshape(X_anom',ny,nx,nt);

eofdata3D.E=eofdata.E;
eofdata3D.E3D=E3D;
eofdata3D.A=eofdata.A;
eofdata3D.XAcorr3D=XAcorr3D;
eofdata3D.CorrSig3D=CorrSig3D;
eofdata3D.X_anom3D=X_anom3D;
eofdata3D.eig=eofdata.eig;
eofdata3D.Method=eofdata.Method;
eofdata3D.C=eofdata.C;
eofdata3D.Rotation=eofdata.Rotation;
