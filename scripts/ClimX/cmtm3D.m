function [f, c3D, ph3D, ci3D, phi3D]= cmtm3D(x,X3D,varargin);

[nx,ny,nt]=size(X3D);

[f, c, ph, ci, phi]=cmtm(randn(size(x)),randn(size(x)),varargin);
c3D=nan(nx,ny,length(c));
ph3D=nan(nx,ny,length(ph));
ci3D=nan(nx,ny,length(ci));
phi3D=nan(nx,ny,length(phi));

if sum(isnan(x)<=2)
    q=isnan(x);
    x(q)=nanmean(x);            
    
    for i =1:nx
        for j =1:ny;
            y =squeeze(X3D(i,j,:));


            if sum(isnan(y) <=2);
                q=find(isnan(y));
                y(q)=nanmean(y);
                

                
                [f, c, ph, ci, phi]=cmtm(x,y,varargin);

                c3D(i,j,:)=c;
                ph3D(i,j,:)=ph;
                ci3D(i,j,:)=ci;
                phi3D(i,j,:)=phi;
            end
        end
    end
else
    error('Too many nans in x');
end