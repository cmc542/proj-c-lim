function [tnew,tstruct]=ncgettime(filename,varargin)
%[tnew,tstruct]=ncgettime(filename,varargin)

if nargin==2
    tunits=varargin;
    if strcmp(tunits,'YYYYMM')
        
        tstr=num2str(ncgetvar(filename,'time'));
        tnew=str2num(tstr(:,1:4)) + ((str2num(tstr(:,5:6)))-.5)./12;       
        tstruct.time=tnew;
        %tstruct.date=
    end
else
    time=ncgetvar(filename,'time');
    ncid=netcdf.open(filename,0);
    varID=netcdf.inqVarID(ncid,'time');
    time_units=netcdf.getAtt(ncid,varID,'units');
    time_calendar=netcdf.getAtt(ncid,varID,'calendar');

    qspace=find(time_units==' ');
    [a,b]=regexp(time_units,'\d{1,4}-\d{1,2}-\d{1,2}');
    ref_date=time_units(a:b);

    date_start_raw=datestr(datenum(ref_date,'YYYY-MM-DD')+time(1),'YYYY-DD-MM');

    if (strcmp(time_calendar,'noleap') | strcmp(time_calendar,'365_day') | strcmp(time_calendar,'360_day'))
        nleaps=sum(leapyear(str2num(datestr(datenum(ref_date,'YYYY-DD-MM'),'YYYY')):str2num(datestr(datenum(date_start_raw,'YYYY-DD-MM'),'YYYY'))));    
    else
        nleaps=0;
    end

    date_start=datestr(datenum(ref_date,'YYYY-MM-DD')+time(1)+nleaps,'YYYY-DD-MM');
    time_start=str2num(datestr(datenum(date_start,'YYYY-DD-MM'),'YYYY'));

    tstruct.time=(time_start+(0:length(time)-1)'/12)';
    tnew=tstruct.time;

    if nargout==2
        tstruct.timeraw=time;
        tstruct.date=datestr(datenum(ref_date,'YYYY-MM-DD')+time,'DD-mmm-YYYY');
        tstruct.datenum=datenum(ref_date,'YYYY-MM-DD')+time;
        tstruct.years=str2num(datestr(datenum(ref_date,'YYYY-MM-DD')+time,'YYYY'));
        tstruct.months=str2num(datestr(datenum(ref_date,'YYYY-MM-DD')+time,'mm') );
        tstruct.time_calendar=time_calendar; 
    end

    netcdf.close(ncid)

end