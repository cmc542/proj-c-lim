function [D,a1,F_n]=DFA_3D(DATA3D,n,order)
%[D,a1,F_n]=DFA_3D(DATA3D,n,order)

dim=size(DATA3D);
a1=nan(dim(1),dim(2));
D=nan(dim(1),dim(2));
F_n=nan(dim(1),dim(2),length(n));


for i =1:dim(1)
    for j=1:dim(2)
        x=squeeze(DATA3D(i,j,:));
        nnan=sum(~isnan(x));
        if nnan==length(x)
           [D(i,j),a1(i,j),F_n(i,j,:)]=DFA_n(x,n,order); 
        end
    end
end
