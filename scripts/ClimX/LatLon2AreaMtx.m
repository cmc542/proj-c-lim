function Area2D=LatLon2AreaMtx(lat,lon,latres,lonres);
%Area2D=LatLon2AreaMtx(lat,lon);
% 
% lon=180;
% lat=45;
% 
% lonres=2.5;
% latres=2.5;
% 
lonedges=[lon+lonres/2 lon-lonres/2];
latedges=[lat+lonres/2 lat-latres/2];

h=lonres*111;
b1=cosd(abs(latedges(1)))*111;
b2=cosd(abs(latedges(2)))*111;

Area2D=h*((b1+b2)/2);

