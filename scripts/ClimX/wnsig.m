function [EigSuite, clv99, clv95 clv90, clv80, clvMedian] = wnsig(E,nr)
%Usage: 
%[EigSuite, clv99, clv95 clv90, clv80, median] = wnsig(E,nr)
%
%-Performs Monte Carlo white noise significance test
%
%-Expects structure array output from eof_ann (E) and number of monte carlo
%realizations to perform (nr)

%Display warning if user has requested less than 1000 realizations
if nr <1000
    warning('Number of Realizations <1000, 99% confidence levels are uncertain');
end

%First, get dimensions of Matrix:
[N,M] = size(E.X_anom);

%Now, build a square matrix whos diagonal is the standard deviations of the
%true data
StdMtx = sqrt(diag(diag(nancov(E.X_anom))));

%Check to see if a correlation or covariance matrix was used
if E.Method(1:3) =='Cor';
    NormOpt = 2;
else
    NormOpt =1;
end

% Don't rotate, too slow
RotOpt = 2;

%Number of eigenvalues computed:
neig = length(E.eig);

%Perform the nr Monte Carlo realizatoins
h = waitbar(0,'Performing Monte Carlo Realizations');
for i = 1:nr
    clear SynthX SynthE;
    SynthX = randn(N,M)*StdMtx;
    SynthE = eof_lim(SynthX,neig,NormOpt,RotOpt);
    EigSuite(i,:) = SynthE.eig;
    waitbar(i/nr,h);
end
close(h)

EigSuite = sort(EigSuite);
ind99 = floor(.99*nr);
ind95 = floor(.95*nr);
ind90 = floor(.90*nr);
ind80 = floor(.80*nr);

clv99 = EigSuite(ind99,:);
clv95 = EigSuite(ind95,:);
clv90 = EigSuite(ind90,:);
clv80 = EigSuite(ind80,:);
clvMedian = median(EigSuite);