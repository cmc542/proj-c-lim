function [Pxx,f]=mlpmtm(x,nw,nf,varargin)
 %[Pxx,f]=mlpmtm(x,nw,nf,varargin)

x=rm_mtx_mean(x);
if 0
    x(isnan(x))=0;
else
    x(isnan(x))=[];
end
   
for i =1:size(x,2);
    if nargin==3
       [Pxx(:,i),w]=pmtm(x(:,i),nw,nf); 
    elseif nargin==4
        [Pxx(:,i),w]=pmtm(x(:,i),nw,nf,varargin{1}); 
    end
end

f=w/(2*pi);