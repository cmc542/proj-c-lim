function Map3D=reg3D(X3D)

[nx,ny,nt]=size(X3D);
Map3D=nan(nx,ny);
for i =1:nx
    for j =1:ny
        x=squeeze(X3D(i,j,:));
        

        if (sum(~isnan(x)) == length(x))
            b=regress(x,[(1:nt)' ones(size(x))]);
            Map3D(i,j)=b(1);
        end
        
    end
end
