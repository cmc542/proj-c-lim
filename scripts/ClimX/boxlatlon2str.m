function box_str=boxlatlon2str(r1W,r1E,r1S,r1N,varargin)

if r1W<0
    r1Wstr=[num2str(-r1W) 'W'];
else
    r1Wstr=[num2str(r1W) 'E'];
end
    
if r1E<0
    r1Estr=[num2str(-r1E) 'W'];
else
    r1Estr=[num2str(r1E) 'E'];
end

if r1S<0
    r1Sstr=[num2str(-r1S) 'S'];
else
    r1Sstr=[num2str(r1S) 'N'];
end

if r1N<0
    r1Nstr=[num2str(-r1N) 'S'];
else
    r1Nstr=[num2str(r1N) 'N'];
end

box_str=[r1Wstr '-' r1Estr ',' r1Sstr '-' r1Nstr];


    
