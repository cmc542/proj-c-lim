function Result=pdgmsim(x,nsim,mtaper,kopt)
% pdgmsim: Simulation of a time series from its periodogram by Percival method
% Result=pdgmsim1(x,nsim,mtaper,kopt);
% Last Revised 2005-3-17
%
% Simulation of a Gaussian stationary time series from its periodogram.  Uses nonparametric (direct spectral)
% estimate of spectral weights through Fourier tranformation of the tapered, padded time
% series.  Applies circulant embedding to generate the simulations. Objective is simulations
% spectral properties of the original time series.
%
%
%****   INPUT 
%
% x (mx x 1)r  time series
% nsim (1 x 1)n number of simulations desired
% mtaper (1 x 1)r decimal proportion of series (on each end) to be tapered. Suggest use mtaper=0.10
% kopt (1 x `1)i  options
%  kopt(1) force simulation means and standard deviations to be same as sample
%       ==1 yes
%       ==2 no
%
%*** OUTPUT
%
% Result -- structure of results
%   .Y (mx x nsim)r  time series matrix of simulated time series
%   .I (mx x 1)r  periodogram of x 
%   .var (1 x 3)r variance of original series, tapered series, padded and tapered series
%
%*** REFERENCES 
%
% Percival, Donald B. and Constantine, William L. B., nd.  Exact simulation of Gaussian time series from
% nonparametric spectral estimates with applications to bootstrapping.  Downloaded from :
% http://faculty.washington.edu/dbp/PDFFILES/esgts.pdf
%
%*** TOOLBOXES NEEDED 
%
% Statistics
%
%*** UW FUNCTIONS CALLED -- none
%
%
%*** NOTES
%
% The method is appropriate for Gaussian distributed time series!  A lilliefors test is applied to the time
% series and the user is flagged (non-fatal) if the assumption of normality is rejected by this test
% at 0.05 alpha.
%
% Approach.  Uses the Direct Spectral Estimator (section 3.1, p. 6 of Percival and Constantine (nd)) to get the
% frequency domain weights.
%
% Uses the Dietrich and Newsam (1997) formulation to convert the frequency domain weights into a simulated time
% series
%
% For simulating time series of length N, requires spectral estimates at 2N frequencies.  Since only N/2 Fourier
% frequencies, use padding with zeros to get series longer than twice it orignal length before Fourier transformation.
% Pad to next highest power of 2 greater than twice the series length.  Tapering (raised cosine bell) is applied
% before padding.
%
% The random component comes into play after estimating the spectral weights.  Standard Gaussian noise is sampled using
% function normrnd.
%


% EMAILS WITH PERCIVAL
% Hi, Dave --
% 
% On Mar 17, 2005, at 1:44 PM, David Meko wrote:
% 
%     I just finished programming a matlab function for simulation of time series using a method you describe in:
% 
%     % Percival, Donald B. and Constantine, William L. B., nd. Exact simulation of Gaussian time series from
%     % nonparametric spectral estimates with applications to bootstrapping. Downloaded from :
%     % http://faculty.washington.edu/dbp/PDFFILES/esgts.pdf
% 
%     Thanks very much for that contribution. I think the function will be very useful for me in dendroclimatology applications!
%     And the paper was clear enough for a non-statistician to follow!
% 
% 
% Glad to hear that you found the paper useful and understandable!
% 
%     I had a couple questions & comments
%     1) Is there a published reference to the online paper?
% 
% 
% Bill and I submitted the paper in December to the journal
% `Statistics and Computing'. We haven't heard back from
% them yet.
% 
%     2) In my implementation, I use the Dietrich and Newam (1997) formulation of the circular imbedding approach, and the periodogram
%     to get the required spectral weights. I pad to the next higher power of 2 larger than twice the sample size before Fourier
%     transformation. That seemed necessary to get simulated series at least as long as the sample time series. So if N=50
%     is the time series length, I pad to N=128. From that I can get simulated time series of length M=64. These are longer than my
%     sample time series, but I assume I can just truncate then and use the first N=50 values.
% 
% 
% Yes, that is exactly what I would do if I needed a series
% of length 50.
% 
%     3) Tapering and padding results in simulated series whose variance is less than that of the original x(t), t=1,..., N. I solved this by
%     restoring the simulations to all have the same variance as the original series, but perhaps I should be looking at a different method.
%     By forcing the variance in this way I take away the possibility of bootstrap testing of variability of the variance. Another
%     option would be to rescale the variance of the simulations by a factor Npad/N, but that would not address the shrinkage effect of tapering on the variance.
% 
% 
% If you use the normalization recommended in Section 3.1
% for the data tapers, the resulting direct spectral estimate
% should integrate to something close to the sample variance.
% If you want the estimate to integrate exactly to the
% sample variance, you can adjust multiply the estimate by
% a constant so that it does. This amounts to a renormalization
% of the taper such that the sum of squares of the tapered
% series is equal to the sum of the squares of the time series
% divided by N. Zero padding will just evaluate the direct
% spectral estimate on a finer grid of frequencies than that
% given by the Fourier frequencies. The simulations that you
% then get from the direct spectral estimate shouldn't have
% to be renormalized and won't have a sample variance that is
% the same as the original time series -- they might be useful
% for statistics that depend upon having the variance change
% from sample to sample in a bootstrapping scheme. Does this
% make sense, or am I misinterpreting your comment?
% 
%     4) The summation index in the eqn top of p. 5 seemed wrong, as
%     Z(2k-1) would be Z(-1) when k==0. I assumed this is a typo,
%     and used the eqn with an index of k running from 1 to M, with corresponding use of Z(1),.... Z(M) instead of Z(0),....,Z(M-1)
%     for the independent standard Gaussian random variables. That seems to work.
% 
% 
% You're right! That equation should have either
% Z_{2k+1} + i Z_{2k}
% or
% Z_{2k} + i Z_{2k+1}
% (the latter looks a bit nicer). I have uploaded a corrected version
% of the paper to the Web site and added you to the acknowledgments
% section.
% 
%     5) The method is fast! 1000 simulations of a 289 year tree ring
%     series returned an answer in about a second.
% 
% 
% The magic of FFTs!!!
% Thanks very much for your e-mail - if you spot anything
% else amiss, please pass it along. Regards,
% -- Don
% 
% ===========================================
% dbp@apl.washington.edu
% Applied Physics Laboratory
% Box 355640
% University of Washington
% Seattle, WA 98195-5640
% Phones: 206-543-1368 Fax: 206-543-6785
% 206-543-1300
% http://faculty.washington.edu/dbp
% =========================================== 
% 
% 


% 
% STEPS
%--- CHECK INPUT: TIME SERIES OF LENGTH N
%--- SUBTRACT SERIES MEAN
%--- TAPER THE SERIES
%--- PAD WITH ZEROS TO NEXT HIGHEST POWER OF 2 GREATER THAN TWICE THE SAMPLE LENGTH
%--- COMPUTE PERIODOGRAM AT FOURIER FREQUENCIES FOR FREQUENCY DOMAIN WEIGHTS S(k) at frequencies k= 0/2M,1/2M,...(2M-1)/2M
%--- COMPUTE COMPLEX VALUED SEQUENCE DEFINED ON P 5 OF REFERENCE
%--- USE THE DFT TO GET PAIRS OF INDEPENDENT REALIZATIONS OF THE TIME TIME SERIES (P. 5)E REAL VALUED SERIES V(t)



%--- CHECK INPUT

if ~(strcmp(class(x),'double') && size(x,2)==1 && size(x,1)>30);
    error('x must be of class double, and col vector of length greater than 30');
end;

if ~(isscalar(nsim) && (strcmp(class(nsim),{'double'}) || isinteger(nsim)) && nsim>=1);
    error('nsim must be scalar, real or integer, and greater than or equal to 1');
end;

if ~(kopt(1)==1 || kopt(1)==2);
    error('invalid kopt(1)');
end;

% Algorithm gives pairs of simulations;  only need to generate half of specified
nsim1 = ceil(nsim/2);

if any(isnan(x));
    error('The input series x should not have any NaNs');
end;

% Gaussian?
knongauss=lillietest(x,.05);
if knongauss ==1;
     uiwait(msgbox('Failed Lilliefors test for normality','Warning','modal'));
end;



%--- SUBTRACT SERIES MEAN

xorig=x;
xmn=mean(x);
xstd=std(x);
x=x-xmn;
mx=length(x);


%--- TAPER THE SERIES

% Taper mtap1/2 of data on each end.  See Bloomfield, p. 84.
% Compute data window
ends= fix((mtaper/2)*mx);
dwin=ones(1,mx);
t=1:ends;
btemp=0.5*(1-cos((pi*(t-0.5))/ends));  %intermediate variable
t=mx-ends+1:mx;
ctemp=0.5*(1-cos((pi*(mx-t+0.5))/ends));  % intermediate

% The data window
dwin(1:ends)=btemp;
dwin(mx-ends+1:mx)=ctemp;

x=x .* dwin';  % Tapered time series



%--- PAD WITH ZEROS TO NEXT HIGHEST POWER OF 2 HIGHER THAN TWICE SAMPLE LENGTH

padlen=2^nextpow2(2*mx);
atemp=padlen-mx;
btemp=zeros(atemp,1);
xpad=[x;btemp]; % padded, tapered time series for analysis period
xvar=[var(xorig) var(x) var(xpad)]; % variance of original, tapered, and tapered+padded


%--- COMPUTE DISCRETE FOURIER TRANSFORM OF PADDED SERIOES

% Note, what follows is same as in  Ljung Notation
% Bloomfield's squared dft is 1/N times Ljung's DFT.

z=fft(xpad,length(xpad));


%--- COMPUTE PERIODOGRAM, 

% Compute periodogram by squaring DFT of tapered, padded series.
% Computation as defined by Ljung by squaring the DFT
% Note: Bloomfield's periodogram is 1/2*pi times Ljung's periodogram.

Pyy = z.*conj(z)/padlen; % Pyy may be tje S(k) of eqn 6, p 7 --- or not; note that sum(Pyy)/(length(xpad)-1) is the variance of xpad
S=Pyy;


%--- SAMPLE GAUSSIAN NOISE AND COMPUTE MU (FIRST EQN, P 5)

M=length(xpad)/2; % M will be length of simulated time series before any truncation

Z=normrnd(0,1,2*length(xpad),nsim1);  % simulations of noise with mean 0 variance 1, per page 5

k=(1:length(xpad))';
i1 = 2*k-1;
i2=2*k;

term1=Z(i1,:)+i*Z(i2,:);
term2=   sqrt(S/length(xpad));
term2=repmat(term2,1,nsim1);

mu = term1 .* term2;


%--- COMPUTE FOURIER TRANSFORM OF MU  (EQN 5, P. 4)

V=fft(mu);


%--- EXTRACT THE nsim1 PAIRS OF SIMULATIONS

Vr =real(V);
Vi = imag(V);
D1= Vr(1:M,:);
D2=  Vi(1:M,:);


%--- SCALE VARIANCE OF THE SIMULATIONS TO COMPENSATE FOR VARIANCE REDUCTION IN PADDING 

fscale=  sqrt(length(xpad)/length(x)); % will need to scale standard dev by this factor

% Scale the simulations from the real part Vr
D1mean = repmat(mean(D1),M,1);
D1std=repmat(std(D1),M,1);
D1stda = D1std * fscale; % re-scaled standard dev
D1z = (D1 - D1mean) ./ D1std; % Z-scores
D1b =  D1z .* D1stda + D1mean;   % Rescaled


% Scale the simulations from the imag part Vi
D2mean = repmat(mean(D2),M,1);
D2std=repmat(std(D2),M,1);
D2stda = D2std * fscale; % re-scaled standard dev
D2z = (D2 - D2mean) ./ D2std; % Z-scores
D2b =  D2z .* D2stda + D2mean;   % Rescaled


%--- REDUCE DIMENSIONS OF SIMULATION MATRIX TO DESIRED 

D = [D1b D2b];
D = D(1:mx,1:nsim);

clear D1 D2 D1mean D1std D1z D1b D2mean D2std D2z D2b

%--- OPTIONAL RESCALING TO FORCE SIMULATIONS MEANS AND STANDARD DEVIATIONS TO EQUAL THOSE OF ORIGINAL SERIES


if kopt(1)==1;
    Dmean = repmat(mean(D),mx,1);
    Dstd = repmat(std(D),mx,1);
    Dz = (D - Dmean) ./ Dstd; % zscores
    
    D =  (Dz .*  repmat(xstd,mx,nsim)) + xmn;

else

end;


%--- STORE SIMILATIONS, PERIODOGRAM OF ORIGINAL SERIES, AND VARIANCE COMPONENTS

Result.Y=D;
Result.I = Pyy;
Result.var=xvar;

