function [A] = Acoef(f,Pxx) 
               a=Pxx;
               for i =1:length(f)-1
                    b=f(i+1)-f(i);
                    h1=min(a(i),a(i+1));
                    h2=max(a(i),a(i+1))-h1;
                    A(i)=b*h1+.5*b*h2;
               end
return 
