function [Pxx,f,PxxClv95] = mtmspec(x,base_f,dt)
%Usage:

[nt,ns] = size(x);
nf=length(base_f);
for i = 1:ns
    xanom=x(:,i)-nanmean(x(:,i));
    xanom(isnan(xanom))=0;

    [Pxx_twosided(:,i),w] = pmtm(xanom,2,base_f,1./dt);
end
uf=round(nt/dt);

Pxx=Pxx_twosided(1:uf,:);
f=w(1:uf);;
