function X3Danom=rm3Dmean(X3D);

dim = size(X3D);

Xave=nanmean(X3D,3);
X3Danom=X3D-repmat(Xave,[1 1 dim(3)]);
