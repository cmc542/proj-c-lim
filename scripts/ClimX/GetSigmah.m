function h =  GetSigmah(nplots,Width,Height,Overlap);
%Usage:
%h =  GetSigmah(nplots,Width,Height,Overlap);
%
%Input:
%Data.......Expects matrix whose first column is the independent x variable
%           (i.e Time, Frequency, etc...) and whose following rows are all 
%           data.
%Width......Width of individual plots
%Height.....Total Height of all the stacked plots for scaling. Note: the heigh 
%           that will appear will be (1-Overlap)*Height.
%Overlap....fraction of overlap between each plot
%
%Output:
%h..........vector of handles for each plot

axHeight = Height/nplots;
LB = .95-axHeight:-(axHeight*(1-Overlap)):-.01;

j = 1;
for i = 1:nplots
    h(i) = axes('Position',[.1 LB(i) Width-.05 axHeight]);
end
