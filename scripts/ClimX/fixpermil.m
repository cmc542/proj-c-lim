function fixpermil(filename)

% FIXPERMIL  A wrapper function to fix notation for permil in postscript Matlab figures
%  
%   fixpermil(filename)
%
% Matlab improperly codes the symbol for \permil in exported postscript.  This wrapper
% function edits the raw postscript automatically so that the the symbol will display correctly.
% This function should be called after a PRINT command which has also used the -adobecset switch
%
% Example:
% figure(1); clf; plot(randn(100,1),randn(100,1)); ylabel(['5 ',char(8240)]);
% print -depsc2 -adobecset figure.eps
% fixpermil('figure.eps')
%
% Kevin Anchukaitis, kja@ldeo.columbia.edu, first version 07.12.08, revised 12.09.10

fid = fopen(filename,'r');
str = fread(fid);
str = char(str');

%% deal with the ISOLatinEncoding, which doesn't seem to be necessary
[ids] = findstr(str,'/ISOLatin1Encoding');
if sum(size(ids)) > 0

% This would be nice, but it's not that simple apparently ...
% [sid] = size(ids,2)
%   for s = 1:sid
%    % str(ids(s):ids(s)+17) = [];
%    % str(ids(s):ids(s)+17) = '/StandardEncoding ';
%    % ids = ids-19     
%   end

% So, force the user to use -adobecset instead for now ...
disp('Exiting without correcting Postscript file ...')
disp('Use switch -adobecset with PRINT command for "correct" encodings')

return

end

%% Use proper Helvetic and Standard Encoding code for permil
strnew = []; strrep = '275';
[id] = findstr(str, '\32');

while size(id) > 0
 str1 = str(1:id(1));
 str2 = str(id(1)+3:end);
 str = sprintf('%s%s%s',str1,strrep,str2);
 [id] = findstr(str, '\32');
end

fid = fopen(filename, 'w');
fprintf(fid, '%s', sprintf('%s',str));
fclose(fid);