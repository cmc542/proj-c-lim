function mkssaplot(file1,file2);

%works in current directory or wherever filename1 & filename2 are specified
%requires both mcssac.st1.tmp (monte carlo projection)  and mcssac.st3.tmp
% (pure monte carlo)
% Monte Carlo projection variables:
mc_proj = load(file1);
mc_frqs = mc_proj(:,1);
eigs = mc_proj(:,2);
mc_clvs = mc_proj(:,3:4);
mean_clvs = mean(mc_clvs,2);

%"Pure" Monte Carlo:
mc_pure = load(file2);
mc_frqs2 = mc_pure(:,1);
eigs2 = mc_pure(:,2);
mc_clvs2 = mc_pure(:,3:4);
mean_clvs2 = mean(mc_clvs2,2);

clf
subplot(211)
hold on
errorbar(mc_frqs,mean_clvs,mc_clvs(:,1)-mean_clvs,mean_clvs-mc_clvs(:,2), ...
	'k','linewidth',1,'linestyle','none')
plot(mc_frqs,eigs,'rd','markerfacecolor','r','linewidth',1)
set(gca,'xlim',[0 .5])



subplot(212)
hold on
errorbar(mc_frqs2,mean_clvs2,mc_clvs2(:,1)-mean_clvs2,...
	mean_clvs-mc_clvs2(:,2),'k','linewidth',1,'linestyle','none')
 plot(mc_frqs2,eigs2,'rd','markerfacecolor','r','linewidth',1)
 set(gca,'xlim',[0 .5])
