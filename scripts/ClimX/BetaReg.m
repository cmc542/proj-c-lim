function [bval,pxxpred,scalef,pval]=BetaReg(f,Pxx);
%[bval,pxxpred,scalef]=BetaReg(f,Pxx);

dim=size(Pxx);
pxxpred=nan(size(Pxx));

for i =1:dim(2)
	q=~isnan(Pxx(:,i));
	y=log(Pxx(q,i));
	x=log(f(q)); 
        [b,bint,r,rint,stats]=regress(y,[x ones(size(f(q)))]);
	bval(i)=b(1);
	scalef(i)=b(2);
	pxxpred(q,i)=exp(x*b(1)+b(2));
        pval=stats(3);
end


