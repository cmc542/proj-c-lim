function disptextbl(mtx,rowLabels,columnLabels,bfmtx,isprcnt);
% usage:
% disptextbl(mtx,rowLabels,columnLabels,isprcnt);

[nrs,ncs]=size(mtx);
colsep='&';
disp(['\begin{table}[t]']);
disp(['\caption{}']);
disp(['\begin{tabular}{p{5cm} ' repmat('c',[1 ncs]) '}'])
disp('\hline')
disp('\hline')
for k =1:nrs+1
    if k==1
        arow='';
        for j=1:ncs
            arow=[arow '&' columnLabels{j}];
        end
    else
       i=k-1;
        arow=rowLabels{i};
        if isprcnt
            for j=1:ncs
                if bfmtx(i,j)
                    arow=[arow colsep '\textbf{' num2str(mtx(i,j)) '\%}'];
                else
                    arow=[arow colsep num2str(mtx(i,j)) '\%'];
                end
                    
            end          
        else 
            for j=1:ncs
                if bfmtx(i,j)
                    arow=[arow colsep '\textbf{' num2str(mtx(i,j)) '}'];
                else
                    arow=[arow colsep num2str(mtx(i,j))];
                end
            end
        end
    end
    disp([arow '\\'])
end
disp([repmat('& ',1,ncs) '\\']);
disp('\hline')
disp('\end{tabular}')
disp('\end{table}')
