function [xnorm,decstd]=decnorm(x,twin);
% xnorm=decnorm(x,twin);

[nt,ns]=size(x);
decave=bin_x2((1:nt)',x,(1:twin)');
decstd=nanstd(decave);
xnorm=x./repmat(decstd,[nt 1]);