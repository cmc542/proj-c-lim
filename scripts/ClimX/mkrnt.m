function [Xnr,ar_1] = mkrnt(x,nr);
% Usage:
% [Xnr] = mkrnt(x,nr);
%
%1)Generates nr relizations of a red-noise time series with variance of x.
%   a) estimate acf from x using Allen and Smith AR(1) model estimation
%       -code for ar1 estimation written by Eric Breigenberger (help: ar1)
%   b) computes red noise series with N= lenght(x)*10;
%   c) uses only last Returns Matr (Xnr) of red-noise series.
%   d) Returns only last "length(x)" values (to allow for stabilization)

xq = x(find(~isnan(x)));
%ar_1 = corr(xq(1:end-1),xq(2:end));
%sigma = std(xq);

[ar_1,sigma] = ar1(xq);
n = length(x);


y(1) = randn*sigma;
for j = 1:nr
    for i=2:n*10
        y(i) = y(i-1)*ar_1+randn*sigma;
    end
    Xnr(:,j) = y(end-n+1:end);
end


    