function [resh,q,nx,ny,nt]=reshape3Ddata(X3D);
%  [resh,q]=reshape3Ddata(X3D);
%  Time must be last dimension.
% q is index (binary) of grids with >90% of time obs
[nx,ny,nt]=size(X3D);
resh=reshape(X3D,nx*ny,nt)';
q=sum(~isnan(resh))>(.9*nt);
