function ts=rsclxar1(x,alpha1)
%Usage:
% ts=rsclxar1(x,alpha1);
%
%Input:
% x..........A time series to be rescaled with autocorrelation
% alpha1.....autocorrelation coefficient (between 0 and 1).
%
%Output:
% ts.........Rescaled version of x with autocorrelation.

% ts params
mn=mean(x);
sg=std(x);
x=(x-mn)/sg;
qgood=find(~isnan(x));


ts=nan(size(x));
ts(qgood(1))=x(qgood(1));
for i =qgood(2:end)';
    ts(qgood(i))=ts(qgood(i-1))*alpha1+x(qgood(i));
end

ts=((ts-nanmean(ts))./nanstd(ts))*sg+mn;