function [ax,c]=monthly_plot(monthly_data,lon,lat,cax,varargin)
load mons

for i =1:12
    
    ax(i)=subplot(4,3,i);
    
    %m_contourf(precL.lon,precL.lat,precL.seasMean(:,:,i)',clvs/10,'linestyle','none')
    m_contourf(lon,lat,monthly_data(:,:,i),varargin{:})
    
    m_coast('color','k')
    set(gca,'xtick',[],'ytick',[])
    title(mons{i})
    caxis(cax)
end
c=colorbar('horiz');    
set(c,'pos',[    0.2177    0.0714    0.6169    0.0262])

