function cmap=clvs2cmap(clvs,cmapraw)
%cmap=clvs2cmap(clvs,cmapraw)

nx1=length(cmapraw);
nx2=length(clvs)-1;

cmap=interp1(1:nx1,cmapraw,((1:nx2)./nx2)*nx1);