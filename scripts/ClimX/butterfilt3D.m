function Y=butterfilt3D(X3D,choice,N,per,missing);
%Y=butterfilt3D(X3D,choice,N,per,missing);

[nx, ny, nt]=size(X3D);
Y=nan(nx,ny,nt);
for i =1:nx,
    for j =1:ny
        x=squeeze(X3D(i,j,:));
        if ~all(isnan(x));
            y=butterfilt(x,choice,N,per,missing);
            Y(i,j,:)=y;
        end
    end
end
       

