function [Sf,tao]=ARspec(f,p,So,fN)
%% [Sf,tao]=ARspec(f,p,So,fN);
%
% f................Frequency intervals (cycles/year)
% p................Autocorrelation coefficient 
% So...............Median (or mean) spectral density
% fN...............Nyquist frequency 
%
% Theoretical AR(1) spectrum, described at:
% http://www.atmos.ucla.edu/tcd/ssa/guide/mann/mann4.html

Sf = (So)*[(1-p^2)./(1-2*p*cos(pi*f/fN)+p^2)];
tao=-1/log(p);