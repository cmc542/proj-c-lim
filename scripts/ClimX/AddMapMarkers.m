function [x,y,proj] = AddMapMarkers(MarkerData,MinMax,lat,lon)
%Usage: 
%[x,y,proj] =AddMapMarkers(MarkerData,MinMax,lat,lon);
%
% Adds scaled (by size) markers to a map (from m_map) using point-wise data. 

x = lat;
y = lon;

[EDotSize,LimRatio] = ScaleDots(MarkerData,MinMax);

%plot markers with appropriate size and color
for i=1:length(MarkerData)
    hold on
    if ~isnan(LimRatio(i))
        if MarkerData(i) > 0;
        %    m_plot(x(i), y(i),'marker','o','color', 'k','MarkerFaceColor','k','MarkerSize',EDotSize(i));
             proj(i,:) = plotm(x(i), y(i),'marker','o','color', 'k','MarkerSize',EDotSize(i),'LineWidth', 1);

        else
%            m_plot(x(i), y(i),'marker','V','color', 'k','MarkerFaceColor',[.95 .95 .95],'MarkerSize', EDotSize(i),'LineWidth',1.5);
             proj(i,:) = plotm(x(i), y(i),'marker','V','color', 'k','MarkerSize', EDotSize(i),'LineWidth', 1);
        end
         

    end
end
hold on