function varout=ncgetvar(filename,var,dofill)
%varout=ncgetvar(filename,var)
% "var" can be numeric index of variable or name
    
 ncid=netcdf.open(filename,0);
if(isnumeric(var))
    varnum=var;    
else
    varnum=netcdf.inqVarID(ncid,var);
end
varout=double(netcdf.getVar(ncid,varnum));

if nargin == 3
    dofill;
    varout(varout==netcdf.getAtt(ncid,varnum,'_FillValue'))=nan;
end

netcdf.close(ncid)
