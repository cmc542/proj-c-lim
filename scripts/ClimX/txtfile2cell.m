function X=txtfile2cell(fname,varargin)

% 
if nargin==2
    nlines=varargin{1};
else
    nlines=inf;

end

fid=fopen(fname);
k=1;
while 1
    tline = fgetl(fid);
    if ~ischar(tline), 
        break, 
    end
    X{k,1}=tline;
    k=k+1;
    
    if k>nlines
        break
    end
end
fclose(fid);