function [Xanom, Seas] = rm_ann_cyc(X,nseas);
%[Xanom, Seas] = rm_ann_cyc(X,nseas);

nt = size(X,1);
Xanom = nan(size(X));
for i = 1:nseas
    Mu = nanmean(X(i:nseas:end,:));
    Seas(i,:) = Mu;
    Xanom(i:nseas:end,:) = X(i:nseas:end,:)-repmat(Mu,size(X(i:nseas:end,:),1),1);
end
    