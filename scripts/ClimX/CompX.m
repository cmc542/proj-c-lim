function C = CompX(X,Ind);
%Usage:
%C = CompositeX(X,Ind);
%
%C....1xN
%X....MxN
%Ind..px1

C = nanmean(X(Ind,:));