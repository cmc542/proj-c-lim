function [bin_mean3D, bin_sum3D,BinTime] =  bin_x2(Xtime,X3D,bins);
%[bin_mean, bin_sum, BinTime] =  bin_x2(Xtime,X,bins);

dim=size(X3D);
X=reshape(X3D,dim(1)*dim(2),dim(3))';
q=find(sum(~isnan(X))>0);
binstep = bins(2)-bins(1);

for i = 1:length(bins)-1
    q = find(Xtime >= bins(i) & Xtime<bins(i+1));
    
    bin_mean(i,:) = [nanmean(X(q,:),1)];
    bin_sum(i,:)  = [nansum(X(q,:),1)];
    BinTime(i,1)= nanmean(Xtime(q));
end

i=i+1;
bin_mean(i,:) = nan(1,size(X,2));

bin_mean3D=reshape(bin_mean',dim(1),dim(2),[]);
bin_sum3D=reshape(bin_sum',dim(1),dim(2),[]);
