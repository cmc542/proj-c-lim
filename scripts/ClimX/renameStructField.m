function s=renameStructField(s,oldName,newName);
%renameStructField(s,oldName,newName);

[s.(newName)] = s.(oldName);
s = rmfield(s,oldName);
