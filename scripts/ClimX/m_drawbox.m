function h=m_drawbox(lon,lat,linewidth,varargin);
%h=m_drawbox(lon,lat,linewidth);


edges=[lon(1) lat(1);
       lon(2) lat(1);
       lon(2) lat(2);
       lon(1) lat(2);
       lon(1) lat(1)];
    
hold on
m_plot(edges(:,1),edges(:,2),'k','linewidth',linewidth,varargin{:});