% 
% Create NetCDF 
% 

function wrnc2d(ncfile,nwtime,wrvar,tunit,time_ini) 

%tunit='years'

%whos nwtime wrvar tunit 


lat(1)=0; 
lon(1)=0; 
xwrvar(:,1,1)=wrvar(:); 
wrvar=xwrvar; 
%xtmp=wrvar'; 
%clear wrvar; 
%wrvar=xtmp; 

%whos wrvar 
%nwtime 


%-ncid = netcdf.open('wrfipccdailyrain_1968.nc','NC_NOWRITE');
%-data_fill = netcdf.getAtt(ncid,3,'_FillValue');
%data_fill 
%data_fill = -999.9; 

data_fill = single(-999.9); 

whos wrvar data_fill

wrvar (find( isnan(wrvar) == 1)) = data_fill; 

%----------------------------------% 
nca=netcdf.create(ncfile,'NC_SHARE');

% Define dimensions
%nlat=max(size(lat)); 
%nlon=max(size(lon)); 
nlat=1;
nlon=1;

ntime=max(size(nwtime)); 
%
lon_dim=netcdf.defDim(nca,'lon',nlon);
lat_dim=netcdf.defDim(nca,'lat',nlat);
tim_dim=netcdf.defDim(nca,'time',ntime);
%%
% Define variables and attributes
lon_id=netcdf.defVar(nca,'lon','double',lon_dim);
netcdf.putAtt(nca,lon_id,'long_name','Longitude');
netcdf.putAtt(nca,lon_id,'units','degrees_east');
netcdf.putAtt(nca,lon_id,'standard_name','lon');
%
 lat_id=netcdf.defVar(nca,'lat','double',lat_dim);
 netcdf.putAtt(nca,lat_id,'long_name','Latitude');
 netcdf.putAtt(nca,lat_id,'units','degrees_north');
 netcdf.putAtt(nca,lat_id,'standard_name','lat');
%
time_id=netcdf.defVar(nca,'time','double',tim_dim);

%tunit='years'
if ( tunit=='years  ' ) 
%  time_ini = 'years since 1971-01-01 00:00:00';
   netcdf.putAtt(nca,time_id,'units',time_ini);
%  netcdf.putAtt(nca,time_id,'units','years  since -001-01-01 00:00:00');
%  netcdf.putAtt(nca,time_id,'units','years  since 0000');
end 
if ( tunit=='days   ' ) 
%  dysini = 'days since 1968-01-01 00:00:00';
%  dysini = 'days since 1971-01-01 00:00:00';
   time_ini = 'days since 1971-01-01 00:00:00';
%  netcdf.putAtt(nca,time_id,'units','days since 1968-01-01 00:00:00');
   netcdf.putAtt(nca,time_id,'units',time_ini);
end 
if ( tunit=='hours  ' ) 
   netcdf.putAtt(nca,time_id,'units','hours since 1968-01-01 00:00:00');
end 
if ( tunit=='seconds' ) 
   netcdf.putAtt(nca,time_id,'units','seconds since 1968-01-01 00:00:00');
end 



%netcdf.putAtt(nca,time_id,'units','years since 1968-01-01 ');
netcdf.putAtt(nca,time_id,'calendar','standard');
%
%data_id = netcdf.defVar(nca,'data','float',[tim_dim lon_dim lat_dim]);
%data_id = netcdf.defVar(nca,'data','float',[tim_dim lat_dim lon_dim]);

%data_id = netcdf.defVar(nca,'data','float',[lat_dim lon_dim]);

%data_id = netcdf.defVar(nca,'data','float',[lon_dim lat_dim]);

%data_id = netcdf.defVar(nca,'data','float',[tim_dim]);

data_id = netcdf.defVar(nca,'data','float',[tim_dim lon_dim lat_dim]);

%data_id = netcdf.defVar(nca,'topo','float',[lat_dim lon_dim]);
%data_id = netcdf.defVar(nca,'topo','float',[lon_dim lat_dim]);

netcdf.putAtt(nca,data_id,'units','m');
netcdf.putAtt(nca,data_id,'_FillValue',data_fill);
netcdf.endDef(nca);

% WRITE data
netcdf.putVar(nca,lon_id,lon);
netcdf.putVar(nca,lat_id,lat);
%netcdf.putVar(nca,data_id,uu);

netcdf.putVar(nca,time_id,nwtime);
netcdf.putVar(nca,data_id,wrvar);

netcdf.reDef(nca);
netcdf.close(nca);
%
