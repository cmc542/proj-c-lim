% 
clear all
close all

addpath ~/matlablib/m_map/ 
%-addpath ../m_map/ 
addpath ../subr/ 

%-PREC file 
  precfile = '/data/cmc542/prec/cmap/precip_Amon_CMAP_historical_v2.5x2.5_197901-201812.tropics.nc';
%-SST  file 
  filegph = '/data/cmc542/sst/ERSSTv5/sst_Amon_ERSST_historical-v5_2.0x2.0_197901-201812.tropics.nc'
%--
%--PART I: EOF analysis 
%--
  mm='dec-feb';
%--
  ncid = netcdf.open(filegph,'NC_NOWRITE');
  rdata = netcdf.getVar(ncid, netcdf.inqVarID(ncid,'sst') );
  lat = netcdf.getVar(ncid, netcdf.inqVarID(ncid,'lat') );
  lon = netcdf.getVar(ncid, netcdf.inqVarID(ncid,'lon') );

% data_fill = netcdf.getAtt(ncid,3,'_FillValue'); 
% data_fill = netcdf.getAtt(ncid,3,'missing_value'); 
  data_fill = netcdf.getAtt(ncid,4,'missing_value'); 
  netcdf.close(ncid);

%-
% gph (find(isnan(rdata) == 1)) = data_fill; 
  rdata ( find(rdata == data_fill )) = NaN;
%--
  Alat=[-5 5]; Alon=[360-150  360-130];
  sst_N34 = X3D_to_area(rdata,lon,lat,Alon,Alat);

% gph = rdata; 

  gph = (rdata(:,:,1:12:end)+rdata(:,:,2:12:end)+rdata(:,:,3:12:end))./3; 

  gphmean = mean(gph,3);
 [nrow ncol ntime] = size(gph); 
  for it=1:ntime  
     gph(:,:,it)  = gph(:,:,it) - gphmean; 
  end 
%-figure(1); contourf(lon,lat,gph(:,:,1)'); 
% 
  gph_raw = gph; 
  for icol=1:ncol
     gph(:,icol,:)  = gph(:,icol,:) * sqrt(cos(lat(icol)*pi/180.)); 
  end 
%-figure(2); contourf(lon,lat,gph(:,:,1)'); 
%-           title('ANOMALY FIELD') 
% 
% 
   styear = 1979;endyear = 2018;
   path_store ='./';
   
   expl_eofr  = strcat(path_store,'expl_roteof.gph.1979-2018.',mm,'.txt');
   expl_eof     = strcat(path_store,'expl_eof.gph.1979-2018.',mm,'.txt'); 
   
% 
%----------------------------------------------------% 
SPI2D = gph; 
%----------------------------------------------------% 
PPmatI = SPI2D; 
[ny,nx,nt] = size(PPmatI);
for i=1:nx
    for j=1:ny 
        nnan = length( find ( isnan(PPmatI(j,i,:)) == 1) ); 
        if (nnan == 0) 
           xpp(j,i,:)=PPmatI(j,i,:); 
 	else 
           xpp(j,i,1:nt)=NaN; 
        end
    end 
end 

nmiss = length( find ( isnan(xpp(:,:,1)) == 1) ); 
nreal = nx*ny-nmiss;
%% 
%% Rarray
%% construct the reduced array
%% 
for it=1:nt
    xtmp = xpp(:,:,it);
%    it 
    idxpp = find(~isnan(xtmp)); % to reconstruct 2D map 
%   realpt = find(~isnan(iPPmat));
%   Rarray(iy,:) = iPPmat(realpt)';
    Rarray(it,:) = xtmp(find(~isnan(xtmp)))';
end
%----------------------------------------------------% 
%%%%%%%%%%%%
% SVD
%%%%%%%%%%%%
disp('computing SVD ...') 
[U,S,EOFs] = svd(Rarray);
% 
%----EOF rotation-------
% 
nmod = 5;  % number of modes retained 
%-nmod = 10; 

%EOFr = rotatefactors(EOFs(:,1:nmod)); % Varimax Rotation: columns 1-5
%PCr = Rarray*EOFr;

EOFr = EOFs(:,1:nmod); % Varimax Rotation: columns 1-5
PCr = Rarray*EOFr;
PCr2 = U(:,1:nmod);
% Normalize 
  for imod = 1:nmod
      stdPCr(imod) = std(PCr(:,imod));
      PCr(:,imod) = PCr(:,imod) ./ std(PCr(:,imod));
      EOFr(:,imod) = EOFr(:,imod) .* stdPCr(imod);
  end

%-----------------% 
% EOFr3D (map) ---% 
%-----------------% 
year = styear:endyear;
ieof = nan(ny,nx); % initial value 
for imod = 1:nmod
    ieof(idxpp) = EOFr(:,imod);
    EOFr3D(:,:,imod) = ieof;
end 

 wrnc2d('eof1.gph.nc',lat,lon,EOFr3D(:,:,1));
 wrnc2d('eof2.gph.nc',lat,lon,EOFr3D(:,:,2));
 wrnc2d('eof3.gph.nc',lat,lon,EOFr3D(:,:,3));

figure;%-contourf(flipud( EOFr3D(:,:,1)' ))
    lon = double(lon); lat=double(lat); 
    lonlim = [min(lon) max(lon)];
    latlim = [min(lat) max(lat)];

    m_proj('Gall-Peters','longitudes',lonlim,'latitudes',latlim);
    m_pcolor(lon,lat, EOFr3D(:,:,1)'); hold on;
    m_coast('color',[0.0 0.6 0.0],'LineWidth', 2.5);
    shading flat 
    c=colorbar;
       title('EOF1 (SST[DJF] )') 

if (1==0) 
    figure;contourf(flipud( EOFr3D(:,:,2)' ))
       title('EOF2 FIELD') 
    figure;contourf(flipud( EOFr3D(:,:,3)' ))
       title('EOF3 FIELD') 
end 

if (1==0)
   figure; 
     plot(PCr2(:,1))   
     title('PC1 TIME SERIES') 
   figure; 
     plot(PCr2(:,2))   
     title('PC2 TIME SERIES') 
   figure; 
     plot(PCr2(:,3))   
     title('PC3 TIME SERIES') 
end 

%------------------% 
  tunit = 'years  ';
  ntime = length(PCr2);
  t = [0:ntime-1];
  time_ini = 'years since 1981-01-01 00:00:00';
  xvar=PCr2(:,1);wrnc1d('pc1.gph.nc',t,xvar,tunit,time_ini);
  xvar=PCr2(:,2);wrnc1d('pc2.gph.nc',t,xvar,tunit,time_ini);
  xvar=PCr2(:,3);wrnc1d('pc3.gph.nc',t,xvar,tunit,time_ini);


%-figure(1);contourf(EOFr3D2(:,:,1)')
%-figure(2);contourf(EOFr3Draw(:,:,1)')

%-figure(1);contourf(EOFr3D2(:,:,2)')
%-figure(2);contourf(EOFr3Draw(:,:,2)')

%-figure(1);contourf(EOFr3D2(:,:,3)')
%-figure(2);contourf(EOFr3Draw(:,:,3)')

%----------------------
%
%--------------------
%  EOF variance 
%--------------------
% meth 1: 
[M xtmp] =  size(S); 
for im=1:M
  lamd(im) = S(im,im); 
end 
 expvar = (lamd.^2) ./ sum(lamd.^2); 


if (1==0) 

   znor = icdf('normal',0.975,0,1.0)
%  N = 300;  
%  N = 1000;  
   N = 30;  
   xlamd = 14.02; 
   xlamd = 12.61; 
   xlamd = 10.67; 
   xlamd = 10.43; 
%  dlamd = xlamd*znor*sqrt(2/N)
   dlamd = expvar*znor*sqrt(2/N)
   x=[1:10]; 
%  figure;plot(x,expvar(1:10),'o','MarkerSize',10,x,expvar(1:10)+dlamd(1:10),'+r',x,expvar(1:10)-dlamd(1:10),'+r') 
   figure;
   plot(x,expvar(1:10),'o','MarkerSize',10,'MarkerFaceColor','b');
   hold on; 
   for ix=1:10
     xline = [x(ix)  x(ix)];  yline  = [expvar(ix)-dlamd(ix) , expvar(ix)+dlamd(ix)];
     line(xline,yline,'LineStyle','-','LineWidth',2,'Color','r')
   end 
   ix=1; 
   xline = [x(ix)  x(ix)];  yline  = [expvar(ix)-dlamd(ix) , expvar(ix)+dlamd(ix)];
   line(xline,yline,'LineStyle','-','LineWidth',3,'Color','r')

   plot(x,expvar(1:10),'o','MarkerSize',10,'MarkerFaceColor','b');

   title('Eigenvalue Spectrum: Significant EOFs at 95% Confidence Interval','FontSize',14,'FontWeight','bold');
   xlabel('(Eigenvalue Number)','FontSize',13);
   ylabel('(Variance Explained)','FontSize',13);

end 

% 
% PART II: correlation 
% 
%
% PREC 
% 
% precfile = '/data/cmc542/prec/cmap/precip_Amon_CMAP_historical_v2.5x2.5_197901-201812.tropics.nc';

  ncid = netcdf.open(precfile,'NC_NOWRITE');
  prec = netcdf.getVar(ncid, netcdf.inqVarID(ncid,'precip') );
  lat = netcdf.getVar(ncid, netcdf.inqVarID(ncid,'lat') );
  lon = netcdf.getVar(ncid, netcdf.inqVarID(ncid,'lon') );
% fillvalue  = netcdf.getAtt(ncid,3,'_FillValue');
  fillvalue  = netcdf.getAtt(ncid,3,'missing_value');

  netcdf.close(ncid);
  prec(find(prec==fillvalue)) = NaN;

  Alat=[-10 0]; Alon=[360-180  360-160];
  prec_N34 = X3D_to_area(prec,lon,lat,Alon,Alat);

  for imo=1:12
      clear xprec xsst 
      xprec = prec_N34(imo:12:end);
      xsst = sst_N34(imo:12:end);
      xcorr(imo) = corr(xprec(:),xsst(:));  
  end 
%---- 
%----
      figure;bar(xcorr,'c');hold on;
%     axis([0 13 0.0 1.5])
      set(gca,'XTickLabel', {'J','F','M','A','M','J','J','A','S','O','N','D'})
      shading flat
%     tl=title(ax(1),'STD(SST [N34] ) a) mv-blocks'); 
      tl=title('(a) OMEGA = CORR(SST,PREC)');
%---- 
%----

  prec2 = prec; 
  clear prec ; 
% mm 2 DJF
  prec = (prec2(:,:,1:12:end)+prec2(:,:,2:12:end)+prec2(:,:,3:12:end))./3; 

%
% correlation  and t-test 
% 
  pcsea = PCr2(:,:); 

  pctime = [1979:2018]; 
  save('pcs.1-5.gph.mat','pcsea','pcsea'); 

  for imod=1:3; 
   [nrow ncol ntime]=size(prec);
    r2d = NaN(nrow,ncol);
    rtest = NaN(nrow,ncol);
    for irow=1:nrow
        for icol=1:ncol
            x = squeeze(prec(irow,icol,:))';
            y = pcsea(:,imod)';

            if (length(x) == length( find(isnan(x)==1)))
                r2d(irow,icol)=NaN;
                rtest(irow,icol) = NaN;
            else
                xcov = cov([x' y']);covxy = xcov(2,1);
                varx = xcov(1,1);vary = xcov(2,2);

                a1 = covxy/varx;  % regression  coefficient 
                r = covxy/( sqrt(varx) * sqrt(vary) ) ;  % regression  coefficient 
                r2d(irow,icol)=r;
%              [rdw  rup]=corrtest(23,r,0.90,'twotails'); 
               [tcrit t]=corrttest(ntime,r,0.90,'twotails');
                   rtest(irow,icol) = t;
%                if(r<rup & r>rdw) 
%                  rtest(irow,icol) = 1.0; 
%                else 
%                   rtest(irow,icol) = -1 .0; 
%                end
            end
        end
    end
    r2dprec(:,:,imod)=r2d;
    rtestprec(:,:,imod)=rtest;
  end 

  wrnc2d('r.prec.pc1.nc',lat,lon,r2dprec(:,:,1));
  wrnc2d('r.prec.pc2.nc',lat,lon,r2dprec(:,:,2));
  wrnc2d('r.prec.pc3.nc',lat,lon,r2dprec(:,:,3));

  wrnc2d('ttest.prec.pc1.nc',lat,lon,rtestprec(:,:,1));
  wrnc2d('ttest.prec.pc2.nc',lat,lon,rtestprec(:,:,2));
  wrnc2d('ttest.prec.pc3.nc',lat,lon,rtestprec(:,:,3));

  figure; % contourf(lon,lat,r2dprec(:,:,1)')
    lon = double(lon); lat=double(lat); 
    lonlim = [min(lon) max(lon)];
    latlim = [min(lat) max(lat)];

    m_proj('Gall-Peters','longitudes',lonlim,'latitudes',latlim);
    m_pcolor(lon,lat, r2dprec(:,:,1)'); hold on;
%   m_coast('color',[0.0 0.6 0.0],'LineWidth', 2.5);
    m_coast('color','r','LineWidth', 2.5);
    shading flat 
    c=colorbar;
       title('CORR(PC1, PREC)') 
%
return 

