% cmc May 23, 2017, a code line added below to remove missing values for X2D1 as in X3D2 
clear
close all
%--
addpath ~/ClimX 
%% Modify this area to specify Y, tau, neigs
% general:

%-nyrs = 400; % nyrears in file 
%-nyrs = 1000; % nyrears in file 
nyrs = 1000; % nyrears in file 
nyrs_sel = 100; % nyrears in selected 

nyrs = 40; % nyrears in file 
nyrs_sel = 40; % nyrears in selected 

flag_ran = 0;%--for new ran:  1=yes or 0=not 
flag_ran = 1;%--for new ran:  1=yes or 0=not 
%--run  = '001';
%-idxran = randperm(nyrs - nyrs_sel,100);
idxran = randperm(nyrs,40);

ranlen = length(idxran); 
%-ranlen = 450 * ones(size(idxran));% this to a fix years 1300=850+450 
ranlen = 480 * ones(size(idxran));% this to a fix years 1300=850+450 
if ( flag_ran == 1) 
     save('ranparms.mat','nyrs','nyrs_sel','idxran'); 
else 
     load ranparms 
end 
%----
%----
%for iiran=1:ranlen 
%for iiran=1:100
 for iiran=1:2
%    iiran=1; 
     sran = ind2str(iiran,3); 
%-   t1 = (idxran(iiran) - 1)*12+1; 
     t1 = 1;
     tn = t1 + nyrs_sel*12-1; 

%-limParamFile='sst_historical_v20180603_085001-124912.Xeofs.global.22pdsiEigs.Norm10000.limParams.mat';
limParamFile=strcat('sst_historical_v20180603_085001-124912.Xeofs.global.22pdsiEigs.Norm10000.limParams.r',sran,'.mat');

%-eofDir='../results/';
%-paramDir='../params/';
eofDir='./results/';
paramDir='./params/';

pauseOn=false;
doEofs=true;
%-
eigen = 'off';
%eigen = 'on ';
to_screen = strcat('EIGEN: ',eigen);
disp (to_screen); 
%-
% lag (tau) range
tau0=3;
tauRange=[3:15];

% NOTE: naming conventions must be "CMIP5-conforming" for the files
% to be correctly identified after the stochastic forcing part is
% done:
%e.g.,: variable_Realm_MODEL_scenario_ensemble_YYYYMM-YYYYMM.nc
%% ------------------- define X1 ---------------------------------------
%inDir1='../data/';
%inFile1='sst_ERSSTv4_bc_0.9x1.25_195801_200012_080315.Tropics.nc';
%inFile1='sst_Omon_Kaplan_historical_v2_195801-200012.TrPac.nc';
%inFile1='sstdt_Omon_ERSST_historical_v3b_195801-200012.Tropics.nc';
%inFile1='ERSSTv3b_1x1.195801-200012.Tropics.nc';
%inFile1='sst_Omon_ERSST_historical_v3bfilled_195801-200012.Tropics.nc';
%inFile1='sst_Omon_ERSST_historical_v3b_195001-200012.Tropics.nc';
%inDir1='../data/';
%inFile1='sst_Omon_ERSST_historical_v3b_195801-200012.Tropics.nc';

%-inDir1='/data/cmc542/sst/ERSST/';
%--inFile1='sst_Omon_ERSST_historical_v3b_196001-200712.extropic.nc';
%-inFile1='sst_Omon_ERSST_historical_v3b_196001-200712.extropic.2varfit.nc';%--this sst will not be detrended 
%--inFile1='sst_Omon_ERSST_historical_v3b_196001-200712.extropic.nc';
%-inDir1='/data2/cmc542/scratch/MEME/post/f.e12.FAMIPC5CN.f19_19.mvBtoga.001x/to_PDSI/';
%-inFile1='TS_Amon_f-e12-FAMIPC5CN-mvBtoga_f19-19_001x_085001-124912.tropics.nc';%--this sst is not detrended 
%inDir1='/data2/cmc542/scratch/MEME/LME_to_LIM/CESM-CAM5-LME/to_PDSI/';
%inFile1='b.e11.B1850C5CN.f19_g16.0850cntl.001.cam.h0.TS.085001-184912.tropics.nc';%--this sst is not detrended 

%-inDir1='/data2/cmc542/scratch/MEME/LME_to_LIM/CESM-CAM5-LME/to_PDSI/';
%-inFile1='TS_Amon_b-e11-B1850C5CN-0850cntl_f19-g16_001_085001-184912.tropics.nc';%--this sst is not detrended 

inDir1='/data/cmc542/sst/ERSSTv5/';
inFile1='sst_Amon_ERSST_historical-v5_2.0x2.0_197901-201812.tropics.nc';%--this sst is not detrended 

if (eigen == 'on ')
    inFile1='sst_Omon_ERSST_historical_v3b_196001-200712.extropic.nc';
end

%varname1='SST_cpl'; lonName='lon'; latName='lat'; timeName='time';
%varname1='ssta'; lonName='X'; latName='Y'; timeName='T';
%varname1='anom'; lonName='X'; latName='Y'; timeName='T';
 varname1='sst'; lonName='lon'; latName='lat'; timeName='time';
%varname1='TS'; lonName='lon'; latName='lat'; timeName='time';
%varname1='mvrn'; lonName='lon'; latName='lat'; timeName='time';

neig1=17;
smoowin1=3;

%import data
dataStruct1=nc2struct([inDir1 inFile1]);
%--
if (eigen == 'on ')
 if isfield(dataStruct1.(varname1).attributes,'scalefactor')
     dataStruct1.(varname1).data= dataStruct1.(varname1).data* ...
     dataStruct1.(varname1).attributes.scalefactor;
 end
%--cmc May 23, 2007, line below added to remove missing values  as in X3D2 
 missingVal1=dataStruct1.(varname1).attributes.missingvalue; dataStruct1.(varname1).data(dataStruct1.(varname1).data==missingVal1)=nan;
 FillvalueVal1=dataStruct1.(varname1).attributes.FillValue; dataStruct1.(varname1).data(dataStruct1.(varname1).data==FillvalueVal1)=nan;
end 
%--
X3D1=dataStruct1.(varname1).data; lon1=dataStruct1.(lonName).data; lat1=dataStruct1.(latName).data; time1=dataStruct1.(timeName).data;
%--
%--
X3D1 = X3D1(:,:,t1:tn);
time1 = time1(t1:tn);
X3D1=rm_ann_cyc3D(X3D1,12);
nlon1=length(lon1); nlat1=length(lat1); ntraw1=length(time1);

% Smooth:
X1resh=reshape(X3D1,nlon1*nlat1,ntraw1)';

X1resh_smoo=smoothx(X1resh,smoowin1,'mean');
time1_smoo=smoothx(time1,smoowin1,'mean');

tq1_smoo=find(~isnan(time1_smoo));
nt1smoo=length(tq1_smoo);
X1resh_smoo=X1resh_smoo(tq1_smoo,:); %get rid of empty rows
q1=find(~all(isnan(X1resh_smoo)));
X1resh_smoo_q=X1resh_smoo(:,q1);
X1resh_smoo_q_anom=rm_mtx_mean(X1resh_smoo_q);

% do eof Filtering
eofFile1=strrep(inFile1,'.nc','.eofs.nc');
if doEofs
    XfiltStruct1=doEOF_filter_wrap(X1resh,X1resh_smoo_q_anom,X1resh_smoo,X3D1,q1,tq1_smoo,time1_smoo,lon1,lat1,neig1,nlon1,nlat1,nt1smoo,smoowin1);
    XfiltStruct1.global_attributes.history=['Created from ' inFile1];
    ncstruct2ncfile(XfiltStruct1,[eofDir eofFile1]);    
end

%% ------------------- define X2 ---------------------------------------
%inDir2='/data/cmc542/to_LIM/';
%inFile2='ssh_Omon_CARTON-GIESE-SODA_historical_v2p0p2p4_196001-200712.extropics.2varfit.nc';
%if(eigen == 'on ')
%  inFile2='ssh_Omon_CARTON-GIESE-SODA_historical_v2p0p2p4_196001-200712.extropics.nc';
%end

%varname2='ssh'; lonName='lon'; latName='lat'; timeName='time';
%neig2=7;
%smoowin2=3;

%import data
%dataStruct2=nc2struct([inDir2 inFile2]);
%--missingVal2=dataStruct2.(varname2).attributes.missingvalue; dataStruct2.(varname2).data(dataStruct2.(varname2).data==missingVal2)=nan;
%X3D2=dataStruct2.(varname2).data; lon2=dataStruct2.(lonName).data; lat2=dataStruct2.(latName).data; time2=dataStruct2.(timeName).data;
%nlon2=length(lon2); nlat2=length(lat2); ntraw2=length(time2);

% Smooth:
%X2resh=reshape(X3D2,nlon2*nlat2,ntraw2)';
%X2resh_smoo=smoothx(X2resh,smoowin2,'mean');
%time2_smoo=smoothx(time2,smoowin2,'mean');
%tq2_smoo=find(~isnan(time2_smoo));
%nt2smoo=length(tq2_smoo);
%X2resh_smoo=X2resh_smoo(tq2_smoo,:); %get rid of empty rows
%q2=find(~all(isnan(X2resh_smoo)));
%X2resh_smoo_q=X2resh_smoo(:,q2);
%X2resh_smoo_q_anom=rm_mtx_mean(X2resh_smoo_q);


% do eof Filtering
%eofFile2=strrep(inFile2,'.nc','.eofs.nc');
%if doEofs
%    XfiltStruct2=doEOF_filter_wrap(X2resh,X2resh_smoo_q_anom,X2resh_smoo,X3D2,q2,tq2_smoo,time2_smoo,lon2,lat2,neig2,nlon2,nlat2,nt2smoo,smoowin2);
%    XfiltStruct2.global_attributes.history=['Created from ' inFile2];
%    ncstruct2ncfile(XfiltStruct2,[eofDir eofFile2]);    
%end
%% ------------------- define X3 ---------------------------------------
%inDir3='/data/cmc542/to_LIM/';
%inFile3='taux_Omon_CARTON-GIESE-SODA_historical_v2p0p2p4_196001-200712.TrPac.nc';

%varname3='taux'; lonName='lon'; latName='lat'; timeName='time';
%neig3=3;
%smoowin3=3;

%import data
%dataStruct3=nc2struct([inDir3 inFile3]);
%missingVal3=dataStruct3.(varname3).attributes.missingvalue; dataStruct3.(varname3).data(dataStruct3.(varname3).data==missingVal3)=nan;
%X3D3=dataStruct3.(varname3).data; lon3=dataStruct3.(lonName).data; lat3=dataStruct3.(latName).data; time3=dataStruct3.(timeName).data;
%nlon3=length(lon3); nlat3=length(lat3); ntraw3=length(time3);

% Smooth:
%X3resh=reshape(X3D3,nlon3*nlat3,ntraw3)';
%X3resh_smoo=smoothx(X3resh,smoowin3,'mean');
%time3_smoo=smoothx(time3,smoowin3,'mean');
%tq3_smoo=find(~isnan(time3_smoo));
%nt3smoo=length(tq3_smoo);
%X3resh_smoo=X3resh_smoo(tq3_smoo,:); %get rid of empty rows
%q3=find(~all(isnan(X3resh_smoo)));
%X3resh_smoo_q=X3resh_smoo(:,q3);
%X3resh_smoo_q_anom=rm_mtx_mean(X3resh_smoo_q);

% do eof Filtering
%eofFile3=strrep(inFile3,'.nc','.eofs.nc');
%if doEofs
%    XfiltStruct3=doEOF_filter_wrap(X3resh,X3resh_smoo_q_anom,X3resh_smoo,X3D3,q3,tq3_smoo,time3_smoo,lon3,lat3,neig3,nlon3,nlat3,nt3smoo,smoowin3);
%    XfiltStruct3.global_attributes.history=['Created from ' inFile3];
%    ncstruct2ncfile(XfiltStruct3,[eofDir eofFile3]);    
%end

%% ------------------- define X4 ---------------------------------------
%-inDir4='../data/';
%-inFile4='scpdsi_Amon_Scheffield_historical_vCRU3p10pm_195801-200012.WNA.nc';
%--inDir4='/data/cmc542/to_LIM/';
%--inFile4='scpdsi_Amon_Scheffield_historical_vCRU3p10pm_196001-200712.WNA.nc';
%inDir4='/data2/cmc542/scratch/MEME/post/f.e12.FAMIPC5CN.f19_19.mvBtoga.001x/to_PDSI/';
%inFile4='pdsi-PM_Amon_f-e12-FAMIPC5CN-mvBtoga_f19-19_001x_085001-124912.swus.nc';
%inDir4='/data2/cmc542/scratch/MEME/post/b.e11.B1850C5CN.f19_g16.0850cntl.001/to_PDSI/';
%inDir4='/data2/cmc542/scratch/MEME/LME_to_LIM/CESM-CAM5-LME/to_PDSI/';
%inFile4='pdsi-PM_Amon_b-e11-B1850C5CN-0850cntl_f19-g16_001_085001-184912.swus.nc';
%inFile4='PRECT_Amon_b-e11-B1850C5CN-0850cntl_f19-g16_001_085001-184912.tropics.nc';

 inDir4='/data/cmc542/prec/cmap/';
 inFile4='precip_Amon_CMAP_historical_v2.5x2.5_197901-201812.tropics.nc';

%varname4='pdsi_pm'; lonName='longitude'; latName='latitude'; timeName='time';
%varname4='PRECT'; lonName='lon'; latName='lat'; timeName='time';
varname4='precip'; lonName='lon'; latName='lat'; timeName='time';
neig4=22;
smoowin4=3;

%import data
dataStruct4=nc2struct([inDir4 inFile4]);
%-missingVal4=-999000000; dataStruct4.(varname4).data(dataStruct4.(varname4).data==missingVal4)=nan;
%missingVal4=dataStruct4.(varname4).attributes.missingvalue; dataStruct4.(varname4).data(dataStruct4.(varname4).data==missingVal4)=nan;
%FillvalueVal4=dataStruct4.(varname4).attributes.FillValue; dataStruct4.(varname4).data(dataStruct4.(varname4).data==FillvalueVal4)=nan;

% missing value not defined, so this line removed 
%-FillvalueVal4=dataStruct4.(varname4).attributes.missingvalue; dataStruct4.(varname4).data(dataStruct4.(varname4).data==FillvalueVal4)=nan;

X3D4=dataStruct4.(varname4).data; lon4=dataStruct4.(lonName).data; lat4=dataStruct4.(latName).data; time4=dataStruct4.(timeName).data;
%--
X3D4 = X3D4(:,:,t1:tn);
time4 = time4(t1:tn);
nlon4=length(lon4); nlat4=length(lat4); ntraw4=length(time4);

% Smooth:
X4resh=reshape(X3D4,nlon4*nlat4,ntraw4)';
X4resh_smoo=smoothx(X4resh,smoowin4,'mean');
time4_smoo=smoothx(time4,smoowin4,'mean');
tq4_smoo=find(~isnan(time4_smoo));
nt4smoo=length(tq4_smoo);
X4resh_smoo=X4resh_smoo(tq4_smoo,:); %get rid of empty rows
q4=find(~all(isnan(X4resh_smoo)));
X4resh_smoo_q=X4resh_smoo(:,q4);
X4resh_smoo_q_anom=rm_mtx_mean(X4resh_smoo_q);

% do eof Filtering
eofFile4=strrep(inFile4,'.nc','.eofs.nc');
if doEofs
    XfiltStruct4=doEOF_filter_wrap(X4resh,X4resh_smoo_q_anom,X4resh_smoo,X3D4,q4,tq4_smoo,time4_smoo,lon4,lat4,neig4,nlon4,nlat4,nt4smoo,smoowin4);
    XfiltStruct4.global_attributes.history=['Created from ' inFile4];
    ncstruct2ncfile(XfiltStruct4,[eofDir eofFile4]);    
end

%return 

%% we will now assign output from the filtering to X:
disp('Testing legitimacy of [Btau0]...')
X1norm=sqrt(sum(diag(cov(XfiltStruct1.pcs.data))));
X1=XfiltStruct1.pcs.data;
X1=X1./X1norm;

%X2norm=sqrt(sum(diag(cov(XfiltStruct2.pcs.data))));
%X2=XfiltStruct2.pcs.data;
%X2=X2./X2norm;

%X3norm=sqrt(sum(diag(cov(XfiltStruct3.pcs.data))));
%X3=XfiltStruct3.pcs.data./X3norm;

%-X4norm=10000*sqrt(sum(diag(cov(XfiltStruct4.pcs.data))));
X4norm=1.0*sqrt(sum(diag(cov(XfiltStruct4.pcs.data))));
%-X4norm=100*sqrt(sum(diag(cov(XfiltStruct4.pcs.data))));


X4=XfiltStruct4.pcs.data./X4norm;

X=[X1 X4];

%X=[X1 X2 X4];
%X=[X1 X2 X3 X4];
%X=[X1 X2];
%
%--if detrending line below--- 
%X = x4detrend_x(X,tau0,0);
%--X = x4detrend_linear(X,1 );
if(eigen == 'on ')
  X = x4detrend_x(X,tau0,0);%--This meth use a eigen value regression 
end

% return 
%---- 
Ctau0=lagCov(X,X,tau0);
Co=cov(X);

% Calculate B (PS95, eq. [4])
Btau0=(1./tau0)*logm(Ctau0*pinv(Co));%
Btau0_eigs=real(eigs(Btau0,10));
if any(Btau0_eigs>0)
     error('Failed: Eigenvalues of B have real positive parts')      
end

disp('Passed: Eigenvalues of B do not have real positive parts')

%% 3) Is [Q] (determined from [B][C0]+[C0][B]'+[Q]=0) positive definite?
disp('3) Is [Q] (determined from [B][C0]+[C0][B]''+[Q]=0) positive definite?')
B=Btau0;

%--- here we detrended B ---
%--[B] = x4detrend(B,0);% (Q,1) for making plot and (Q,0) for not plotting

tau=tau0;
Ctau=Ctau0;
B_eigs=Btau0_eigs;
Q=-(B*Co+Co*B');
%--
%--
[Qeofs,Qv]=svd(Q);
if(any(diag(Qv)<0)) 
    error('Some eigenvalues of Q <0. These should be eliminated.');    
end
Qeigs=diag(Qv);
%-save(limParamFile,'tau','X','X1','X2','X3','X4','Ctau','Co','B','B_eigs','Q','Qeofs','Qeigs','Qv',...
%-save(limParamFile,'tau','X','X1','X2','X4','Ctau','Co','B','B_eigs','Q','Qeofs','Qeigs','Qv',...
save(limParamFile,'tau','X','X1','X4','Ctau','Co','B','B_eigs','Q','Qeofs','Qeigs','Qv',...
     'neig*','q*','lon*','lat*','nlon*','nlat*','inFile*','X*norm','eofFile*','eofDir');


disp(['Results saved in "' limParamFile '"']);
disp(['EOFs saved in "' eofDir '"'])
%------------------% 
%------------------% 
 end  % for iiran 1 to 100 
