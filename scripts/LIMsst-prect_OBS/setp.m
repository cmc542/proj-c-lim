function setp
%Function to set local paths and variable names. Should be called at the
%initial part of every script. It will (eventually) detect what machine
%things are running on, and set paths/environmental variable names
%accordingly.

[~,host]=system('hostname');
host=deblank(host);

switch host
  case 'Your Machine'
      % edit area here to set parameters for your machine
  otherwise
        % Setup all relevant paths and variable names that may change when        
        disp(['Running as generic'])
%       scratchDir='/data2/tra38/scratch/LIM-Output/';
%       scratchDir='/data2/cmc542/scratch/MEME/post/f.e12.FAMIPC5CN.f19_19.mvBtoga.001x/LIM-Output/';
%       scratchDir='/data/cmc542/scratch/MEME/post/f.e12.FAMIPC5CN.f19_19.mvBtoga.001x/LIM-Output/';
%       scratchDir='/data2/cmc542/scratch/MEME/LIM-Output/b.e11.B1850C5CN.f19_g16.0850cntl.001.0850-1849/';
%       scratchDir='/data2/cmc542/scratch/MEME/LIM-Output/b.e11.B1850C5CN.f19_g16.0850cntl.001.1300-1400/';
        scratchDir='/data2/cmc542/scratch/2019/C-LIM/LIM-Output/LIMsst-pdsi_LME/';
%       scratchDir='/data2/cmc542/scratch/LIM-Output/';
end
        
save sessionParams scratchDir
