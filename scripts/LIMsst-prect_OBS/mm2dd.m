%
function tout=mm2dd(tvar)
% 
%  Vsmth9 = V;
   len = length(tvar);
   imo = 1; 
   for i=1:len
       mm(i) = imo; 
       if (imo == 12)
           imo = 1; 
       else 
          imo = imo + 1 ; 
       end 
   end 
   topm = [31 28 31 30 31 30 31 31 30 31 30 31];   
   tout(1) = 0; 
   for i=2:len
       tout(i) = tout(i-1) + topm( mm(i-1) ) + 0; 
   end  
%--
