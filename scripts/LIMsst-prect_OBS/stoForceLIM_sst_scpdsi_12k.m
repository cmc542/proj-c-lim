%-------------------------------------------------
%-------------------------------------------------
disp('Stochastic forcing part (requires Q and B)')
clear
close all
%--
addpath ~/ClimX/ 
%-addpath ~/matlablib/cmc/ 
addpath ../subr/
setp
load sessionParams
%-------------------------------------------------
%-------------------------------------------------
%-------------------------------------------------
fcorr=1; 
fareaN34=1; 
fareaN34=0; 
%--
if (1==0)
   c = parcluster('local');
   c.NumWorkers = 32;
%  c.NumWorkers = 24;
%  c.NumWorkers = 12;
   parpool(c, c.NumWorkers);
end
%--
%--

%-dataDir='../data/';
%-dataDir='../../data/';
%-nYears=100;
%-nYears=10*1000;

% nr=1;
% nYears=10*1200;

  nr=1;
  nYears=100;
% nr=10;
% nYears=1000;

% nr=1;
% nr=20;
% nYears=100;

%-nr=100;
%-nYears=100;
%--
%-for iiran=1:100 
%-for iiran=1:2
 for iiran=1:1
%   iiran=1; 
    sran = ind2str(iiran,3);


nYearsExtra=1;
 nPar = 24; % this will set the number of workers in the parfor loop, if you
%nPar = 1; % this will set the number of workers in the parfor loop, if you
            % are setting up your experiments to run in parallel
            % (recommended if possible). However, it isn't required.
%-limParamFile='sst_historical_v20170921_196001-200712.Xeofs.global.22pdsiEigs.Norm10000.limParams.mat';
%-limParamFile='sst_historical_v20180603_085001-124912.Xeofs.global.22pdsiEigs.Norm10000.limParams.mat';
limParamFile=strcat('sst_historical_v20180603_085001-124912.Xeofs.global.22pdsiEigs.Norm10000.limParams.r',sran,'.mat');
dateTag=datestr(datenum(now),'yyyymmddHHMM');
disp(dateTag)

% you could modify this further to specify details of your results dir.
% note that by default a new folder will be created for EACH experiment...
% resultsDir=[scratchDir '/limOut_' dateTag '/'];
resultsDir=[scratchDir 'r' sran '/limOut_' dateTag '/'];
%return 
if ~exist(resultsDir);
    eval(['!mkdir -p ' resultsDir]);
end

save lastRun dateTag resultsDir
%% --------------------------------------------------------------------------------
% General
load(limParamFile);

nt=size(X,1);
ns=size(X,2);


nDaysInMonth=30;
nHrsInDay=24;
nMonthsInYear=12;
nHrsInRn=3;
deltaT=nHrsInRn/(nHrsInDay*nDaysInMonth);  %6hrly data = 6/(24*30)=1/120
ntRn=(nDaysInMonth*nYears*(nHrsInDay/nHrsInRn)*nMonthsInYear);
ntRnExtra=(nDaysInMonth*nYearsExtra*(nHrsInDay/nHrsInRn)*nMonthsInYear);
nMoExtra=nYearsExtra*12;
% sampling is at Nhr hourly, so there are 120 time chunks per month:
moLen=(nDaysInMonth*(nHrsInDay/nHrsInRn));
nMo=ntRn/moLen;
time=(1:nMo)';

if nYears>1000;
    nYrDig=length(num2str(nYears));
else
    nYrDig=4;
end

[~,hostname]=system('hostname');
metaDescr=['Parameters for LIM are from:' limParamFile  '.' char(10) ...
          'NetCDF4 file created by matlab on ' date char(10) ...
          'from script: ' char(10) ...
          '     ' mfilename('fullpath') char(10) ...
          'by ' getenv('USER') char(10) ...
          'using ' hostname];
%% ------------------------ X1 ------------------------
eofStruct1=nc2struct([eofDir eofFile1]);
oFileTemplate1=strrep([resultsDir inFile1],'.nc', '.Rn_%s.nc');
[varname,realm,modid,expid,runid,yrStart,moStart,yrEnd,moEnd]=filename2fileinfo(inFile1);
oFileTemplate1=strrep(oFileTemplate1,[ind2str(yrStart,nYrDig) ind2str(moStart,2)],[ind2str(1,nYrDig) ind2str(1,2)]);
oFileTemplate1=strrep(oFileTemplate1,[ind2str(yrEnd,nYrDig) ind2str(moEnd,2)],[ind2str(nYears,nYrDig) ind2str(nMonthsInYear,2)]);
oFileTemplate1=strrep(oFileTemplate1,expid,['limCntrl' dateTag ]);

history1=['From ' inFile1 char(10) ...
          ' using eofs from ' eofFile1 char(10) ...
          ' neig=' num2str(neig1) '.' char(10) ...
          metaDescr];

%% ------------------------ X2 ------------------------
%eofStruct2=nc2struct([eofDir eofFile2]);
%oFileTemplate2=strrep([resultsDir inFile2],'.nc', '.Rn_%s.nc');
%[varname,realm,modid,expid,runid,yrStart,moStart,yrEnd,moEnd]=filename2fileinfo(inFile2);
%oFileTemplate2=strrep(oFileTemplate2,[ind2str(yrStart,nYrDig) ind2str(moStart,2)],[ind2str(1,nYrDig) ind2str(1,2)]);
%oFileTemplate2=strrep(oFileTemplate2,[ind2str(yrEnd,nYrDig) ind2str(moEnd,2)],[ind2str(nYears,nYrDig) ind2str(nMonthsInYear,2)]);
%oFileTemplate2=strrep(oFileTemplate2,expid,['limCntrl' dateTag ]);
%
%history2=['From ' inFile2 char(10) ...
%          ' using eofs from ' eofFile2 char(10) ...
%          ' neig=' num2str(neig2) '.' char(10) ...
%          metaDescr];
%

%% ------------------------ X3 ------------------------
%eofStruct3=nc2struct([eofDir eofFile3]);
%oFileTemplate3=strrep([resultsDir inFile3],'.nc', '.Rn_%s.nc');
%[varnatoForceLIM_sst_ssh_taux_scpdsie,realm,modid,expid,runid,yrStart,moStart,yrEnd,moEnd]=filename2fileinfo(inFile3);
%oFileTemplate3=strrep(oFileTemplate3,[ind2str(yrStart,nYrDig) ind2str(moStart,2)],[ind2str(1,nYrDig) ind2str(1,2)]);
%oFileTemplate3=strrep(oFileTemplate3,[ind2str(yrEnd,nYrDig) ind2str(moEnd,2)],[ind2str(nYears,nYrDig) ind2str(nMonthsInYear,2)]);
%oFileTemplate3=strrep(oFileTemplate3,expid,['limCntrl' dateTag ]);

%history3=['From ' inFile3 char(10) ...
%         ' using eofs from ' eofFile3 char(10) ...
%         ' neig=' num2str(neig3) '.' char(10) ...
%         metaDescr];

%% ------------------------ X4 ------------------------
eofStruct4=nc2struct([eofDir eofFile4]);
oFileTemplate4=strrep([resultsDir inFile4],'.nc', '.Rn_%s.nc');
[varname,realm,modid,expid,runid,yrStart,moStart,yrEnd,moEnd]=filename2fileinfo(inFile4);
oFileTemplate4=strrep(oFileTemplate4,[ind2str(yrStart,nYrDig) ind2str(moStart,2)],[ind2str(1,nYrDig) ind2str(1,2)]);
oFileTemplate4=strrep(oFileTemplate4,[ind2str(yrEnd,nYrDig) ind2str(moEnd,2)],[ind2str(nYears,nYrDig) ind2str(nMonthsInYear,2)]);
oFileTemplate4=strrep(oFileTemplate4,expid,['limCntrl' dateTag ]);

history4=['From ' inFile4 char(10) ...
          ' using eofs from ' eofFile4 char(10) ...
          ' neig=' num2str(neig4) '.' char(10) ...
          metaDescr];
      
%% rescale "forcing functions"
%nQeigs=neig1+neig2+neig3+neig4;
%nQeigs=neig1+neig2+neig4;
nQeigs=neig1+neig4;
clear Qscld
for i =1:nQeigs
    Qscld(:,i)=Qeofs(:,i).*sqrt(Qv(i,i));
end 

%Subspace Indices
x1subq=(1:neig1);
%x2subq=(1+neig1:neig1+neig2);
%x3subq=(1+neig1+neig2:neig1+neig2+neig3);
%x4subq=(1+neig1+neig2+neig3:neig1+neig2+neig3+neig4);
%x4subq=(1+neig1+neig2:neig1+neig2+neig4);
x4subq=(1+neig1:neig1+neig4);

%%
%-p = gcp('nocreate'); % If no pool, do not create new one.
%-if ~isempty(p)
%-  delete(gcp)
%-end

%Uncomment these lines and comment line 132 to use parfor.
%parpool(nPar) 
%parfor n =1:nr;
 for n =1:nr;
    disp(n) 
    
    %randomize start mo
    Xinit=0.0*X(randperm(nt,1),:);
    
    % Generate stochastically forced output:
    Xrand=stoForceFn(Xinit,B,Qscld,nQeigs,nMo,deltaT,moLen,nMoExtra);    
        
    % now split up into section (e.g., x1 | x2)
    X1rand=X1norm*(Xrand(x1subq,:))';
%   X2rand=X2norm*(Xrand(x2subq,:))';
%   X3rand=X3norm*(Xrand(x3subq,:))';    
    X4rand=X4norm*(Xrand(x4subq,:))';    
    
    %X1: project back onto original field:
    X1RandMoq=X1rand*eofStruct1.eofs.data';
    X1RandMo=nan(nlon1*nlat1,nMo);
    X1RandMo(q1,:)=X1RandMoq';
    X1RandMo3D=reshape(X1RandMo,nlon1,nlat1,nMo);    

%-  eofX1_3D=reshape(eofStruct1.eofs.data,nlon1,nlat1,17);    
    eofX1_3D=reshape(eofStruct1.eofs.data,nlon1,nlat1,neig1);    
    pcX1_3D=eofStruct1.pcs.data;

    oFile1=strrep(oFileTemplate1,'%s',ind2str(n,4));
    saveLIMfn(oFile1,X1RandMo3D,lon1,lat1,time,history1)
%--
    %X2: project back onto original field:
%   X2RandMoq=X2rand*eofStruct2.eofs.data';
    % the line below is to avoid OUT OF MEMORY 
%   X2RandMoq=axb(X2rand,eofStruct2.eofs.data',0);
%   X2RandMo=nan(nlon2*nlat2,nMo);
%   X2RandMo(q2,:)=X2RandMoq';
%   X2RandMo3D=reshape(X2RandMo,nlon2,nlat2,nMo);    
%   oFile2=strrep(oFileTemplate2,'%s',ind2str(n,4));
%   saveLIMfn(oFile2,X2RandMo3D,lon2,lat2,time,history2)
%--   
    %X3: project back onto original field:
%-- X3RandMoq=X3rand*eofStruct3.eofs.data';
%   X3RandMoq=axb(X3rand,eofStruct3.eofs.data',0);
%-- X3RandMo=nan(nlon3*nlat3,nMo);
%-- X3RandMo(q3,:)=X3RandMoq';
%   X3RandMo = arand(q3,X3RandMoq);

%   X3RandMo3D=reshape(X3RandMo,nlon3,nlat3,nMo);    
%   oFile3=strrep(oFileTemplate3,'%s',ind2str(n,4));
%   saveLIMfn(oFile3,X3RandMo3D,lon3,lat3,time,history3)
%--    
    %X4: project back onto original field:
    X4RandMoq=X4rand*eofStruct4.eofs.data';
    X4RandMo=nan(nlon4*nlat4,nMo);
    X4RandMo(q4,:)=X4RandMoq';
    X4RandMo3D=reshape(X4RandMo,nlon4,nlat4,nMo);    
%-- EOF var4 
%   eofX4_3D=reshape(eofStruct4.eofs.data,nlon4,nlat4,22);    
    eofX4_3D=reshape(eofStruct4.eofs.data,nlon4,nlat4,neig4);    
    pcX4_3D=eofStruct4.pcs.data;
%--
    oFile4=strrep(oFileTemplate4,'%s',ind2str(n,4));
    saveLIMfn(oFile4,X4RandMo3D,lon4,lat4,time,history4)
    
    %% sanity check:
%-  NN34(:,n)=X3D_to_nino34(X1RandMo3D,lon1,lat1);
end
%----
end % for iiran 1 to 100 
%---- 
%--plot EOFs 

%--plot time series correlation 
  for ipc = 1:3 
      if (fareaN34 == 1) 
          Alat=[-10 0]; Alon=[360-180  360-160]; % El Nino34 
      else 
          Alat=[-5 5]; Alon=[360-60  360-40]; % El Amz NE
      end 
      prec_N34 = X3D_to_area(eofX4_3D(:,:,ipc),lon4,lat4,Alon,Alat);
      for imo=1:12
%         xsst = X1rand(imo:12:end,1);
%         xprec = X4rand(imo:12:end,ipc);
%-        xsst = pcX1_3D(imo:12:end,1);
%-        xprec = prec_N34 * pcX4_3D(imo:12:end,ipc);
          xsst = (X1norm) * pcX1_3D(imo:12:end,1);
          xprec = (1/X4norm) * prec_N34 * pcX4_3D(imo:12:end,ipc);

          xcorr(imo) = corr(xprec(:),xsst(:));
%         xcorr(imo) = xmse(xprec(:),xsst(:));
	  if (fcorr == 0) 
              xx = cov(xprec(:),xsst(:));
              xcorr(imo) = xx(1,2); 
	  end 
%         xcorr(imo) = xssclim(xprec(:),xsst(:));
      end
      figure;bar(xcorr,'c');hold on;
%     axis([0 13 0.0 1.0])
%-    axis([0 13 -1.0 1.0])
      set(gca,'XTickLabel', {'J','F','M','A','M','J','J','A','S','O','N','D'})
      shading flat
      xtitle=strcat('OMEGA[LIM-OBS] = CORR(SST [PC1] , PREC [PC',num2str( ipc ),'] )'); 
      if (fcorr == 0) 
          xtitle=strcat('OMEGA[LIM-OBS] = COV_{12}(SST [PC1] , PREC [PC',num2str( ipc ),'] )'); 
      end 
%     tl=title('OMEGA[LIM] = CORR(SST [PC1] , PREC [PCx] )'); 
      tl=title(xtitle);
  end 
%---- 
% save EOFs

 wrnc2d('eof1.sst.nc',lat1,lon1,eofX1_3D(:,:,1),-999.9);
 wrnc2d('eof2.sst.nc',lat1,lon1,eofX1_3D(:,:,2),-999.9);
 wrnc2d('eof3.sst.nc',lat1,lon1,eofX1_3D(:,:,3),-999.9);

 wrnc2d('eof1.prec.nc',lat4,lon4,eofX4_3D(:,:,1),-999.9);
 wrnc2d('eof2.prec.nc',lat4,lon4,eofX4_3D(:,:,2),-999.9);
 wrnc2d('eof3.prec.nc',lat4,lon4,eofX4_3D(:,:,3),-999.9);

%---- 
