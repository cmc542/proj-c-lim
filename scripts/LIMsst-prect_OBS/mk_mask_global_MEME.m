%----------
% clear all
% close all 
%   infile1
%   infile2
    addpath('/home/cmc542/SIx/ml_si_v6.0.0/ML_SI/si_funcs/');
    addpath('/home/cmc542/matlablib/cmc/');  
%-------
%-------CFSv2 files 
%       infiletmax = strcat('/home/cmc542/2016/ATLAS/SIx/tmax.nc');
%       infiletmin = strcat('/home/cmc542/2016/ATLAS/SIx/tmin.nc');
%-------CLIM files 
%--
        obstmaxf = strcat('x.anom.nc');
        obstminf = strcat('cfsv2rr/leaf.best.NA.LatLong_cfsv2.1982-2012.nc');

%-------output files 
        ofileleaf = strcat('./NA_LatLong_cfsv2_mask.nc');
        ofilebloom = strcat('./NA_LatLong_cfsv2_mask.nc');
%---------------
%       infile = '/data2/cmc542/scratch/MEME/post/f.e12.FAMIPC5CN.f19_19.mvBtoga.001x/to_PDSI/f.e12.FAMIPC5CN.f19_19.mvBtoga.001x.cam.h0.TS.085001-124912.nc'; 
%        ofile = '/data2/cmc542/scratch/MEME/post/f.e12.FAMIPC5CN.f19_19.mvBtoga.001x/to_PDSI/f.e12.FAMIPC5CN.f19_19.mvBtoga.001x.cam.h0.TS.085001-124912.masked.nc';  
%- 
        infile = '/data2/cmc542/scratch/MEME/LME_to_LIM/CESM-CAM5-LME/to_PDSI/b.e11.B1850C5CN.f19_g16.0850cntl.001.cam.h0.TS.085001-184912.nc';
         ofile = '/data2/cmc542/scratch/MEME/LME_to_LIM/CESM-CAM5-LME/to_PDSI/b.e11.B1850C5CN.f19_g16.0850cntl.001.cam.h0.TS.085001-184912.masked.nc';

         iniyr = 850 
%---------------
        maskfile = '/data2/cmc542/scratch/MEME/topo/USGS-gtopo30_1.9x2.5_remap_c050602.nc'
%---------------

        varnms(1,:)='TS             ';% 
%       varnms(2,:)='data           ';% 
%       varnms(3,:)='time           ';% 


%       1. read the file
%       TMAX  FCST 
       [lat lon time invar data_fill] = xreadnc3(infile,strtrim(varnms(1,:)),'lat','lon','time',['_FillValue   ']);
%       TMIN FCST 
%      [lat lon time datamin data_fill] = xreadnc3(obstminf,strtrim(varnms(2,:)),'lat','lon','data',['_FillValue   ']);

% 
       [lat lon mask data_fill] = xreadnc2d(maskfile,strtrim('LANDFRAC'),'lat','lon',['_FillValue']);


%      figure;contourf(lon,lat,datamin') 
%      figure;contourf(lon,lat,datamax') 

%      datamax(find( isnan(datamin) == 1 ) ) = NaN;  
      [nrow ncol ntime]= size(invar); 
       for it=1:ntime 
           clear xtmp 
	   xtmp = invar(:,:,it); 
%          xtmp (find( mask ~= 0 ) ) = NaN;  
%          xtmp (find( mask == 1 ) ) = NaN;  
%          xtmp (find( mask >= 0.75 ) ) = NaN;  
%          xtmp (find( mask >= 0.5 ) ) = NaN;  
           xtmp (find( mask >= 0.25 ) ) = NaN;  
	   invar2(:,:,it) = xtmp; 
       end 
       invar = invar2; 

%      figure;contourf(lon,lat,datamax') 
%      datamax(find( isnan(datamin) == 0 ) ) = 1;  
%      datamax(find( isnan(datamax) == 0 ) ) = 0;  
%      figure;contourf(lon,lat,datamax') 
%    
%---------------------
%--- To FILE ---------
%           yrcase = 2004; 
%           tunit = 'years  ';
            xtmp = strcat(num2str(iniyr),'-01-01 00:00:00');
            time_ini = ['years since ',xtmp];
%          [nrow ncol ntime] = size(datamax);
%           t = [0:ntime-1];
%
%           wrnc2d(ofileleaf,lat,lon,datamax,data_fill);
%------------- 

       t_natt(1,:) = 'calendar    ';
%      t_vatt(1,:) = '365_day                         ';
       t_vatt(1,:) = 'noleap                          ';
       t_natt(2,:) = 'units       ';
%      t_vatt(2,:) = 'YYYYMM                          ';
%      t_vatt(2,:) =  time_ini;
       t_vatt(2,:) = 'days since 0850-01-01 00:00:00  ';
       t_natt(3,:) = 'standardname';
       t_vatt(3,:) = 'time                            ';
%      t_natt(4,:) = 'axis        ';
%      t_vatt(4,:) = 'T                               ';

      [nrow ncol ntime] = size(invar);
       t = [0:ntime-1];
%--  
       timeT = mm2dd(t);%                convert months to days 
%      dateT = mm2yymmdd(iniyr,1,1,t);%  iniyr,inimo,inidy 
       dateT = mm2yymm(iniyr,1,1,t);%  iniyr,inimo,inidy 
       datesecT = 0.0*ones(size(t));%    not using hourly resolution
%--
       natt(1,:) = 'long_name ';
       vatt(1,:) = 'Surface temperature (radiative)';
       natt(2,:) = 'units     ';
       vatt(2,:) = 'deg_K                          ';
       natt(3,:) = '_FillValue';
       vatt(3,:) = '_FILLVALUE is data_fill        ';
       data_fill = 1.e36;

       varname = 'TS';%       varname = 'SST';
       vunits = 'K';
%--
       wrnc3dll_x2(ofile,lat,lon,timeT,invar,t_natt,t_vatt,data_fill,varname,natt,vatt);
%--
