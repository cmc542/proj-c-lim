%-  
%-
clear all
close all
addpath ~/matlablib/cmc/ 
%-
% GPH 
%-

  mm='nov-apr';

% filegph = '/data2/cmc542/scratch/2019/Pheno/post/f.e12.FCLM45SP.f19_19.control.001z/to_PDSI/f.e12.FCLM45SP.f19_19.control.001z.clm2.h0.pdsi_TH.197501-200512.nc'; 
% filegph = '/data2/cmc542/scratch/2019/Pheno/post/f.e12.FCLM45SP.f19_19.latephen.001z/to_PDSI/f.e12.FCLM45SP.f19_19.latephen.001z.clm2.h0.pdsi_TH.197501-200512.nc'; 
  filegph = '/data2/cmc542/scratch/2019/Pheno/post/f.e12.FAMIPC5CN.f19_19.mvBtoga.001x/f.e12.FAMIPC5CN.f19_19.mvBtoga.001x.clm2.h0.pdsi_TH.085001-185912.nc';
% filegph = '/data2/cmc542/scratch/2019/Pheno/post/f.e12.FAMIPC5CN.f19_19.mvBtoga.001x/f.e12.FAMIPC5CN.f19_19.mvBtoga.001x.clm2.h0.RAIN.085001-185912.nc'; 

  ncid = netcdf.open(filegph,'NC_NOWRITE');
  rdata = netcdf.getVar(ncid, netcdf.inqVarID(ncid,'pdsi') );
% rdata = netcdf.getVar(ncid, netcdf.inqVarID(ncid,'RAIN') );

  lat = netcdf.getVar(ncid, netcdf.inqVarID(ncid,'lat') );
  lon = netcdf.getVar(ncid, netcdf.inqVarID(ncid,'lon') );

% data_fill = netcdf.getAtt(ncid,3,'_FillValue'); 
  data_fill = netcdf.getAtt(ncid,3,'missing_value'); 
% data_fill = netcdf.getAtt(ncid,4,'missing_value'); 
  netcdf.close(ncid);

%-
% gph (find(isnan(rdata) == 1)) = data_fill; 
  data_fill = single( data_fill ); 
  rdata ( find(rdata == data_fill )) = NaN;
%-only 100 years 
  rdata = rdata(:,:,1:1200); 
%- 
%-reduce domain 
%-  
if (1==1)
%  mnlat=15; mxlat=60;
   mnlat=20; mxlat=55;
   mnlon=220; mxlon=300;  % -140 -> -60 

   idwlat = floor( (mnlat - lat(1))/(lat(2)-lat(1)) + 1 );
   iuplat = ceil ( (mxlat - lat(1))/(lat(2)-lat(1)) + 1 );

   ilflon = floor( (mnlon - lon(1))/(lon(2)-lon(1)) + 1 );
   irglon = ceil ( (mxlon - lon(1))/(lon(2)-lon(1)) + 1 );

  [nrow ncol ntime ] = size(rdata);

   for it = 1:ntime
%     nwdata(:,:,it) = rdata(idwlat:iuplat,ilflon:irglon,it);
      nwdata(:,:,it) = rdata(ilflon:irglon,idwlat:iuplat,it);
   end
%--
   nwlon = lon(ilflon:irglon);
   nwlat = lat(idwlat:iuplat);

   clear rdata lon lat;
   rdata = nwdata;lon = nwlon;lat = nwlat;
end

%--
% Alat=[-5 5]; Alon=[360-150  360-130];
% Alat=[30 35]; Alon=[360-110  360-100];
  Alat=[35 42]; Alon=[360-95 360-85];
  sst_N34 = X3D_to_area(rdata,lon,lat,Alon,Alat);

% gph = rdata; 

% gph = (rdata(:,:,1:12:end)+rdata(:,:,2:12:end)+rdata(:,:,3:12:end))./3; 
  gph = (rdata(:,:,6:12:end)+rdata(:,:,7:12:end))./2; 

  gphmean = mean(gph,3);
 [nrow ncol ntime] = size(gph); 
  for it=1:ntime  
     gph(:,:,it)  = gph(:,:,it) - gphmean; 
  end 
%-detrend 
  for ix=1:nrow
      for iy=1:ncol
          gph(ix,iy,:)  = detrend ( squeeze(gph(ix,iy,:)) );
      end 
  end 
%- 
  figure(1); contourf(lon,lat,gph(:,:,1)'); 
% 
  gph_raw = gph; 
  for icol=1:ncol
     gph(:,icol,:)  = gph(:,icol,:) * sqrt(cos(lat(icol)*pi/180.)); 
  end 
  figure(2); contourf(lon,lat,gph(:,:,1)'); 
             title('ANOMALY FIELD') 
% 
% 
   styear = 1975;endyear = 2005;
   path_store ='./';
   
   expl_eofr  = strcat(path_store,'expl_roteof.gph.1975-2005.',mm,'.txt');
   expl_eof     = strcat(path_store,'expl_eof.gph.1975-2005.',mm,'.txt'); 
   
% 
% LOAD
% 

%load(infile); 
%SPI2D = dataspi; 
SPI2D = gph; 
%----------------------------------------------------% 
%----------------------------------------------------% 
PPmatI = SPI2D; 
[ny,nx,nt] = size(PPmatI);
for i=1:nx
    for j=1:ny 
        nnan = length( find ( isnan(PPmatI(j,i,:)) == 1) ); 
        if (nnan == 0) 
           xpp(j,i,:)=PPmatI(j,i,:); 
 	else 
           xpp(j,i,1:nt)=NaN; 
        end
    end 
end 

nmiss = length( find ( isnan(xpp(:,:,1)) == 1) ); 
nreal = nx*ny-nmiss;
%% 
%% Rarray
%% construct the reduced array
%% 
for it=1:nt
    xtmp = xpp(:,:,it);
%    it 
    idxpp = find(~isnan(xtmp)); % to reconstruct 2D map 
%   realpt = find(~isnan(iPPmat));
%   Rarray(iy,:) = iPPmat(realpt)';
    Rarray(it,:) = xtmp(find(~isnan(xtmp)))';
end
%----------------------------------------------------% 
%%%%%%%%%%%%
% SVD
%%%%%%%%%%%%
disp('computing SVD ...') 
[U,S,EOFs] = svd(Rarray);
% 
%----EOF rotation-------
% 
nmod = 5;  % number of modes retained 
%-nmod = 10; 

%EOFr = rotatefactors(EOFs(:,1:nmod)); % Varimax Rotation: columns 1-5
%PCr = Rarray*EOFr;

EOFr = EOFs(:,1:nmod); % Varimax Rotation: columns 1-5
PCr = Rarray*EOFr;
PCr2 = U(:,1:nmod);
% Normalize 
  for imod = 1:nmod
      stdPCr(imod) = std(PCr(:,imod));
      PCr(:,imod) = PCr(:,imod) ./ std(PCr(:,imod));
      EOFr(:,imod) = EOFr(:,imod) .* stdPCr(imod);
  end

%-----------------% 
% EOFr3D (map) ---% 
%-----------------% 
year = styear:endyear;
ieof = nan(ny,nx); % initial value 
for imod = 1:nmod
    ieof(idxpp) = EOFr(:,imod);
    EOFr3D(:,:,imod) = ieof;
end 

 wrnc2d('eof1.gph.nc',lat,lon,EOFr3D(:,:,1));
 wrnc2d('eof2.gph.nc',lat,lon,EOFr3D(:,:,2));
 wrnc2d('eof3.gph.nc',lat,lon,EOFr3D(:,:,3));

figure;contourf(flipud( EOFr3D(:,:,1)' ))
       title('EOF1 FIELD') 
figure;contourf(flipud( EOFr3D(:,:,2)' ))
       title('EOF2 FIELD') 
figure;contourf(flipud( EOFr3D(:,:,3)' ))
       title('EOF3 FIELD') 

figure; 
     plot(PCr2(:,1))   
     title('PC1 TIME SERIES') 
figure; 
     plot(PCr2(:,2))   
     title('PC2 TIME SERIES') 
figure; 
     plot(PCr2(:,3))   
     title('PC3 TIME SERIES') 

%------------------% 
  tunit = 'years  ';
  ntime = length(PCr2);
  t = [0:ntime-1];
%time_ini = 'days since 1971-01-01 00:00:00';
 time_ini = 'years since 1981-01-01 00:00:00';
 xvar=PCr2(:,1);wrnc1d('pc1.gph.nc',t,xvar,tunit,time_ini);
 xvar=PCr2(:,2);wrnc1d('pc2.gph.nc',t,xvar,tunit,time_ini);
 xvar=PCr2(:,3);wrnc1d('pc3.gph.nc',t,xvar,tunit,time_ini);


%-figure(1);contourf(EOFr3D2(:,:,1)')
%-figure(2);contourf(EOFr3Draw(:,:,1)')

%-figure(1);contourf(EOFr3D2(:,:,2)')
%-figure(2);contourf(EOFr3Draw(:,:,2)')

%-figure(1);contourf(EOFr3D2(:,:,3)')
%-figure(2);contourf(EOFr3Draw(:,:,3)')

%----------------------
%
%--------------------
%  EOF variance 
%--------------------
% meth 1: 
[M xtmp] =  size(S); 
[M] =  min(size(S)); 
for im=1:M
  lamd(im) = S(im,im); 
end 
 expvar = (lamd.^2) ./ sum(lamd.^2); 



 znor = icdf('normal',0.975,0,1.0)
%N = 300;  
%N = 1000;  
 N = 30;  
 xlamd = 14.02; 
 xlamd = 12.61; 
 xlamd = 10.67; 
 xlamd = 10.43; 
%dlamd = xlamd*znor*sqrt(2/N)
 dlamd = expvar*znor*sqrt(2/N)
 x=[1:10] 
 %figure;plot(x,expvar(1:10),'o','MarkerSize',10,x,expvar(1:10)+dlamd(1:10),'+r',x,expvar(1:10)-dlamd(1:10),'+r') 
 figure;
 plot(x,expvar(1:10),'o','MarkerSize',10,'MarkerFaceColor','b');
 hold on; 
 for ix=1:10
  xline = [x(ix)  x(ix)];  yline  = [expvar(ix)-dlamd(ix) , expvar(ix)+dlamd(ix)];
  line(xline,yline,'LineStyle','-','LineWidth',2,'Color','r')
 end 
 ix=1; 
 xline = [x(ix)  x(ix)];  yline  = [expvar(ix)-dlamd(ix) , expvar(ix)+dlamd(ix)];
 line(xline,yline,'LineStyle','-','LineWidth',3,'Color','r')

 plot(x,expvar(1:10),'o','MarkerSize',10,'MarkerFaceColor','b');

 
 title('Eigenvalue Spectrum: Significant EOFs at 95% Confidence Interval','FontSize',14,'FontWeight','bold');
 xlabel('(Eigenvalue Number)','FontSize',13);
 ylabel('(Variance Explained)','FontSize',13);

% 
% PART III 
% 

%
% UDEL-PREC 
% 
% precfile = '../work_data/prec.nov.apr.nc' 
% precfile = '/data/cmc542/prec/cmap/precip_Amon_CMAP_historical_v2.5x2.5_197901-201812.tropics.nc';
% precfile = '/data2/cmc542/scratch/MEME/LME_to_LIM/CESM-CAM5-LME/to_PDSI/PRECT_Amon_b-e11-B1850C5CN-0850cntl_f19-g16_001_085001-184912.tropics.nc';

  precfile = '/data2/cmc542/scratch/2019/Pheno/post/f.e12.FAMIPC5CN.f19_19.mvBtoga.001x/f.e12.FAMIPC5CN.f19_19.mvBtoga.001x.clm2.h0.H2OSOI-0x5m.085001-185012.nc';


  ncid = netcdf.open(precfile,'NC_NOWRITE');
%-ncid = netcdf.open(filegph,'NC_NOWRITE');
% prec = netcdf.getVar(ncid, netcdf.inqVarID(ncid,'PRECT') );

% prec = netcdf.getVar(ncid, netcdf.inqVarID(ncid,'pdsi') );
  prec = netcdf.getVar(ncid, netcdf.inqVarID(ncid,'H2OSOI') );
% prec = netcdf.getVar(ncid, netcdf.inqVarID(ncid,'RAIN') );

  lat = netcdf.getVar(ncid, netcdf.inqVarID(ncid,'lat') );
  lon = netcdf.getVar(ncid, netcdf.inqVarID(ncid,'lon') );
% fillvalue  = netcdf.getAtt(ncid,3,'_FillValue');
%-data_fill = netcdf.getAtt(ncid,3,'missing_value');

  netcdf.close(ncid);
% prec(find(prec==fillvalue)) = NaN;
  prec(find(prec==data_fill)) = NaN;

  prec = prec(:,:,1:1200); 

% Alat=[-10 0]; Alon=[360-180  360-160];
  Alat=[35 42]; Alon=[360-95 360-85];
  prec_N34 = X3D_to_area(prec,lon,lat,Alon,Alat);

  for imo=1:12
      clear xprec xsst 
      xprec = prec_N34(imo:12:end);
      xsst = sst_N34(imo:12:end);
      xcorr(imo) = corr(xprec(:),xsst(:));  
  end 
%---- 
%----
      figure;bar(xcorr,'c');hold on;
%     axis([0 13 0.0 1.5])
      set(gca,'XTickLabel', {'J','F','M','A','M','J','J','A','S','O','N','D'})
      shading flat
%     tl=title(ax(1),'STD(SST [N34] ) a) mv-blocks'); 
      tl=title('(a) OMEGA = CORR(SST,PREC)');
%---- 
%----


  prec2 = prec; 
  clear prec ; 
% mm 2 DJF
% prec = (prec2(:,:,1:12:end)+prec2(:,:,2:12:end)+prec2(:,:,3:12:end))./3; 
  prec = (prec2(:,:,6:12:end)+prec2(:,:,7:12:end))./2; 

% contourf(lon,lat,prec(:,:,1)')
%
% UDEL-PREC 
% 

% tempfile = '../work_data/temp.nov.apr.nc' 

% ncid = netcdf.open(tempfile,'NC_NOWRITE');
% temp = netcdf.getVar(ncid, netcdf.inqVarID(ncid,'data') );
% lat = netcdf.getVar(ncid, netcdf.inqVarID(ncid,'lat') );
% lon = netcdf.getVar(ncid, netcdf.inqVarID(ncid,'lon') );
% fillvalue  = netcdf.getAtt(ncid,3,'_FillValue');

% netcdf.close(ncid);
% temp(find(temp==fillvalue)) = NaN;

% 
% correlation  and t-test 
% 

% pcsea = PCr2(1:23,:); 
  pcsea = PCr2(:,:); 

  pctime = [1979:2018]; 
  save('pcs.1-5.gph.mat','pcsea','pcsea'); 

  for imod=1:3; 
   [nrow ncol ntime]=size(prec);
    r2d = NaN(nrow,ncol);
    rtest = NaN(nrow,ncol);
    for irow=1:nrow
        for icol=1:ncol
            x = squeeze(prec(irow,icol,:))';
            y = pcsea(:,imod)';

            if (length(x) == length( find(isnan(x)==1)))
                r2d(irow,icol)=NaN;
                rtest(irow,icol) = NaN;
            else
                xcov = cov([x' y']);covxy = xcov(2,1);
                varx = xcov(1,1);vary = xcov(2,2);

                a1 = covxy/varx;  % regression  coefficient 
                r = covxy/( sqrt(varx) * sqrt(vary) ) ;  % regression  coefficient 
                r2d(irow,icol)=r;
%              [rdw  rup]=corrtest(23,r,0.90,'twotails'); 
               [tcrit t]=corrttest(ntime,r,0.90,'twotails');
                   rtest(irow,icol) = t;
%                if(r<rup & r>rdw) 
%                  rtest(irow,icol) = 1.0; 
%                else 
%                   rtest(irow,icol) = -1 .0; 
%                end
            end
        end
    end
    r2dprec(:,:,imod)=r2d;
    rtestprec(:,:,imod)=rtest;
  end 

wrnc2d('r.prec.pc1.nc',lat,lon,r2dprec(:,:,1));
wrnc2d('r.prec.pc2.nc',lat,lon,r2dprec(:,:,2));
wrnc2d('r.prec.pc3.nc',lat,lon,r2dprec(:,:,3));

wrnc2d('ttest.prec.pc1.nc',lat,lon,rtestprec(:,:,1));
wrnc2d('ttest.prec.pc2.nc',lat,lon,rtestprec(:,:,2));
wrnc2d('ttest.prec.pc3.nc',lat,lon,rtestprec(:,:,3));

  figure; 
       contourf(lon,lat,r2dprec(:,:,1)')
       title('PREC FIELD CORRELATION - MODE 1') 
% figure;
%      contourf(lon,lat,rtestprec(:,:,1)')
%      title('PREC FIELD t-TEST - MODE 1')


return 


% TEMP correlation  and t-test 


% for imod=1:3; 
%  [nrow ncol ntime]=size(temp);
%   r2d = NaN(nrow,ncol);
%   rtest = NaN(nrow,ncol);
%   for irow=1:nrow
%       for icol=1:ncol
%           x = squeeze(temp(irow,icol,:))';
%           y = pcsea(:,imod)';

%           if (length(x) == length( find(isnan(x)==1)))
%               r2d(irow,icol)=NaN;
%               rtest(irow,icol) = NaN;
%           else
%               xcov = cov([x' y']);covxy = xcov(2,1);
%               varx = xcov(1,1);vary = xcov(2,2);

%               a1 = covxy/varx;  % regression  coefficient 
%               r = covxy/( sqrt(varx) * sqrt(vary) ) ;  % regression  coefficient 
%               r2d(irow,icol)=r;
%              [tcrit t]=corrttest(ntime,r,0.90,'twotails');
%                  rtest(irow,icol) = t;
%           end
%       end
%   end
%   r2dtemp(:,:,imod)=r2d;
%   rtesttemp(:,:,imod)=rtest;
% end 


% wrnc2d('r.temp.pc1.nc',lat,lon,r2dtemp(:,:,1));
% wrnc2d('r.temp.pc2.nc',lat,lon,r2dtemp(:,:,2));
% wrnc2d('r.temp.pc3.nc',lat,lon,r2dtemp(:,:,3));

% wrnc2d('ttest.temp.pc1.nc',lat,lon,rtesttemp(:,:,1));
% wrnc2d('ttest.temp.pc2.nc',lat,lon,rtesttemp(:,:,2));
% wrnc2d('ttest.temp.pc3.nc',lat,lon,rtesttemp(:,:,3));

% figure; 
%      contourf(lon,lat,r2dtemp(:,:,1)')
%      title('TEMP FIELD CORRELATION - MODE 1') 
% figure; 
%      contourf(lon,lat,rtesttemp(:,:,1)')
%      title('TEMP FIELD t-TEST - MODE 1') 
%--
