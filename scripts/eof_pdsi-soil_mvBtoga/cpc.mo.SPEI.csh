#! /bin/csh 

set storage = /data/cmc542/GCM/cfsv2/forecast/Aday/

set storage2 = /home/eas_shared/ault/GCM/cfsv2/forecast/Aday/
set storage3 =  /home/web/ecrl/Misc/SpringOnset/SpringCasting2016/

set source = /data/cmc542/GCM/cfsv2/forecast/Aday/
set exepath = /home/cmc542/2016/ATLAS/spei/CPC/

set path = ($path /usr/local/bin)
set path = ($path /home/exclude/matlab/R2016a/bin)

setenv path
# 
#----------
# 
 cd $exepath 
#
#----------
setenv grads21 /home/cmc542/bin/grads21
setenv GASCRP /home/cmc542/grads/gs
setenv GADDIR /home/cmc542/grads/grads-2.1.0/data/
#----------
 set imo = $1
 set iyr = $2
#----------
 echo "#####" 
 echo "#####" 
#----------
#----------
#   set icase = 1,2,6,12 
#   set icase = 24 need to get more files previous to 2015 (this is pending) 
#   set imo = 01 
#   set imo = 02 
    
#   set imo = 10 
#   set imo = 11 
#   set iyr = 2016
    set rainfiles = rain.files.txt
    set pefiles = pe.files.txt
#-------------
#-------------
#-------------
    foreach icase (1 3 6 12) 
#   several lines to make the rain.files.txt  
#   several lines to make the pe.files.txt  
        rm -rf $rainfiles 
        rm -rf $pefiles 
        set t1 = `expr $icase - 1` 
        set tn =  0 
        foreach fdd (`awk -v II=$t1 -v NN=$tn 'BEGIN{for(i=II;i>=NN;i=i-1) {if(i<10) i="0"i;print i}}'`)
            set yyyymm = `$exepath/pre_date.xmo.csh $iyr$imo  $fdd` 
            set yyyy = `echo $yyyymm | awk '{print substr($1,1,4)}'`
            set mm = `echo $yyyymm | awk '{print substr($1,5,2)}'`
#           echo $yyyymm 
            echo /data/cmc542/prec/CPC/Amon/prec/CPC/rain.$yyyy.$mm.nc 
            echo /data/cmc542/prec/CPC/Amon/prec/CPC/rain.$yyyy.$mm.nc  >> $rainfiles
# 
            echo /data/cmc542/tas/GHNC_CAMS/Amon/pe/GHNC_CAMS/pe.Amon.GHNC_CAMS.$yyyy.$mm.nc
            echo /data/cmc542/tas/GHNC_CAMS/Amon/pe/GHNC_CAMS/pe.Amon.GHNC_CAMS.$yyyy.$mm.nc >> $pefiles
        end 
#
        echo $icase" "$imo" "$iyr" "$rainfiles 
        echo $icase" "$imo" "$iyr" "$pefiles 
#
        echo matlab -nodisplay -r "icase=$icase;imo=$imo;iyr=$iyr;pefiles='$pefiles';rainfiles='$rainfiles';mkspei_cpc_v3_mode2;exit"
#       SPEI path defined in matlab: /data/cmc542/prec/CPC/Amon/spei/CPC/ 
#       SPEI file defined as follow: /data/cmc542/prec/CPC/Amon/spei/CPC/spei.CPC.6mo.2016.07.nc 
        matlab -nodisplay -r "icase=$icase;imo=$imo;iyr=$iyr;pefiles='$pefiles';rainfiles='$rainfiles';mkspei_cpc_v3_mode2;exit"
# 
#       exit 
# 
        set spitype  = ${icase}mo
        set ncfile = /data/cmc542/prec/CPC/Amon/spei/CPC/spei.CPC.${spitype}.${iyr}.${imo}.nc
#       set pngfile = '1mo-spi.mar2002.ST4.NE.png'
#       set iyr = 2002 ; defined above 
#       set imo = 3    ; defined above 

        set pngfile = /data/cmc542/prec/CPC/Amon/spei/CPC/spei.CPC.${spitype}.${iyr}.${imo}.NY.png
        $grads21 -bpc "spi.ST4.NY.gs $ncfile $pngfile $spitype $imo $iyr"

        set pngfile = /data/cmc542/prec/CPC/Amon/spei/CPC/spei.CPC.${spitype}.${iyr}.${imo}.NE.png
        $grads21 -bpc "spi.ST4.NE.gs $ncfile $pngfile $spitype $imo $iyr"

        set pngfile = /data/cmc542/prec/CPC/Amon/spei/CPC/spei.CPC.${spitype}.${iyr}.${imo}.US.png
        $grads21 -bpc "spi.ST4.US.gs $ncfile $pngfile $spitype $imo $iyr"

#       exit 
#       exit 
    end 
#   exit 
#---------------#  
#---------------#  
