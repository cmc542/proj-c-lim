*===================================================
*function cbar(x,y,k)
function cbar(args)
x=subwrd(args,1) 
y=subwrd(args,2) 
k=subwrd(args,3) 
drawon=subwrd(args,4)
cthick=subwrd(args,5)
*-
dx=subwrd(args,6)
dy=subwrd(args,7)
*-
 'query shades'
  shdinfo = result
  if (subwrd(shdinfo,1)='None')
    say 'Cannot plot color bar: No shading information'
    return
  endif
*
  cnum = subwrd(shdinfo,5)
*
*  Plot colorbar
*
*  x=1
*  y=0
*  k=2 
*
*  dx=0.12;dy=0.18
*  dx=0.14;dy=0.20

*  dx=0.20;dy=0.18
*  dx=0.20;dy=0.233
*  dx=0.20;dy=0.226
   sizx=0.13;sizy=0.12;
   sizx=0.13;sizy=0.13;
*  drawon='on'
* 
  

    x=x
   x0=x
    y=y
   y0=y
 
* 'set string 1 l 2 0' ; 'set strsiz 0.06 0.07'
 'set string 1 l 2 0' ; 'set strsiz 0.14 0.14'
  num = 0
  while (num<cnum)
 
    rec = sublin(shdinfo,num+2)
    col = subwrd(rec,1)
    hi  = subwrd(rec,3)
     'set line 'col' '1' '2''  
*    'draw recf 'x-dx' 'y' 'x' 'y+dy
     'draw recf 'x-dx' 'y' 'x' 'y+dy
*    'set line 1  1 10'
     'set line 1  1 6'
*    'draw rec  'x-dx' 'y' 'x' 'y+dy
     'draw rec  'x-dx' 'y' 'x' 'y+dy
    if(num<cnum-1)
      'set strsiz  0.06' 
      'set strsiz  0.11 0.12' 
*      'set string 1 c ''3 60' 
*     for rotated 
*     'set string 1 l 3 60' 
      'set strsiz  'sizx' 'sizy''
      'set string 1 l 'cthick' 0' 
*     'draw string 'x-dx*0' 'y+1.55*dy' 'hi
*     'draw string 'x+dx*0.2' 'y+1.33*dy' 'hi
      'draw string 'x+dx*0.2' 'y+1.03*dy' 'hi
      'set string 1 c 3 0' 
    endif
    num = num + 1
*   for vertical 
    y=y+dy
* for horizontal 
*    x=x+dy
  endwhile
  y=y0
  x=x0
 
  if(k=1)
           "draw string "x-0.12" "y+0.36" ms"
           'set strsiz 0.03 0.04'
           "draw string "x+0.00" "y+0.41" -1" 
  endif
  if(k=2); "draw string "x+0.25" "y+0.55" ww`30`1m"
           'set strsiz 0.04'
           "draw string "x+0.15" "y+0.62" -2" ;endif
  if(k=3); "draw string "x+0.25" "y+0.55" m`30`1s"
           'set strsiz 0.04'
           "draw string "x+0.25" "y+0.62" -1" ;endif
