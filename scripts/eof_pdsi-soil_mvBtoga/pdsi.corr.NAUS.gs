*--
*$grads21 -bpc "six.bloom.cfsv2.US.gs $bloomnc_avg $bloomnc_avgpng $title_1 $title_2"
*--
 function plotSPI(args)
  ncfile = subwrd(args,1);
  pngfile = subwrd(args,2);
  spitype = subwrd(args,3);
  imo = subwrd(args,4);
  iyr = subwrd(args,5);

  'reinit' 
   cthick = 12;
   cthickb = 6;

  ncfile = 'r.prec.pc1.nc' 
  pngfile = 'r.prec.pc1.png' 
  title1a = 'CORR(PC1[PDSI], V-Soil-C)';

* ncfile = 'r.prec.pc2.nc' 
* pngfile = 'r.prec.pc2.png' 
* title1a = 'CORR(PC2[PDSI], V-Soil-C)';

* ncfile = 'r.prec.pc3.nc' 
* pngfile = 'r.prec.pc3.png' 
* title1a = 'CORR(PC3[PDSI], V-Soil-C)';

  spitype = 'PDSI' 
  imo = '06' 
  iyr = '1976'
*---
  imo = imo + 0
* 'reinit'
*pathref="/home/cmc542/2016/indices/pdsi_cook/pdsi.narr.NE.gs"
*----------------------*
   smo.1=Jan;smo.2=Feb;smo.3=Mar;smo.4=Apr;smo.5=May;smo.6=Jun;
   smo.7=Jul;smo.8=Aug;smo.9=Sep;smo.10=Oct;smo.11=Nov;smo.12=Dec lxxhJun;
*----------------------*
*----------------------* 
*  ncfile = '/data/cmc542/prec/CPC/Amon/spi/CPC/spi.CPC.3mo.2016.06.nc'
*  pngfile = '1mo-spi.mar2002.ST4.NE.png'
*  title1a = spitype'-SPEI (CPC)';
*  title2 = smo.imo' 'iyr
*  title1a = 'CORR(PC1[PDSI], PDSI)';
   title2 = 'mvBlocks'
*----------------------* 
*----------------------* 
*--------------------------------------------------------------------------------* 
*--------------------------------------------------------------------------------* 
*----------------------* 
'sdfopen 'ncfile''
*'open 'ncfile''

 'q gxinfo'
  say result
  xline=sublin(result,3);xmax=subwrd(xline,6);xmin=subwrd(xline,4)
  xline=sublin(result,4);ymax=subwrd(xline,6);ymin=subwrd(xline,4)
  dx=(xmax-xmin)/2;dy=(ymax-ymin)/2

  xl=xmin+0.7;xr=xl+dx*0.95;
* yt=ymax-0.1;yb=yt-dy*0.58;
  yt=ymax-0.1;yb=yt-dy*0.78;

 'set parea 'xl' 'xr' 'yb' 'yt''
*---------

'set grads off'
'set frame off'
'set xlab off'
'set ylab off'

'set mpt 1 off'

*---------------* 
* the NAM region 
*'set lat 25 40'
*'set lon -120 -100'
* US region 
*'set lat 15 49'
*'set lon -125 -80'
* MX region 
*'set lat 15 38'
*'set lon -124 -80'
* US-MX region 
* 'set lat 20 50'
* 'set lon 230 300'
* US- region 
  'set lat 24.0 50'
  'set lon 230 300'
* 
* US-Can-MX region 
  'set lat 20.0 60'
  'set lon 230 300'
* 
*   var   * 
ntime=12*63
*ntime=37
*ntime=12*20
*ntime=12*5

*scale = 86400;* mm/day 
*scale = 1.0/30;* mm/day
 scale = 1.0;* mm/day
*'set time 'initime''
* say resu
  
'define xave=data'
*'define xave=spei'
'define xvar = xave'

*'define xvar = smth9(smth9(smth9(smth9(smth9(smth9(xave))))))'
*'define xvar = smth9(smth9(smth9(smth9(smth9(smth9(xave))))))'
*'define xvar = smth9(smth9(smth9(smth9(smth9(smth9(xave))))))'
*'define xvar = smth9(smth9(smth9(smth9(smth9(smth9(xave))))))'

'set gxout shaded'
'set grid off'

'set ylopts 1 'cthick' 0.15'
'set xlopts 1 'cthick' 0.15'
*--------------  
*'redblue';ccols = '0 25 24 23 22 21'
*'yell2red';ccols = '0 21 23 25 27 29'
*'yell2red';ccols = '28 25 23 21 0 21 23 25 29'
*
'yell2blue';ccols = '38 35 33 31 0 29 25 24 21'
*clevs = '2.0 4.0 6.0 8.0 10.0'
*clevs = '-2.5 -2 -1.5 -1 1 1.5 2.0 2.5'
*clevs = '-.25 -.2 -.15 -0.1 0.1 .15 .20 .25'
 clevs = '-.8 -0.6 -.4 -0.2 0.2 .4 .6 .8'
 clevs = '-.4 -0.3 -0.2 -0.1 0.1 .2 .3 .4'
'yell2blue';ccols = '38 37 35 34 33 31 0 29 27 25 24 23 21'
            clevs = '-0.6 -0.5 -.4 -0.3 -0.2 -0.1 0.1 .2 .3 .4 0.5 0.6'
 'set clevs 'clevs''
 'set ccols 'ccols''
 'd xvar'
*'cbarv 'xr+0.15' 'yb+0.70' 0'
 'cbarv.big.3.gs 'xr+0.275' 'yb+0.785' 0 on 12 0.22 0.21'
 'set strsiz 0.12 0.12'
*-------
'set gxout contour'
'set clevs 'clevs''
'set ccolor 1';'set clab off';'set cthick 1'
'd xvar'
*---
*-remove values over Canada and Mexico------
'set shpopts 0'
'set line 0' 
*--'draw shp /data/cmc542/shp/CAN/province';* Canada
*'set shpopts 0'
'set line 0' 
'set shpopts 0'
*--'draw shp /data/cmc542/shp/MEX/mex_adm1';* Mexico 
*i=1;
*while (i<=31)
*'draw shp /data/cmc542/shp/MEX/mex_adm1 'i'';* Mexico 
*  i=i+1
*endwhile 
*-------

*'basemap O 0 1 L' 
'basemap O 0 1 H' 

*--  U.S. map ---
'set shpopts -1'
'set line 1' 
'draw shp /data/cmc542/shp/US1/s_10nv15';* Canada
*-------
'draw shp /data/cmc542/shp/MEX/mex_adm1';* Mexico 
'draw shp /data/cmc542/shp/CAN/province';* Canada
*----------

'set strsiz 0.13 0.14'
'set string 1 l 'cthick''
'draw string 'xl+0.05' 'yt-0.55' 'title1a''
*'set strsiz 0.12 0.12'
*'draw string 'xl+0.0' 'yt-0.68' 'title1b''
*'draw string 'xl+0.0' 'yb+0.74' 'title1b''



'set strsiz 0.13 0.13'
'set string 1 l 'cthick''
'draw string 'xl+0.05' 'yb+0.90' 'title2''


'myframe 1 1 'cthickb''

* x-y label 
 'q gxinfo';*the first parea changes when the plot is defined
   xline=sublin(result,3);xr2=subwrd(xline,6);xl2=subwrd(xline,4)
   xline=sublin(result,4);yt2=subwrd(xline,6);yb2=subwrd(xline,4)

xlab=on
ylab=on
*if(xlab=on);xvlf=0;xvrg=180;xint=60;
*            call=myxlabh(xl2,xr2,yt2,yb2,xvlf,xvrg,xint);endif
if(xlab=on);xvlf=230;xvrg=300;xint=20.0;
            call=myxlabh(xl2,xr2,yt2,yb2,xvlf,xvrg,xint);endif
if(ylab=on);yvb=20.0;yvt=60.0;yint=10.0;
            call=myylabh(xl2,xr2,yt2,yb2,yvb,yvt,yint);endif
* 
*------------------------

*'set mpdraw on'
*'set map 1 1 2'
*'set mpdset hires'
*'draw map'

*'set mpdset lowres'
*'set map 1 1 5'
*'draw map'

*-----------
*'set strsiz 0.11 0.12'
*'set string 1 l'
*'draw string 'xl+0.02' 'yb+0.68' 'title2''
*'draw string '0.25' 'yb-0.50' 'pathref''
*------------------------* 
if (1=1) 
  xres=2000*0.85
  yres=2000*1.10
  'printim 'pngfile' x'xres' y'yres' white'
* '!mogrify -crop   1075x575+1+50 +repage 'pngfile''
endif 
*-'quit'
*-------------------------* 
*-------------------------* 
function myxlabh(xl,xr,yt,yb,xvlf,xvrg,xint)
* xvlf=-120;xvrg=-20;xint=20 
*
* to log scale set splot=log
* to linear scale set splot=anything
* Jan 21, 2009
*
  cthickb = 6;
  splot=log
  splot=nolog
*
  'q dim'
  xline=sublin(result,2);x1=subwrd(xline,6);xn=subwrd(xline,8)
*
  nptos=(xvrg-xvlf)/xint + 1;
*
  it=1;while(it<=nptos)
          xx.it = xvlf + (it-1)*xint 
*          say xx.it 
       it=it+1
       endwhile
* lab 
  it=1;while(it<=nptos)
        xreal=xx.it
        xabs=math_abs(xx.it)
        say xreal 
        if (xreal<0);
           if (xreal>-180);
               xxlb.it=xabs%"`aO`nW";
           else; 
                xtmp = 360-xabs
                xxlb.it=xtmp%"`aO`nE";
           endif
        else
           if(xreal<=180) 
            xxlb.it=xabs%"`aO`nE";
           else 
              xtmp = 360-xabs
              xxlb.it=xtmp%"`aO`nW";
           endif 
           if(xreal=0|xreal=360);xxlb.it="0`aO`n";endif

        endif
        if(xreal=0|xreal=180);xxlb.it=xabs%"`aO`n";endif
*        xxlb.it=math_abs(xx.it) 
*        if(splot=log);tmp=math_log(xx.it);xx.it=tmp;endif
*        say xxlb.it 
        it=it+1
       endwhile
*

*
  ip=1;while(ip<=nptos)
         xx = xx.ip
         xs = xl+(xx-x1)*(xr-xl)/(xn-x1)
         'set line 1 1 1'
         'draw line 'xs' 'yb' 'xs' 'yb-0.075''
         'set line 1 3 1'
*         'draw line 'xs' 'yb' 'xs' 'yt*2.45''
         y = yt
*        'draw line 'xs' 'yb' 'xs' 'yt''
*        'set strsiz 0.14'
         'set strsiz 0.11 0.11'
*        'set string 1 c 3'
         'set string 1 c 'cthickb''
         'draw string 'xs'  'yb-0.175' 'xxlb.ip''
      ip=ip+1
      endwhile
return
*------------------------
function myylabh(xl,xr,yt,yb,yvb,yvt,yint)
* yvb=-40;yvt=40;yint=10;
*
* to log scale set splot=log
* to linear scale set splot=anything
* Jan 21, 2009
*
  cthickb=6 
  splot=log
  splot=nolog
*
  'q dim'
  xline=sublin(result,3);y1=subwrd(xline,6);yn=subwrd(xline,8)
  nptos=(yvt-yvb)/yint + 1;
*
  it=1;while(it<=nptos)
          yy.it = yvb  + (it-1)*yint 
*          say yy.it 
       it=it+1
       endwhile
* lab 

  it=1;while(it<=nptos)
        yreal=yy.it
        yabs=math_abs(yy.it)
        if(yreal=0);yylb.it="EQ";endif
        if(yreal<0);yylb.it=yabs%"`aO`nS";endif
        if(yreal>0);yylb.it=yabs%"`aO`nN";endif
*        if(splot=log);tmp=math_log(xx.it);xx.it=tmp;endif
*        say yylb.it 
        it=it+1
       endwhile
*
  ip=1;while(ip<=nptos)
         yy = yy.ip
*        xs = xl+(xx-x1)*(xr-xl)/(xn-x1)
         ys = yb+(yy-y1)*(yt-yb)/(yn-y1)
         'set line 1 1 1'
         'draw line 'xl-0.075' 'ys' 'xl' 'ys''
         'set line 1 3 1'
*         'draw line 'xs' 'yb' 'xs' 'yt*2.45''
*         'draw line 'xs' 'yb' 'xs' 'y''
*        'draw line 'xr' 'ys' 'xl' 'ys''
*        'set strsiz 0.14'
         'set strsiz 0.11 0.11'
*        'set string 1 r 3'
         'set string 1 r 'cthickb''
*         'draw string 'xs'  'yb-0.2' 'xxlb.ip''
         'draw string 'xl-0.10'  'ys' 'yylb.ip''
      ip=ip+1
      endwhile
return
*----------------------------------------------

