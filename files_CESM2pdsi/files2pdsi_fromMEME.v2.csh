#! /bin/csh 

# 
#---

#set case = f.e12.FCLM45SP.f19_19.control.001z
#set case = f.e12.FCLM45SP.f19_19.latephen.001z
 set case = f.e12.FCLM45SP.f19_19.earlyphen.001z

# 

 set outpath = /glade/work/cmc542/post/$case/to_PDSI/

 set xpath =  /glade/p/univ/ucor0023/cmc542/archive/$case/

# 
#------------------------------
#set t_period = '085001-185012' 
#-
 set t_period = '197501-200512' 
#------------------------------

#set filesclm = `ls $xpath/lnd/hist/$case.clm2.h1.*nc` 
#set filescam = `ls $xpath/atm/hist/$case.cam.h1.*nc` 
 set filesclm = `ls $xpath/lnd/hist/$case.clm2.h0.*nc` 
 set filescam = `ls $xpath/atm/hist/$case.cam.h0.*nc` 

 mkdir -p $outpath

#--
#--
# in lsm ( LAND submodes )  
# foreach var (RAIN TSA Q2M)
# in cam ( ATM submodes )  
# foreach var (PSL FLNS FSNS)
#--
#--
#foreach var (TS PSL FLNS FSNS)
#foreach var (TS PSL PS FLNS FSNS PRECC PRECL PRECSC PRECSL)
#foreach var (TREFHT)
 foreach var (TREFHT TS PSL PS FLNS FSNS PRECC PRECL)
   echo $var  "..."
#  set fileout = /glade/scratch/cmc542/post/$case/to_PDSI/$case.cam.h0.$var.190001-200512.nc  
#  set fileout = $outpath/$case.cam.h0.$var.190001-200512.nc  
   set fileout = $outpath/$case.cam.h0.$var.${t_period}.nc  
    ncrcat --ovr -v $var $filescam $fileout 
   echo $fileout 
#- exit 
 end 
#exit 
 foreach var (RAIN TSA Q2M)
#foreach var (TSA Q2M)
   echo $var  "..."
#  set fileout = /glade/scratch/cmc542/post/$case/to_PDSI/$case.clm2.h0.$var.190001-200512.nc  
#  set fileout = $outpath/$case.clm2.h0.$var.190001-200512.nc  
   set fileout = $outpath/$case.clm2.h0.$var.${t_period}.nc  
   ncrcat --ovr -v $var $filesclm $fileout 
   echo $fileout 
#  exit 
 end 
